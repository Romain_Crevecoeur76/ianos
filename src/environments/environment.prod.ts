export const environment = {
  production: true,
  dialogFlowToken: 'e34a1f6932304e6684588f9dd79390f6',
  firebase: {
    apiKey: 'AIzaSyCM1kvaJNoSmwAYdS3M5pR43rJxkySZ0-E',
    authDomain: 'ianos-9b26a.firebaseapp.com',
    databaseURL: 'https://ianos-9b26a.firebaseio.com',
    projectId: 'ianos-9b26a',
    storageBucket: 'ianos-9b26a.appspot.com',
    messagingSenderId: '462661567247'
  },
  dialogflow: {
    type: 'service_account',
    project_id: 'small-talk-e71c1',
    private_key_id: 'fe82eab6d4bf28cdeab4918e107d0cf78a4b437f',
    private_key: '-----BEGIN PRIVATE KEY-----\nMIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQCw53DXztgk/xZ5\nGmCMrh8ekh09v47mYjqyKN1ELN4TdiHNE0ZoBgX0JRknCol55/IC2ue3D7A2H6nI\nKYdH3Nl71E0BV7KMwCo0NTHqJXPHsHWasiJpd8/q9okvDynvqPloCMmYhgTS5o2q\nciFddB2slWt1nq/Xp+NcUxRHqGxFyqxbQOAQqTk2E6ILMJooxInOt1PP0sXBUaiz\n8ZP45v5yXB5r9GBYWPC7WJxP3t8NJbn1eDiSCDcXfKNb12PdDL1uWXHdGWGdTlK5\nb9vfNFxgN5gYVk3XEn/8V7tcF0s+pvUI0BCTvJd21Def46/eLZN9Z0vj97ZSHyMF\nWVHViNOJAgMBAAECggEAAY+m57sLN8P2uUHFFXPwHndlVPceWV46pkaLi1XTba8J\n2QY0RYGi5rbQ5cINsm4Phrxsgo/oh9W+mRLBJZ50K3sh/qLJ1qM+GOdZeSqnvqmj\npAzLfNkVYRcYLbp8lE4BfQxLNFjsbN/WSYLuOr8/uspRYDPMcyk0lBfZZdEXIYRT\ngj7BSPbz7O0mjkXr4J1XpZz2yy+jd7z1QHuFgQFFXUxEG8ISh3d0iSxscr0lK1uS\nhP5lT2BrMhDIaxvy/X7JEaDgSgBRnI5eXe3b5OwCpJGtLAIRe8wd6rJmfzv2TnNc\nELAqSrKbDZw95gXKDYaLS1c3pk2HTxYGy2UH6CHe4QKBgQD3zuqiVag3vkiAh5kF\n7K6425zSeOe+X6XBPbBWyVakaYh5+qaQUgfYPteQRDDOeAHmzZR7zdxUt+sMuRYu\nl/3yhTLfUW/58GIH2w26FvNS7otX0+/cpoaBUucTQI21LT6NLNGzOBuPLyMS9b2p\nCQ/Yqc29xZcHjXvJk1gP/44RGQKBgQC2wH7oATvhinBriOn0LswcncIKeAkJvONO\nBoXD9R/kCnN0RdhoimgzRn98+rUqXXADVHTgejGbSuKIL+eEys/SL572Xtz9JLte\n5imNaxOdmrJoYIjAFUILtrCCjnbpNkXDIeUZUZUPAEZDpJlM9kVnTrFDQnCzdCDp\nlMj5nRDz8QKBgQChTFvJYnvmHMNQdfKSuqZXyQrgrLYbYmKfz6mSM+djiRW+X/o/\nAkX86ZqiWuBx5AdI1hOKKkr6EeS7GVZo6aHwU+bzGS0LwP3uIxViRUIIfyDQADGm\nq/XtA6tJCsGyZkrK1ZuBTkoR/xqTYsUa270myBAVMCHIVr12TaQKdcthMQKBgQCA\nAeuidKLOA1tlZ1yAZnsgHDx7esLR6FXLJHRz4vH4mH1wVzATYcdFCz9nenCFgTdh\njbnkNpMitFxiijN/51OUZTjSV3YxdpYsDcnTPcpana4TwHJpITrG26tdd47BIfvR\nh59ikat3+3g8D0I2D1Zj21H8y8AHYvNeyZHQjfz+8QKBgQDJofdR7agTtHAla60j\nA3qz1JJIU1YEFQKnMPpzEEIDBS1PMV0kLCiPxciqFhVdvRZGleYrQ9HVqfNVtMiD\nTgiVeJpmr3xs9QYCM2QU85Pse27R86ys4Qj0w6R9T+bZtfctiT2onyDJt6+ywY0S\nK9F0+pD0zORXIq0phno9Z3LM6g==\n-----END PRIVATE KEY-----\n',
    client_email: 'dialogflow-ioccdq@small-talk-e71c1.iam.gserviceaccount.com',
    client_i: '113775243838545936513',
    auth_ur: 'https://accounts.google.com/o/oauth2/auth',
    token_ur: 'https://oauth2.googleapis.com/token',
    auth_provider_x509_cert_ur: 'https://www.googleapis.com/oauth2/v1/certs',
    client_x509_cert_url: 'https://www.googleapis.com/robot/v1/metadata/x509/dialogflow-ioccdq%40small-talk-e71c1.iam.gserviceaccount.com'
  },
  // contentful: {
  //   spaceId: 'fm9x1oiyunsx',
  //   token: 'a7817cb018484cfc75176fd56fae373177dcbb302cd2d1708954b16aa437129e'
  // }
};
