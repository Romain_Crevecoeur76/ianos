import { BookingService } from './services/firestore/booking.service';
import { SpreadsheetService } from './services/fetch/spreadsheet.service';
import { CompanyService } from './services/firestore/company.service';
import { Inject, PLATFORM_ID, ViewChild } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { UsersService } from './services/firestore/users.service';
import { AuthService } from './services/auth/auth.service';
import { Subscription } from 'rxjs/internal/Subscription';
import { User } from './interfaces/user';
import { Component, Output, OnDestroy, NgZone } from '@angular/core';
import { ObservableService } from './services/utils/observable.service';
import { Router, NavigationEnd } from '@angular/router';
import { NgbPopover } from '@ng-bootstrap/ng-bootstrap/popover/popover';
import { MatMenuTrigger } from '@angular/material/menu';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Company } from './interfaces/company';
import { Booking } from './interfaces/booking';
import { NgbDropdown } from '@ng-bootstrap/ng-bootstrap';
import { LoginComponent } from './../app/components/guest/sign/login/login.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

declare let ga: Function;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnDestroy {
  @Output() user: User;
  @ViewChild(MatMenuTrigger, { static: true }) trigger: MatMenuTrigger;

  propositions: Booking[] = [];
  favorites: Booking[] = [];
  bookings: Booking[] = [];
  pendings: Booking[] = [];
  booked: Booking[] = [];

  fournisseur = true
  batisseur = true;
  profil = true;

  toggleBot = false;

  isConnected = false;
  isNotConnected = false;

  isDesktop = false;
  isMobile = false;

  isInscriptions = false;

  userToAdd: User = {
    isOrganisme: false,
    isStagiaire: true,
  };
  subscription: Subscription;
  hidden = true;
  errorLoginCode = "";

  windowScrolled: boolean;

  routerLink = "";
  url: string;

  constructor(
    private snackBar: MatSnackBar,
    private modalService: NgbModal,
    private router: Router,
    private authService: AuthService,
    private userService: UsersService,
    private companyService: CompanyService,
    private bookingService: BookingService,
    private observableService: ObservableService,
    private spreadsheetService: SpreadsheetService,
    @Inject(PLATFORM_ID) private platformId: Object,
    public zone: NgZone
  ) {
    this.subscription = this.observableService.currentLogout.subscribe(trigger => {
        trigger ? this.deconnected() : null;
        this.user && trigger ? null : this.getUser();
      });

    router.events.subscribe(val => {
      let url = val["url"];
      if (url) {
        this.url = url;
        url === '/' ? this.routerLink = "bot" : this.routerLink = "";
        url.includes('inscription') || url.includes('connexion') ? this.isInscriptions = true : this.isInscriptions = false;
      }
    });

    this.getUser();

    if (isPlatformBrowser(this.platformId)) {
      if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        this.observableService.triggerMobile(true);
        this.isDesktop = false;
        this.isMobile = true;
      }
      else {
        this.isDesktop = true;
        this.isMobile = false;
      }
    }

    // router pour google analytics
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        ga('set', 'page', event.urlAfterRedirects);
        ga('send', 'pageview');
      }
    });

  }

  ngOnDestroy = () => this.subscription.unsubscribe();

  topFunction = () => document.querySelector('.main').scrollIntoView({ behavior: 'smooth' });

  close = () => this.hidden = true;

  scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
      document.getElementById("myBtn").style.display = "block";
    } else {
      document.getElementById("myBtn").style.display = "none";
    }
  }

  getUser() {
    if (!this.user) {
      this.userToAdd.email ? this.user = this.userToAdd : null;
      this.authService.getCurrentUser()
        .then(user => {
          if (user) {
            this.userService.getUserById(user.uid).snapshotChanges()
              .subscribe(user => {
                this.user = { id: user.payload.id, ...user.payload.data() as User};
                this.user.eid ? this.getCompany() : null;
                this.observableService.triggerGetUser(this.user);
                this.hidden = true;
                this.isNotConnected = false;
                this.isConnected = true;
                this.getPropositions(this.user.id);
                this.getFavorites(this.user.id);
                this.getPendings(this.user.id);
                this.getBooked(this.user.id);
                this.zone.run(() => { });
              });
          }
          else {
            this.deconnected();
          }
        })
        .catch(error => {
          this.spreadsheetService.sendError(error);
          this.deconnected();
        });
    }
  }

  getPropositions(uid: string) {
    this.bookingService.getPropositionsByUid(uid).onSnapshot(query => {
      this.propositions = [];
      query.docChanges().forEach(booking => this.propositions.concat(this.handleBooking(booking, this.propositions)))
    });
  }

  getPendings(uid: string) {
    this.bookingService.getPendingsByUid(uid).onSnapshot(query => {
      this.pendings = [];
      query.docChanges().forEach(booking => this.pendings.concat(this.handleBooking(booking, this.pendings)))
    });
  }

  getBooked(uid: string) {
    this.bookingService.getBookedByUid(uid).onSnapshot(query => {
      this.booked = [];
      query.docChanges().forEach(booking => this.booked.concat(this.handleBooking(booking, this.booked)))
    });
  }

  getFavorites(uid: string) {
    this.bookingService.getFavoritesByUid(uid).onSnapshot(query => {
      this.favorites = [];
      query.docChanges().forEach(booking => this.favorites.concat(this.handleBooking(booking, this.favorites)))
    });
  }

  handleBooking(booking, bookingToFill: Booking[]) {
    booking.type === "removed" ? bookingToFill = bookingToFill.filter(item => item.id !== booking.doc.id) : null;
    booking.type === "added" ? bookingToFill.push({ id: booking.doc.id, ...booking.doc.data() as Booking}) : null;
    return bookingToFill;
  }

  getCompany() {
    this.companyService.getCompanyById(this.user.eid).snapshotChanges()
      .subscribe(company => {
        let companyToCompare = { id: company.payload.id, ...company.payload.data() as Company};
        companyToCompare.managers && companyToCompare.managers.includes(this.user.id) ? this.user.isManager = true : null;
        companyToCompare.rh && companyToCompare.rh.includes(this.user.id) ? this.user.isRh = true : null;
      })
  }

  logout() {
    this.router.navigate(['/']);
    this.authService.doLogout()
      .then(() => this.deconnected())
      .catch(error => console.log("​BsNavbarComponent -> logout -> error", error));
  }

  deconnected() {
    this.isInscriptions = false;
    this.isConnected = false;
    this.isNotConnected = true;
    this.hidden = false;
    this.user = null;
    this.observableService.triggerGetUser(this.user);
  }

  connect(popover: NgbPopover) {
    this.authService.tryLogin(this.userToAdd)
      .then(() => this.getUser())
      .catch(error => this.handleError(error, popover));
  }

  handleError(error, popover: NgbPopover) {
    console.log("TCL: AppComponent -> handleError -> error", error)
    this.userToAdd.email ? null : this.userToAdd.email = error.email;
    error.credential ? this.userToAdd.signInMethod = error.credential.signInMethod : this.userToAdd.signInMethod = 'password';
    let add = "";
    error.code === "auth/wrong-password" ? add = " Changer de mot de passe : ici" : null;
    popover.popoverClass = "test-class"
    popover.ngbPopover = this.authService.translateErrorMessage(error, this.userToAdd) + add;
    popover.open();
    if (add !== "") {
      popover.hidden.subscribe(() => {
        this.authService.changePassword(this.userToAdd.email);
        this.openSnackBar("Un email pour réinitialiser votre mot de passe a été envoyé", "Valider");
      });
    }
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, { duration: 10000 });
  }

  connectWithFacebook(popover: NgbPopover) {
    this.authService.doRegisterWithFacebook()
      .then(query => query.additionalUserInfo.isNewUser ? this.addUser(query.user) : this.getUser())
      .catch(error => this.handleError(error, popover));
  }

  connectWithGoogle(popover: NgbPopover) {
    this.authService.doRegisterWithGoogle()
      .then(query => query.additionalUserInfo.isNewUser ? this.addUser(query.user) : this.getUser())
      .catch(error => this.handleError(error, popover));
  }

  addUser(user) {
    this.userToAdd.email = user.email;
    this.userToAdd.id = user.uid;
    this.authService.addUserToBdd(user, this.userToAdd);
  }

  inscriptions() {
    this.observableService.triggerGetUser(this.userToAdd);
    this.router.navigate(["/inscriptions"]);
  }

  hideSubNav(myDrop: NgbDropdown) {
    if (myDrop && !myDrop.isOpen()) {
      this.profil = true;
      this.fournisseur = true;
      this.batisseur = true;
    }
  }

  open(){
    this.modalService.dismissAll(); 
    this.modalService.open(LoginComponent, { size: 'lg', centered: true});
  }

  connected = () => this.isConnected && !this.isNotConnected;

  notConnected = () => !this.isConnected && this.isNotConnected;

  isHideSubNav = () => this.profil && this.fournisseur && this.batisseur;

  toggle = () => this.toggleBot ? this.toggleBot = false : this.toggleBot = true;

  sendMessage = (inputMessage: string) => inputMessage.length > 2 ? this.router.navigate(['bot'], { queryParams: { message: inputMessage } }) : this.router.navigate(['/bot']);

}