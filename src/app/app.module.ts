// import { VoiceRecognitionService } from './services/utils/voice-recognition.service';
import { MaterialModule } from './../assets/material-module';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import { LOCALE_ID } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';

import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule, SETTINGS } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { NativeDateModule, MatNativeDateModule } from '@angular/material/core';

import { AppComponent } from './app.component';

import { environment } from '../environments/environment';

import { SanitizeStylePipe } from './pipes/sanitize-style.pipe';
import { FilterCarouselPipe } from './pipes/filter-carousel.pipe';
import { SanitizeHtmlPipe } from './pipes/sanitize-html.pipe';
import { SafeUrlPipe } from './pipes/safe-url.pipe';

import { UsersService } from './services/firestore/users.service';
import { ObservableService } from './services/utils/observable.service';
import { AuthGuardOrganisme } from './services/auth/auth.guard-organisme';
import { AuthGuardStagiaire } from './services/auth/auth.guard';
import { BlogService } from './services/firestore/blog.service';
import { TodoService } from  './services/firestore/todolist.service';
import { SendEmail } from './services/fetch/send-email';
import { AdminService } from './services/firestore/admin.service';
import { FormationsService } from './services/firestore/formations.service';
import { AuthService } from './services/auth/auth.service';

import { HomeComponent } from './components/guest/home/home.component';
import { BsNavbarComponent } from './components/guest/bs-navbar/bs-navbar.component';
import { AboutComponent } from './components/guest/about/about.component';
import { FinancementComponent } from './components/guest/financement/financement.component';
import { SignInComponent } from './components/guest/sign/sign-in/sign-in.component';
import { GetSessionsModalComponent } from './components/guest/bot/get-sessions-modal/get-sessions-modal.component';
import { GetSessionComponent } from './components/guest/bot/get-session/get-session.component';
import { CarouselModalComponent } from './components/guest/bot/carousel-modal/carousel-modal.component';
import { CarouselComponent } from './components/guest/bot/carousel/carousel.component';
import { DashboardComponent } from './components/user/dashboard/dashboard.component';
import { CguComponent } from './components/guest/cgu/cgu.component';
import { CGVComponent } from './components/guest/cgv/cgv.component';
import { ApportAffairesComponent } from './components/guest/apport-affaires/apport-affaires.component';
import { CharteComponent } from './components/guest/charte/charte.component';
import { ContactComponent } from './components/guest/contact/contact.component';
import { ProviderComponent } from './components/guest/provider/provider.component';
import { LoginComponent } from './components/guest/sign/login/login.component';
import { SignInEmbedComponent } from './components/guest/sign/sign-in-embed/sign-in-embed.component';
import { ProfilComponent } from './components/user/profil/profil.component';
import { CategoriesComponent } from './components/guest/categories/categories.component';
import { TrainingComponent } from './components/guest/training/training.component';
import { BatisseurComponent } from './components/guest/batisseur/batisseur.component';
import { SearchComponent } from './components/guest/search/search.component';
import { CardTrainingComponent } from './components/guest/card-training/card-training.component';
import { FooterComponent } from './components/guest/footer/footer.component';
import { toDolistComponent } from './components/user/toDolist/toDolist.component';
import { AllTrainingComponent } from './components/guest/all-training/all-training.component';
import { EspaceEntrepriseComponent } from './components/entreprise/espace-entreprise/espace-entreprise.component';
import { DashboardCompanyComponent } from './components/user/dashboard-company/dashboard-company.component';
import { CardMemberComponent } from './components/entreprise/card-member/card-member.component';
// import { FavoriteComponent } from './components/user/favorite/favorite.component';
import { RequestComponent } from './components/user/request/request.component';
// import { PropositionComponent } from './components/user/proposition/proposition.component';
import { BookedComponent } from './components/user/booked/booked.component';
import { PageEntrepriseComponent } from './components/entreprise/page-entreprise/page-entreprise.component';
import { PendingComponent } from './components/user/pending/pending.component';
import { ListMemberComponent } from './components/entreprise/list-member/list-member.component';
import { GlobalPipesModule } from './pipes/global-pipes/global-pipes.module';
import { CarouselFinancementComponent } from './components/guest/carousel-financement/carousel-financement.component';
import { BotComponent } from './components/guest/bot/bot/bot.component';
import { ListeMembreComponent } from './components/organisme/liste-membre/liste-membre.component';
import { BrowserModule } from '@angular/platform-browser';
import { MatTableModule } from '@angular/material/table';
import { MatMomentDateModule } from '@angular/material-moment-adapter'

registerLocaleData(localeFr, 'fr');

@NgModule({
  declarations: [
    AppComponent,
    BsNavbarComponent,
    HomeComponent,
    AboutComponent,
    FinancementComponent,
    SignInComponent,
    GetSessionsModalComponent,
    GetSessionComponent,
    CarouselModalComponent,
    CarouselComponent,
    DashboardComponent,
    CguComponent,
    CGVComponent,
    ApportAffairesComponent,
    CharteComponent,
    ContactComponent,
    ProviderComponent,
    LoginComponent,
    SignInEmbedComponent,
    ProfilComponent,
    CategoriesComponent,
    TrainingComponent,
    BatisseurComponent,
    SearchComponent,

    FilterCarouselPipe,
    SanitizeHtmlPipe,
    SafeUrlPipe,
    SanitizeStylePipe,
    CardTrainingComponent,
    FooterComponent,
    toDolistComponent,
    AllTrainingComponent,
    DashboardCompanyComponent,
    // FavoriteComponent,
    RequestComponent,
    // PropositionComponent,
    BookedComponent,
    PageEntrepriseComponent,
    PendingComponent,
    ListMemberComponent,
    CarouselFinancementComponent,
    BotComponent,
    ListeMembreComponent,

    // entreprise
    CardMemberComponent,
    EspaceEntrepriseComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({
      appId: 'ng-universal-demystified'
    }),
    FlexLayoutModule,
    HttpClientModule,
    GlobalPipesModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
    FormsModule,
    ReactiveFormsModule,
    NativeDateModule,
    AppRoutingModule,
    MaterialModule,
    MatDatepickerModule,
    NgbModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireStorageModule,
    AngularFireAuthModule,
    MatNativeDateModule,
    MatMomentDateModule,

  ],
  providers: [
    { provide: SETTINGS, useValue: {} }, //To delete when firebase 6.0.0 is ssr proof
    { provide: LOCALE_ID, useValue: 'fr' },
    ObservableService,
    AuthService,
    AuthGuardOrganisme,
    AuthGuardStagiaire,
    FormationsService,
    UsersService,
    AdminService,
    BlogService,
    TodoService,
    // VoiceRecognitionService,
    SendEmail
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    ListMemberComponent,
    GetSessionsModalComponent,
    GetSessionComponent,
    CarouselModalComponent,
    
  ]
})
export class AppModule { }