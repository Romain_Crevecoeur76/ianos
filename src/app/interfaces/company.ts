export interface Company {
 id?: string;
 name?: string;
 presentation?: string;
 siret?: string;
 email?: string;
 phone?: string;

 cp?: string;
 state?: string;
 adress?: string;
 town?: string;

 members?: string[];
 managers?: string[];
 rh?: string[];
 employees?: string[];

 // plan de formation
 // periode de planification
//  trainingPlan?: string[] = ['2021', '2022', '2023', '2024'];
//  content
 prenom?: string;
 nom?: string;
 function?: string;
 need?: string;
 disponibility?: string;
 training?: string;
 trainingOrganisation?: string;
 trainingDuration?: number;
 totalCost?: number;
 internalCost?: number;
 externalCost?: number;
 missFinancing?: number;
 trainingPriority?: string[];
 value?: string;
 viewValue?: string;
 userKeywords?: string[];
 userKeyword?: string;


 campaignOne?: Date;
 campaignTwo?: Date;
 startDisponibility?: Date ;
 EndDisponibility?: Date;
 StartTraining?: Date;
 EndTraining?: Date;


//  analytics
numMembers?: number;
totalTrainingDuration?: number[];
}
