
export interface Message {
  content?: string;
  avatar?: string;
  timestamp?: Date;
  contexts?: any[];
  isBot?: boolean;
  sessionId?: string;
  intent?: string;
  allRequiredParamsPresent?: boolean;
}