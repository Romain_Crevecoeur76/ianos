export interface User {
    // inscription
    id?: string;
    password?: string;
    email?: string;

    
    displayName?: string;
    siret?: string;
    phone?: string;
    prenom?: string;
    nom?: string;

    // données entreprises
    eid?: string;
    company?: string;
    function?: string;
    role?: string;
    status?: string;
    sector?: string;
    userImage?: string;

    // plan de formation
    // periode de planification
    // trainingPlan?: string[] = ['2021', '2022', '2023', '2024'];
    //  content
    need?: string;
    disponibility?: string;
    training?: string;
    trainingOrganisation?: string;
    trainingDuration?: number;
    totalCost?: number;
    internalCost?: number;
    externalCost?: number;
    missFinancing?: number;
    trainingPriority?: string[];
    value?: string;
    viewValue?: string;


    campaignOne?: Date;
    campaignTwo?: Date;
    startDisponibility?: Date;
    EndDisponibility?: Date;
    StartTraining?: Date;
    EndTraining?: Date;

    // coordonnées users
    adress?: string;
    ville?: string;
    state?: string;
    cp?: string;
    region?: string;
    geoPoint?: any;

    leadOrNot?: string;

    inscription?: string;
    isStagiaire?: boolean;
    isOrganisme?: boolean;
    isAdmin?: boolean;
    isRh?: boolean;
    isManager?: boolean;
    isFormationUser?: boolean;
    isMemberProposition?: boolean;

    signInMethod?: string;

    // ???
    getProperties?(): string[];

    // données infos OF
    presentation?: string;
    pedagogie?: string;
    rncp?: string;
    pointFort1?: string;
    pointFort2?: string;
    pointFort3?: string;
    keywords?: string;
    datadock?: boolean;
    qualiopi?: boolean;
    
    formateurIndependant?: boolean;
    agenceDeConseil?: boolean;
    organismeDeFormation?: boolean;
    activitePrincipale?: boolean;
    activiteSecondaire?: boolean;
    OF?: boolean;
    prestataireDeService?: boolean;

    userKeywords?: string[];
    userKeyword?: string;

    // todolist
    Todolist?: string;
}
