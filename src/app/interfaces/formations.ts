import { Place } from './place';
import { Trainer } from './trainer';
import { GeoPoint } from '@firebase/firestore-types';

export interface Formation {

    id?: string;
    pid?: string;
    sid?: string;
    tid?: string;
    title?: string;
    description?: string;
    programme?: string;
    prerequis?: string;
    public?: string;
    keywords?: string [];
    nombrePlaceTotal?: number;
    nombrePlaceMinimum?: number;
    organisme?: string;
    certification?: boolean;
    parcoursDeFormation?: string;
    objectifGeneralFormation?: string;
    dateCreated?: Date;
    presentation?: string;

    // Reviewing
    review?: number;

    // Search
    searchPoint?: number;
    baseSearchPoint?: number;
    motCleArray?: string[];
    motCle?: string;
    

    // Formation
    intitule?: string;
    argument?: string;
    objectifs?: string;
    resultats?: string;
    pedagogie?: string;
    extras?: string;
    books?: string;
    securite?: string;
    // contenu?: string;
    urlFormation?: string;
    urlweb?: string;
    urlImage?: string;
    urlImage2?: string;
    urlVideoYoutube?: string;
    urlVideoYoutube2?: string;
    codeRNCP?: string;
    codeCERTIFINFO?: string;

    // Contact
    // OF Responsable
    uid?: string;

    // Action de formation
    publicVise?: string;
    rythme?: string;
    niveauEntree?: string;
    modalite?: string;
    presentiel?: boolean;
    recrutement?: string;

    // modalité et prix
    formationInter?: boolean;
    formationIntra?: boolean;
    formationIndividuelle?: boolean;
    prixTotalTTC?: number;
    prixIntraTTC?: number;
    prixIndividuelleTTC?: number;
    prixHoraireTTC?: number;
    exonerationtva?: boolean;
    visio?: boolean;
    video?: boolean;
    tutoring?: boolean;
    mooc?: boolean;
    phoning?: string;


    // certification & financement
    datadock?: boolean;
    conventionnement?: boolean;
    cpf?: boolean;

    // Adresse
    adresse?: string;
    ville?: string;
    departement?: string;
    region?: string;
    pays?: string;
    codePostal?: number;
    ligne?: string;
    geoPoint?: GeoPoint;

    duree?: number;
    dureeSeance?: number;
    nombreHeures?: number;
    isRythmeSpread?: boolean;
    isRythmeSpreadMatin?: boolean;
    isRythmeSpreadMidi?: boolean;
    isRythmeSpreadApresMidi?: boolean;
    isRythmeSpreadSoir?: boolean;


    inscription?: string;
    isDate?: boolean;
    placeArray?: Place[];
    sessionArray?: string[];

    trainer?: Trainer;

    // analytics
    
    eclickNumber?: number; // Nbr de  redirection vers un site quand formation elearning
    clickNumber?: number; // Nbr de préinscription
    viewNumber?: number; // Nbr de fois que la page a été vue
    
}
