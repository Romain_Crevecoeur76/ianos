export interface todolist{
    id?: string;
    uid?: string;
    text?: any;
    task?: string;
    title?: string[];
    completed?: boolean;
    editing?:boolean;

}
