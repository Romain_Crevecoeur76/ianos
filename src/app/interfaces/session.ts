import { NgbDate } from "@ng-bootstrap/ng-bootstrap";

export interface Session {
    id?: string;
    pid?: string;
    fid?: string;
    uid?: string;

    dateString?: string;
    startDate?: Date;
    endDate?: Date;
    fromDate?: NgbDate;
    toDate?: NgbDate;

    inscription?: string;
    nombrePlaceTotal?: number;
    intituleFormation?: string;
    organisme?: string;

    uidPreinscription?: string[];
    uidEnAttenteInscription?: string[];
    uidInscrit?: string[];
}
