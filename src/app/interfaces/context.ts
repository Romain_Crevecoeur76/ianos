import { GeoPoint } from '@firebase/firestore-types';

export interface Context {
    // Interface de gestion de l'affichage du carousel
    id?: string;
    pid?: string;
    sid?: string;
    bid?: string;
    certification?: boolean;
    modalite?: string;
    motCle?: string;
    motCleArray?: string[];
    ville?: string;
    region?: string;
    departement?: string;
    geoPoint?: GeoPoint;
    intent?: string;
    location?: string;

}
