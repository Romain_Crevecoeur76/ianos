export interface Review {
 id?: string;
 fid?: string;
 uid?: string;
 sid?: string;
 pid?: string;
 bid?: string;

 value?: number;
 

 isQuestion?: boolean;
 isReview?: boolean;
 isCompanyChat?: boolean;

 dateCreated?: number;
 dateUpdated?: Date;

 email?: string;
 prenom?: string;
 nom?: string;
 role?: string;
 comment?: string;
 currentRate?: number;

 
 responses?: {
  message?: string,
  role?: string,
  prenom?: string,
  nom?: string
 }[];
 lastResponse?: string;
}