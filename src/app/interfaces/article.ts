export interface Article {
 titleUrl?: string;
 title?: string;
 content?: string;
 contentSummary?: string;
 contentArray?: any[];
 dateCreation?: Date;
 dateUpdate?: Date;
 motCleArray?: string[];
}
