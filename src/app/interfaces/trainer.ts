import { User } from './user';

export interface Trainer extends User {
  presentation?: string;
  hobbies?: string;
  quote?: string;
  keywords?: string;
  filePicture?: any;
  urlPicture?: string;
  isTrainer?: boolean;
}
