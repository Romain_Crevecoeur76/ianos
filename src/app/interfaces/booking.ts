export interface Booking {
 id?: string;
 uid?: string;
 fid?: string;
 pid?: string;
 sid?: string;
 eid?: string;
 ofid?: string;
 rhid?: string;

 email?: string;
 intituleFormation?: string;
 ville?: string;

 dateString?: string;

 members?: {
  id: string;
  prenom?:string;
  email:string;
  role: string;
  theme: boolean;
  formation: boolean;
  date: boolean;
  finance: boolean;
 }[];

 isFavorite?: boolean;
 isRequest?: boolean;
 isProposition?: boolean;
 isPending?: boolean;
 isBooked?: boolean;
 isArchived?: boolean;
}
