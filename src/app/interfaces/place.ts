import { GeoPoint } from '@firebase/firestore-types';

export interface Place {
    id?: string;
    fid?: string;
    uid?: string;
    intituleFormation?: string;
    organisme?: string;

    // Contact sur place
    civilite?: string;
    nom?: string;
    prenom?: string;

    // Adresse
    adresse?: string;
    ville?: string;
    departement?: string;
    region?: string;
    pays?: string;
    codePostal?: number;
    ligne?: string;

    inscription?: string;

    dateAskingPreInscrit?: any[];
    dateAskingEnAttente?: any[];
    dateAskingInscrit?: any[];

    isDate?: boolean;
    sessionArray?: string[];
    location?: { latitude: number, longitude: number };
    geoPoint?: GeoPoint;
}
