export interface Category {
 titleUrl?: string;
 title?: string;
 content?: string;
 contentArray?: any[];
 motCleArray?: string[];
}
