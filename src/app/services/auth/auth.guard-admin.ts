import { UsersService } from './../firestore/users.service';
import { AuthService } from '../../services/auth/auth.service';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class AuthGuardAdmin {
  check = true;

  constructor(
    private router: Router,
    private userService: UsersService,
    private authService: AuthService
  ) { }

  canActivate(): boolean {
    this.getUser();
    return this.check;
  }

  getUser() {
    this.authService.getCurrentUser()
      .then(user => this.userService.getUserById(user.uid).get().toPromise()
        .then(user => user.data()['isAdmin'] ? this.check = user.data()['isAdmin'] : this.check = this.navigate()))
      .catch(() => this.check = this.navigate());
  }

  navigate(): boolean {
    this.router.navigate(['/connexion']);
    return false;
  }
}
