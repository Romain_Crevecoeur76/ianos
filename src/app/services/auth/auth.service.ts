import { SendEmail } from './../fetch/send-email';
import { SlackService } from './../fetch/slack.service';
import { UsersService } from './../firestore/users.service';
import { ObservableService } from '../utils/observable.service';
import { Injectable } from '@angular/core';
import { User } from '../../interfaces/user';
import { AngularFireAuth } from '@angular/fire/auth';
// import { auth } from 'firebase/app';
import firebase from 'firebase/app'



@Injectable()
export class AuthService {

  constructor(
    private userService: UsersService,
    private observableService: ObservableService,
    private slackService: SlackService,
    private sendEmail: SendEmail,
    // public authFire: AngularFireAuth,
    private authFire: AngularFireAuth,
  ) { }

  getCurrentUser() {
    return new Promise<any>((resolve, reject) => {
      this.authFire.onAuthStateChanged(function (user) {
        if (user) {
          resolve(user);
        } else {
          reject('No user logged in');
        }
      })
    })
  }

  doRegister(value) {
    return new Promise<any>((resolve, reject) => {
      this.authFire.createUserWithEmailAndPassword(value.email, value.password)
        .then(res => {
          resolve(res);
        }, err => reject(err))
    });
  }

  signUpWithEmailPasswoerd(value) {
    return new Promise<any>((resolve, reject) =>{
      var email = value.email;
      var password = value.password;
      // [START auth_signin_password]
      firebase.auth().createUserWithEmailAndPassword(email, password)
      .then(res => {
        resolve(res);
      }, err => reject(err))
  });
}
    
   
  

  doRegisterWithGoogle = () => this.authFire.signInWithPopup(new firebase.auth.GoogleAuthProvider());
  

  doRegisterWithFacebook = () => this.authFire.signInWithPopup(new firebase.auth.FacebookAuthProvider());

  doLogin(value) {
    return new Promise<any>((resolve, reject) => {
      this.authFire.signInWithEmailAndPassword(value.email, value.password)
        .then(res => {
          resolve(res);
        }, err => reject(err))
    });
  }

  doLogout() {
    return new Promise((resolve, reject) => {
      if (this.authFire.currentUser) {
        this.authFire.signOut()
        resolve();
      }
      else {
        reject();
      }
    });
  }

  emailConfirmationInscription = () => firebase.auth().currentUser.sendEmailVerification();

  changePassword = (email: string) => this.authFire.sendPasswordResetEmail(email);

  changeEmail = (email: string) => firebase.auth().currentUser.updateEmail(email);

  async deleteUser(user: User) {
    await this.userService.deleteUser(user.id).then(() => 
      firebase.auth().currentUser.delete()).catch(error => 
        console.log('TCL: AuthService -> deleteUser -> error', error));
  }

  async tryRegister(user: User) {
    if (user.email.includes("@")) {
      user.email = user.email.replace(' ', '');
      const result = await this.signUpWithEmailPasswoerd(user);
      return this.addUserToBdd(result.user, user);
    }
  }

  async tryLogin(user: User) {
    if (user.email.includes("@")) {
      user.email = user.email.replace(' ', '');
      await this.doLogin(user);
      return this.observableService.triggerLogout(false);
    }
  }

  addUserToBdd(firebaseUser: firebase.User, user: User) {
    user.id = firebaseUser.uid;
    if (firebaseUser.displayName) {
      user.displayName = firebaseUser.displayName;
      user.prenom = user.displayName.split(" ")[0];
      user.nom = user.displayName.split(" ")[1];
    }
    this.userService.createUser(user)
      .then(() => {
        this.emailConfirmationInscription();
        this.slackService.sendMessageToSlackToInscription('Nouvel utilisateur : ' + user.email + ' Est il stagiaire ? ' + user.isStagiaire);
        user.isStagiaire ? this.sendEmail.sendEmailBienvenue(user) : null;
      });
  }

  translateErrorMessage(error, user: User) {
    console.log("TCL: AuthService -> translateErrorMessage -> errorCode", error)
    let errorTranslated: string;
    switch (error.code) {
      case 'auth/user-disabled':
        errorTranslated = 'Votre compte a été désactivé';
        break;
      case "auth/network-request-failed":
        errorTranslated = 'Problème de réseaux veuillez réessayer';
        break;
      case 'auth/invalid-action-code':
        errorTranslated = 'Le code envoyé par mail est invalide. Il est sans doute expiré';
        break;
      case 'auth/user-not-found':
        errorTranslated = 'Aucun utilisateur ne posséde ce mail chez nous';
        break;
      case 'auth/invalid-email':
        errorTranslated = 'Le format de votre e-mail est incorrecte';
        break;
      case 'auth/weak-password':
        errorTranslated = 'Le mot de passe doit contenir plus de caractères';
        break;
      case 'auth/argument-error':
        errorTranslated = "Le mot de passe n'est pas valide";
        break;
      case 'auth/email-already-in-use':
        errorTranslated = "L'email est déjà utilisé";
        this.tryLogin(user)
        break;
      case 'auth/popup-closed-by-user':
      case 'auth/cancelled-popup-request':
        errorTranslated = "La popup d'inscription a été fermé. Verifiez que rien ne bloque l'ouverture de cette popup (type AdBlock)";
        break;
      case 'auth/account-exists-with-different-credential':
        errorTranslated = "Cet e-mail appartient déjà à un de nos utilisateurs mais par un autre mode de connexion";
        break;
      case 'auth/wrong-password':
        errorTranslated = "Le mot de passe n'est pas valide";
        break;
      default:
        errorTranslated = 'Erreur... Un message a été envoyé à notre équipe';
        break;
    }
    this.slackService.sendMessageToSlackToProblemes("Erreur d'inscription/connexion : " + errorTranslated + ' ' + error.code + " / Pour l'utilistateur : " + user.email + " / Méthode de connexion : " + user.signInMethod);
    return errorTranslated;
  }

  handleResetPassword = (code: string) => {
  console.log("TCL: AuthService -> handleResetPassword -> code", code)
    
    return this.authFire.verifyPasswordResetCode(code)
  };

  handleVerifyEmail = (code: string) => this.authFire.checkActionCode(code);

  confirmResetPassword = (code: string, password: string) => this.authFire.confirmPasswordReset(code, password);
}