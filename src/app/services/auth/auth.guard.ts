import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class AuthGuardStagiaire {
  check = true;

  constructor(
    private router: Router,
    private authService: AuthService,
  ) { }

  canActivate(): boolean {
    this.authService.getCurrentUser()
      .catch(error => {
        console.log("​AuthGuardStagiaire -> canActivate -> error", error);
        this.router.navigate(['/connexion']);
        this.check = false;
      })
    return this.check;
  }
}
