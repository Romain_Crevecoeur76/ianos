import { Injectable } from '@angular/core';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap';

@Injectable({
  providedIn: 'root'
})
export class CalendarService {
  hoveredDate: NgbDate;
  dateArray: any[];
  displayMonths: number;

  public fromDate: NgbDate;
  public toDate: NgbDate;
  public sid: string;

  constructor() {
    this.getDisplayMounths();
  }

  getDisplayMounths() {
    window.innerWidth > 1000 ? this.displayMonths = 3 : null;
    window.innerWidth > 700 && window.innerWidth < 1000 ? this.displayMonths = 2 : null;
    window.innerWidth < 700 ? this.displayMonths = 1 : null;
  }

  onOneDateSelection(date: NgbDate) {
    this.fromDate = null;
    this.fromDate = date;
  }

  onDateSelection(date: NgbDate) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && date.after(this.fromDate)) {
      this.toDate = date;
    } else {
      this.toDate = null;
      this.fromDate = date;
    }
  }

  isHovered(date: NgbDate) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isHoveredOneDate(date: NgbDate) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.equals(this.fromDate);
  }

  changeHoveredDate(hoveredDate: NgbDate) {
    this.hoveredDate = hoveredDate;
  }

  isInside(date: NgbDate) {
    return date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return date.equals(this.fromDate) || date.equals(this.toDate) || this.isInside(date) || this.isHovered(date);
  }

  isRangeOneDate(date: NgbDate) {
    return date.equals(this.fromDate) || date.equals(this.toDate) || this.isInside(date) || this.isHoveredOneDate(date);
  }

  isSelected(date: NgbDate) {
    let dateSelected = false;
    if (this.dateArray && this.dateArray[0]) {
      this.dateArray.forEach(dateFromArray => {
        date.equals(dateFromArray.fromDate) || date.equals(dateFromArray.toDate) ? dateSelected = true : null;
      })
    }
    return dateSelected;
  }

  isRangeSelected(date: NgbDate) {
    let dateRangeSelected = false;
    if (this.dateArray && this.dateArray[0]) {
      this.dateArray.forEach(dateFromArray => {
        date.after(dateFromArray.fromDate) && date.before(dateFromArray.toDate) ? dateRangeSelected = true : null;
      })
    }
    return dateRangeSelected;
  }

  stringifyDates() {
    if (this.fromDate && this.toDate) {
      return this.stringifyDate(this.fromDate) + ' Au ' + this.stringifyDate(this.toDate);
    }
    else if (this.fromDate) {
      return this.stringifyDate(this.fromDate);
    }
  }

  stringifyDate(date: NgbDate) {
    return date.day + '/' + date.month + '/' + date.year
  }

  deleteDate() {
    this.fromDate = null;
    this.toDate = null;
  }

  parseDateStringAndPushToArray(dateString: string) {
    let sessionToPush = JSON.parse(dateString)
    this.dateArray.push({ toDate: sessionToPush.toDate, fromDate: sessionToPush.fromDate });
  }
}
