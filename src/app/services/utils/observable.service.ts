import { Context } from '../../interfaces/context';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { User } from '../../interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class ObservableService {

  private article = new BehaviorSubject<any>(false);
  currentArticle = this.article.asObservable();

  private drawerTrigger = new BehaviorSubject<boolean>(false);
  currentTrigger = this.drawerTrigger.asObservable();

  private chat = new BehaviorSubject<any>(null);
  currentChat = this.chat.asObservable();

  private context = new BehaviorSubject<Context>(null);
  currentContext = this.context.asObservable();

  private refresh = new BehaviorSubject<boolean>(false);
  currentRefresh = this.refresh.asObservable();

  private logout = new BehaviorSubject<boolean>(false);
  currentLogout = this.logout.asObservable();

  private toBottom = new BehaviorSubject<boolean>(false);
  currentToBottom = this.toBottom.asObservable();

  private getUser = new BehaviorSubject<User>(null);
  currentUser = this.getUser.asObservable();

  private isRecherche = new BehaviorSubject<boolean>(null);
  currentIsRecherche = this.isRecherche.asObservable();

  private isFinancement = new BehaviorSubject<boolean>(null);
  currentIsFinancement = this.isFinancement.asObservable();

  private isMobile = new BehaviorSubject<boolean>(null);
  currentIsMobile = this.isMobile.asObservable();

  triggerMobile(isMobile: boolean) {
    this.isMobile.next(isMobile);
  }

  triggerArticle(message: any) {
    this.article.next(message);
  }

  triggerGetUser(user: User) {
    this.getUser.next(user);
  }

  triggerDrawer(drawerTrigger: boolean) {
    this.drawerTrigger.next(drawerTrigger);
  }

  triggerChat(chat: any) {
    this.chat.next(chat);
  }

  triggerRefresh(refresh: boolean) {
    this.refresh.next(refresh);
  }

  triggerLogout(logout: boolean) {
    this.logout.next(logout);
  }

  triggerToBottom(toBottom: boolean) {
    this.toBottom.next(toBottom);
  }

  triggerContext(context: Context) {
    return this.context.next(context);
  }

  triggerCarousel(isRecherche: boolean) {
    this.isRecherche.next(isRecherche);
  }

  triggerCarouselFinancement(isFinancement: boolean) {
    this.isFinancement.next(isFinancement);
  }
}
