import { Injectable } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class SeoService {

  constructor(
    private meta: Meta,
    private titleService: Title
  ) { }

  generateTags(tags) {
    tags = {
      title: 'IANOS | Formation professionnelle',
      description: 'Trouvez LA formation professionnelle qui répondra parfaitement à votre situation ! Formations éligibles aux CPF, finançables par votre OPCO, Pôle Emploi ou par votre entreprise !"',
      img: 'https://firebasestorage.googleapis.com/v0/b/ianos-9b26a.appspot.com/o/IANOS.jpg?alt=media&token=ca03e661-22aa-4bd0-87f3-0238cac21336',
      ...tags
    }

    tags.title = tags.title
    tags.description = tags.description
    this.titleService.setTitle(tags.title);
    this.meta.updateTag({ name: 'description', content: tags.description })

    this.meta.updateTag({ name: 'twitter:card', content: tags.description });
    this.meta.updateTag({ name: 'twitter:site', content: '@ianos' });
    this.meta.updateTag({ name: 'twitter:title', content: 'Ianos' });
    this.meta.updateTag({ name: 'twitter:description', content: 'Ianos' });

    this.meta.updateTag({ property: 'og:type', content: 'summary' });
    this.meta.updateTag({ property: 'og:site_name', content: 'Ianos' });
    this.meta.updateTag({ property: 'og:title', content: tags.title });
    this.meta.updateTag({ property: 'og:description', content: tags.description });
    this.meta.updateTag({ property: 'og:image', content: tags.img });
  }
}
