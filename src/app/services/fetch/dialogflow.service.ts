import { Message } from './../../interfaces/message';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DialogflowService {

  constructor( ) { }

  async getIntent(message: Message) {
    message.sessionId ? null : message.sessionId = this.getNewSessionId();
    const baseUrl = "https://us-central1-small-talk-e71c1.cloudfunctions.net/getIntent?";
    let url = baseUrl + "message=" + message.content + "&sessionId=" + message.sessionId + "&projectId=bertrand";    
    return await fetch(url);
  }
  
  getNewSessionId() {
    let sessionId = '';
    for (let index = 0; index < 5; index++) {
      sessionId = sessionId + Math.floor((Math.random() * 9) + 1);
    }
    return sessionId;
  }
}
