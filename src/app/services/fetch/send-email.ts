import { SlackService } from './slack.service';
import { User } from './../../interfaces/user';
import { Formation } from './../../interfaces/formations';
import { Injectable } from '@angular/core';
import { Company } from '../../interfaces/company';

@Injectable({
  providedIn: 'root'
})
export class SendEmail {

  constructor(
    private slackService: SlackService
  ) { }

  baseUrl = "https://us-central1-small-talk-e71c1.cloudfunctions.net/";

  // envoi un email
  async sendEmail(emailFrom: string, emailTo: string, subject: string, text: string) {
    emailFrom ? null : emailFrom = 'b.jegoudulaz@ianos.io';
    const url = this.baseUrl + "firestoreEmail?emailFrom=" + emailFrom + "&emailTo=" + emailTo + "&subject=" + subject + "&text=" + text.replace(/\n/g, '');
    return await fetch(url);
  }

  sendMailPreinscription(user: User, formation: Formation) {
    let prenom = '';
    user.prenom ? prenom = user.prenom : prenom = user.email;
    const subject = 'Votre pré-inscription à la formation : ' + formation.intitule;
    const text = `
    <p>Bonjour ` + prenom + `,</p>
    <p>Vous venez de vous pré-inscrire à la formation ` + formation.intitule + ` et nous vous en félicitons. Nous sommes persuadés que vous faites le meilleur choix pour vous et votre carrière !</p>
    <p>Nous vous invitons à continuer la discussion avec ianos afin de trouver vos financements si ce n’est pas déjà fait.</p>
    <p>Pour rappel, votre pré-inscription NE VOUS ENGAGE PAS DU TOUT, ni envers IANOS, ni envers l’organisme de formation, ni envers vous-même. Il s’agit d’un intérêt pour une formation vous laissant le temps de réfléchir, en discuter autour de vous et trouver les financements.</p>
    <p>Pour finaliser votre inscription, nous vous donnons rendez-vous sur votre compte, espace « dashboard », puis cliquer sur le bouton « inscription » correspondant à votre formation.</p>
    <p>Si vous avez des doutes sur la formation pour laquelle vous penser vous engager, n’hésitez pas à en parler autour de vous, vos proches, votre entreprise.</p>
    <p>Nous restons à votre disposition pour en discuter également !!</p>
    <p>A très vite,</p>
    <p>L’équipe IANOS</p>
    `;
    return this.sendEmail('', user.email, subject, text);
  }

  sendMailInscriptionFormation(intituleFormation: string, user: User) {
    let prenom = '';
    user.prenom ? prenom = user.prenom : prenom = user.email;
    const subject = 'Votre inscription à la formation ' + intituleFormation;
    const text = `
    <p>Bonjour ` + prenom + `,</p>
    <p>Vous venez de vous inscrire à la formation ` + intituleFormation + ` et nous vous en félicitons. En vous formant, vous nous garantissez collectivement un avenir. BRAVO !</p>
    <p>Voici vos prochaines démarches :</p>
    <ol>
      <li>L’organisme de formation reçoit une notification lui indiquant votre inscription.</li>
      <li>L’organisme de formation prend contact avec vous pour finaliser les détails et contrats</li>
      <li>Vous suivez votre formation</li>
      <li>Nous vous enverrons un questionnaire à chaud pour savoir comment vous avez vécu cette formation et son intérêt</li>
      <li>Vous pouvez toujours accéder au contenu du programme depuis votre compte, espace dashboard.</li>
    </ol>
    <p>Nous restons à votre disposition pour toute interrogation et vous souhaitons le meilleur !</p>
    <p>Bien à vous,</p>
    <p>L’équipe IANOS</p>
    `;
    return this.sendEmail('', user.email, subject, text);
  }

  sendEmailBienvenue(user: User) {
    let text = '';
    const subject = 'Bienvenue chez IANOS';

    if (user.isOrganisme) {
      text = `<p>Bonjour,</p>
      <p>Vous venez de vous inscrire sur le site ianos.io en tant que prestataire de formation. Toute l’équipe de IANOS vous souhaite le bienvenue et vous remercie de la confiance que vous nous accordez !
      IANOS est un assistant en ligne qui permettra à vos prospects de chercher, comparer, réserver une formation professionnelle et de trouver comment la financer selon sa situation.
      Ainsi ce service vous permet aujourd'hui de prendre plus de temps dans votre métier de formateur et de nous déléguer votre partie commerciale.</p>
      <p>D'autre part, nous travaillons activement pour vous faciliter, ainsi qu'à vos stagiaires, l'administratif qui représente un poids très important de votre quotidien...</p>
      <p>IANOS étant une jeune entreprise, n'hésitez pas à nous faire des retours/critiques afin que nous puissions répondre le plus possible à votre besoin !</p>
      <p>Nous vous sommes totalement dédiés !</p>
      <p>Nous vous remercions à nouveau de votre confiance et vous souhaitons une bonne fin de journée.</p>
      <p>Bien à vous,</p>
      <p>L’équipe IANOS</p>
      `;
    }
    else {
      text = `<p>Bonjour,</p>
      <p>Vous venez de vous inscrire sur le site ianos.io en tant que bâtisseur de notre avenir. Toute l’équipe de IANOS vous souhaite le bienvenue et vous remercie de la confiance que vous nous accordez !
      IANOS est un assistant en ligne qui vous permettra de chercher, comparer, réserver une formation professionnelle et de trouver comment la financer selon votre situation.
      Notre objectif est donc de simplifier vos démarches afin que puissiez rapidement vous former et exprimer vos talents dans votre travail !</p>
      <p>IANOS étant une jeune entreprise, n'hésitez pas à nous faire des retours/critiques afin que nous puissions répondre le plus possible à votre besoin !</p>
      <p>Nous vous sommes totalement dédiés !</p>
      <p>Nous vous remercions à nouveau de votre confiance et vous souhaitons une bonne fin de journée.</p>
      <p>Bien à vous,</p>
      <p>L’équipe IANOS</p>
      `;
    }
    return this.sendEmail('', user.email, subject, text);
  }

  // envoi un email pour deveni membre / employé d'une entreprise
  sendEmailInvitationEmployee(emailEmployee: string, company: Company, rh: User) {
    const subject = "Invitation à faire partie de l'entreprise : " + company.name + " sur www.ianos.io de la part de : " + rh.email;
    const signIn = encodeURIComponent(`eidToAddToUid=` + rh.eid + `&emailToAdd=` + emailEmployee);
    const logIn = encodeURIComponent(`eidToAddToUid=` + rh.eid);

    const text = `
    <p>Bonjour, votre responsable des ressources humaines : ` + this.getDisplayName(rh) + `</p>
    <p>Vous invite à faire partie de l'entreprise : </p> ` + company.name + `
    <p>Si vous avez déjà un compte chez Ianos cliquez ici : <a href="https://ianos.io/connexion?` + logIn + `" target="_blank">Connexion</a></p>
    <p>Si vous souhaitez vous inscrire cliquez ici : <a href="https://ianos.io/inscriptions?` + signIn + `" target="_blank">Inscriptions</a></p>
    <p>Nous vous remercions à nouveau de votre confiance et vous souhaitons une bonne fin de journée.</p>
    <p>Bien à vous,</p>
    <p>L’équipe IANOS</p>
    `;
    return this.sendEmail('', emailEmployee, subject, text);
  }

  // envoi un email quand on est salarié, à son RH
  sendEmailInvitationRh(emailRh: string, employee: User) {
    const subject = `Bonjour je voudrais rejoindre notre entreprise sur www.ianos.io de la part de : ` + employee.email;
    const signIn = encodeURIComponent(`uidToAddToEid=` + employee.id + `&emailToAdd=` + emailRh);
    const logIn = encodeURIComponent(`uidToAddToEid=` + employee.id);

    const text = `
    <p>Bonjour, votre collaborateur : ` + this.getDisplayName(employee) + `</p>
    <p>Souhaite que vous l'invitiez à faire partie de votre entreprise</p>
    <p>Si vous avez déjà un compte entreprise chez Ianos cliquez ici : <a href="https://ianos.io/connexion?` + logIn + `" target="_blank">Connexion</a></p>
    <p>Si vous souhaitez inscrire votre entreprise cliquez ici : <a href="https://ianos.io/inscriptions?` + signIn + `" target="_blank"> Inscriptions</a></p>
    <p>Nous vous remercions à nouveau de votre confiance et vous souhaitons une bonne fin de journée.</p>
    <p>Bien à vous,</p>
    <p>L’équipe IANOS</p>
    `;
    return this.sendEmail('', emailRh, subject, text);
  }

  getDisplayName(user: User) {
    let displayName = '';
    if(user.displayName) {
      displayName = user.displayName;
    }
    else if(user.prenom && user.nom) {
      displayName = user.prenom + ' ' + user.nom;
    }
    else {
      displayName = user.email;
    }
    return displayName
  }
}
