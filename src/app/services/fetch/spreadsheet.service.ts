import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { Message } from './../../interfaces/message';
import { Context } from './../../interfaces/context';
import { Injectable, PLATFORM_ID, Inject } from '@angular/core';
import { User } from '../../interfaces/user';
import { ObservableService } from '../utils/observable.service';
import { Formation } from './../../interfaces/formations';
import { Place } from './../../interfaces/place';
import { getMatIconFailedToSanitizeUrlError } from '@angular/material/icon';


@Injectable({
  providedIn: 'root'
})
export class SpreadsheetService {
  user: User;
  formation: Formation;
  place: Place;

  constructor(
    @Inject(PLATFORM_ID) private platformId: Object,
    private observableService: ObservableService,
    
  ) {
    this.observableService.currentUser.subscribe(user => user ? this.user = user : this.user = null);
  }

  async sendCollection(data: string) {
    const baseUrl = "https://us-central1-small-talk-e71c1.cloudfunctions.net/toSpreadsheet?";
    let url = baseUrl + "data=" + data;     
    return await fetch(url);
  }

  async sendTrainingAsking(context: Context, message: Message) {
    if(!this.isLocalhost()) {
      let optionUrl = ""
      // s'il y a un motCle alors optionURL ('keyword = moCle &) sinon null
      context.motCleArray[0] ? optionUrl = optionUrl + "keyword=" + context.motCleArray[0] + "&" : null;
      context.ville ? optionUrl = optionUrl + "town=" + context.ville + "&" : null;
      context.certification ? optionUrl = optionUrl + "certification=" + context.certification + "&" : null;
      context.modalite ? optionUrl = optionUrl + "modalite=" + context.modalite + "&" : null;
      this.user && this.user.email ? optionUrl = optionUrl + "email=" + this.user.email + "&" : null;
  
      const baseUrl = "https://us-central1-small-talk-e71c1.cloudfunctions.net/rowToSpreadsheet?";
      let url = baseUrl + optionUrl + "sessionId=" + message.sessionId + "&spreadsheetId=1fGBg_EyzNr4cEUdCYhJLnVg30_oteh7_PcS0SAp1pKs"; 
      return await fetch(url);
    }
  }

  async sendFormation(formation: Formation) {

      

      let optionUrl = ""
      this.user && this.user.displayName ? optionUrl = optionUrl + "email=" + this.user.displayName + "&" : null;
      formation.certification ? optionUrl = optionUrl + "certification=" + formation.certification + "&": null;
      formation.modalite ? optionUrl = optionUrl + "modalite=" + formation.modalite + "&" : null;
      formation.motCleArray ? optionUrl = optionUrl + "keyword=" + formation.motCleArray + "&" : null;

      formation.intitule ? optionUrl = optionUrl + "&intitule=" + formation.intitule + "&" : null;
      console.log('TCL: request.query', formation.intitule)

      // formation.cpf ? optionUrl = optionUrl + "cpf=" + formation.cpf + "&": null;
      console.log(formation.motCleArray)

      // this.place.ville ? optionUrl = optionUrl + "town=" + this.place.ville + "&": null;
      // formation.programme ? optionUrl = optionUrl + "programme=" + formation.programme + "&": null;
      // formation.duree ? optionUrl = optionUrl + "duree=" + formation.duree + "&": null;
      // formation.id ? optionUrl = optionUrl + "id=" + formation.id + "&": null;
      // formation.organisme ? optionUrl = optionUrl + "organisme=" + formation.organisme + "&": null;
      

  
      const baseUrl = "https://us-central1-small-talk-e71c1.cloudfunctions.net/rowToSpreadsheet?";
      
      let url = baseUrl + optionUrl + "formation=" + formation.id + "&spreadsheetId=1-6P64rImCY_BOaA892DdxmVs2t5HXx-c5Lc-i-IxW8w"; 
     
      return await fetch(url);
    
  }

  async sendError(error) {
    console.log("TCL: SpreadsheetService -> sendTrainingAsking -> this.platformId", this.platformId)

    console.log("TCL: sendError -> error", error);
    if(this.isExceptError(error) && !this.isLocalhost()) {
      let optionUrl = ""
      // si c'est utilisateur alors "optionUrl" = ...
      this.user && this.user.email ? optionUrl = optionUrl + "email=" + this.user.email + "&" : null;
      // si erreur alors optionUrl = ...
      error && error.code && error.message ? optionUrl = optionUrl + "&error=" + error.message : optionUrl = optionUrl + "&error=" + error;

      const baseUrl = "https://us-central1-small-talk-e71c1.cloudfunctions.net/rowToSpreadsheet?";
      let url = baseUrl + optionUrl + "&location=" + window.location.href + "&spreadsheetId=1fHrtYow5nOlgNRgGiny_PowfjyTQDLR_QG9twI02et4"; 
      return await fetch(url);
    }
  }

  private isLocalhost = () => window.location.hostname === "localhost";

  async getData(spreadsheetId: string) {
    const baseUrl = "https://us-central1-small-talk-e71c1.cloudfunctions.net/fromSpreadsheet?";
    let url = baseUrl + "spreadsheetId=" + spreadsheetId;    
    return await fetch(url);
  }

  isExceptError(error) {
    let isExceptError = true;
    error === "No user logged in" ? isExceptError = false : null;
    return isExceptError;
  }
}
