import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MapService {
  baseUrl = 'https://maps.googleapis.com/maps/api/';
  placeKey = 'AIzaSyCPupgsyDUMYaAkp1FwNHqvemdq-qYxWCQ';
  distanceKey = 'AIzaSyBbMZv5nhIfU06vj5jADF7qmB5KKTb6H7s';
  proxyurl = "https://cors-anywhere.herokuapp.com/";

  // https://maps.googleapis.com/maps/api/place/autocomplete/json?input=1600+Amphitheatre&key=<API_KEY>&sessiontoken=1234567890


  constructor() { }

  async autocomplete(input: string) {
    var url = this.proxyurl + this.baseUrl + 'place/autocomplete/json?input=' + input + '&language=fr&types=geocode&components=country:fr&key=' + this.placeKey;
    return await fetch(url).then(response => response.json());
  }

  async getPlaceId(placeId: string) {
    var url = this.proxyurl + this.baseUrl + 'place/details/json?placeid=' + placeId + '&key=' + this.placeKey;
    return await fetch(url).then(response => response.json());
  }

  async getPlaceWithString(placeName: string) {
    var url = this.proxyurl + this.baseUrl + 'place/findplacefromtext/json?input=' + placeName + '&inputtype=textquery&fields=geometry&key=' + this.placeKey;
    return await fetch(url).then(response => response.json());
  }

  async getPlaceWithLatitudeAndLongitude(position) {
    var url = this.proxyurl + this.baseUrl + 'geocode/json?latlng=' + position.latitude + ',' + position.longitude + '&key=' + this.placeKey;
    // var url = this.proxyurl + this.baseUrl + 'geocode/json?latlng=' + position.latitude + ',' + position.longitude + '&result_type=locality&key=' + this.placeKey;
    return await fetch(url).then(response => response.json());
  }

  async distanceBetweenTwoTown (start: string, end: string) {
    var url = this.proxyurl + this.baseUrl + 'distancematrix/json?origins=' + start + '&destinations=' + end + '&components=country:fr&key=' + this.distanceKey;
    return await fetch(url).then(response => response.json());
  }
}
