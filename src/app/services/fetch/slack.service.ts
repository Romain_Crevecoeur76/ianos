import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SlackService {
  baseUrl: string = "https://slack.com/api/chat.postMessage?token=xoxb-207293447824-534662791698-9mYCTeRVo7AdrFeRb8AHd8IB&channel=";

  sendMessageToSlackToProblemes(message: string) {
    let chanel = "GCCM3RZJ5"; 
    this.isLocalhost() ? null : this.sendMessageToSlack(chanel, message);
  }

  sendMessageToSlackToInscription(message: string) {
    let chanel = "GFR55SMC0";
    this.isLocalhost() ? null : this.sendMessageToSlack(chanel, message);
  }

  sendMessageToSlackToFormation(message: string) {
    let chanel = "CL5TWEKAS";
    this.isLocalhost() ? null : this.sendMessageToSlack(chanel, message);
  }

  sendMessageToSlackToKeywords(message: string) {
    let chanel = "G018C95NVUH";
    this.isLocalhost() ? null : this.sendMessageToSlack(chanel, message);
  }

  private isLocalhost = () => window.location.hostname === "localhost";

  private async sendMessageToSlack(chanel: string, message: string) {
    let url = this.baseUrl + chanel + "&text=" + message + ' Sur ' + window.location.href;
    return await fetch(url);
  }
}
