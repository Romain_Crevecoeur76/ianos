import { AngularFirestore } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { Formation } from '../../interfaces/formations';

@Injectable({
  providedIn: 'root'
})
export class FormationsService {

  constructor(public af: AngularFirestore) { }

  getAllFormations = () => this.af.collection('formations');

  getFormationById = (id: string) => this.af.collection('formations').doc(id);

  getFormationsByUid = (uid: string) => this.af.collection('formations').ref.where('uid', '==', uid);

  getFormationByPid = (pid: string) => this.af.collection('formations').ref.where('pid', '==', pid);

  getFormationsByKeyword = (keyword: string) => this.af.collection('formations').ref.where('keywords', 'array-contains', keyword);

  getFormationsByKeywordLimited = (keyword: string, limit: number) => this.af.collection('formations').ref.where('keywords', 'array-contains', keyword).limit(limit);

  getFormationsByContext(context) {
    let collection = this.af.collection('formations').ref
    context.motCle ? collection = this.addGetFormationsByKeyword(collection, context.motCle) : null;
    context.certification ? collection = this.addGetFormationsByCertification(collection, context.certification.toLowerCase()) : null;
    context.modalite ? collection = this.addGetFormationsByModalite(collection, context.modalite.toLowerCase()) : null;
    return collection;
  }

  addGetFormationsByKeyword = (collection, keyword: string) => collection.where('keywords', 'array-contains', keyword);

  addGetFormationsByModalite(collection, modalite: string) {
    if (modalite === "elearning") {
      return collection.where('modalite', '==', 'Elearning');
    }
    else {
      return collection.where('modalite', '>', 'Elearning');
    }
  }

  addGetFormationsByCertification(collection, certification: string) {
    console.log("TCL: FormationsService -> addGetFormationsByCertification -> certification", certification)
    let isCertifiante = false;
    certification.toLocaleLowerCase().includes("avec certification") ? isCertifiante = true : null;
    if (certification.toLocaleLowerCase().includes("avec ou sans certification")) {
      return collection
    }
    return collection.where('certification', '==', isCertifiante);
  }

  addFormation = (formation: Formation) => this.af.collection('formations').ref.add(formation);

  deleteFormation = (formation: Formation) => this.af.collection('formations').doc(formation.id).delete();

  updateFormation = (formation: Formation) => this.af.collection('formations').doc(formation.id).update(formation);


}