import { Injectable } from '@angular/core';
import { User } from '../../interfaces/user';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private af: AngularFirestore) { }

  getUsersByEid = (eid: string) => this.af.collection('user').ref.where('eid', '==', eid);

  getUserByEmail = (email: string) => this.af.collection('user').ref.where('email', '==', email);

  getUserById = (id: string) => this.af.collection('user').doc(id);

  getOrganismes = () => this.af.collection('user').ref.orderBy('isOrganisme').startAt(true);

  createUser = (user: User) => this.af.collection('user').doc(user.id).set(user);

  addTrainer = (user: User) => this.af.collection('user').add({ isTrainer: true, ...user});

  deleteUser = (uid: string) => this.af.collection('user').doc(uid).delete();

  updateUser = (user: User) => this.af.collection('user').doc(user.id).update(user);


}
