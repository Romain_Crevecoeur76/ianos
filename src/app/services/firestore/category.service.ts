import { Category } from './../../interfaces/category';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(public af: AngularFirestore) { }

  getCategoryById = (titleUrl: string) => this.af.collection('category').doc(titleUrl);

  getCategories = () => this.af.collection('category');

  deleteCategory = (category: Category) => this.af.collection('category').doc(category.titleUrl).delete();

  addCategory = (category: Category) => this.af.collection('category').doc(category.titleUrl).set(category);

  updateCategory = (category: Category) => this.af.collection('category').doc(category.titleUrl).update(category);

}
