import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class EntitiesService {

  constructor(public af: AngularFirestore) { }

  getMotCleEntities = () => this.af.collection('dialogflow').doc('mot_cle');

  getSecteurEntities = () => this.af.collection('dialogflow').doc('Secteur');

  getSecteurActivitePrincipale = () => this.af.collection('dialogflow').doc('activite_principale');

  getRegions = () => this.af.collection('dialogflow').doc('region');

}
