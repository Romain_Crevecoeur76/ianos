import { Injectable } from '@angular/core';
import { Session } from './../../interfaces/session';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  constructor(public af: AngularFirestore) { }

  getSessionById = (id: string) => this.af.collection('session').doc(id);

  getSessionsByFid = (fid: string) => this.af.collection('session').ref.where('fid', '==', fid);

  getSessionsByPid = (pid: string) => this.af.collection('session').ref.where('pid', '==', pid);

  getSessionsByUid = (uid: string) => this.af.collection('session').ref.where('uid', '==', uid);

  addSession = (session: Session) => this.af.collection('session').add(session);

  deleteSessionById = (sessionId: string) => this.af.collection('session').doc(sessionId).delete();

  deleteSession = (session: Session) => this.af.collection('session').doc(session.id).delete();

  updateSession = (session: Session) => this.af.collection('session').doc(session.id).update(session);
}
