import { Booking } from './../../interfaces/booking';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Formation } from '../../interfaces/formations';
import { User } from '../../interfaces/user';
import { SlackService } from '../fetch/slack.service';

@Injectable({
  providedIn: 'root'
})
export class BookingService {

  constructor(
    private af: AngularFirestore,
    private slackService: SlackService
  ) { }

  getBookingById = (id: string) => this.af.collection('booking').doc(id);

  getPropositionsByEid = (eid: string) => this.af.collection('booking').ref.where('eid', '==', eid).orderBy('isProposition').startAt(true);

  getPropositionsByEidAndMemberIncluded = (eid: string, uid: string) => {
    return this.af.collection('booking').ref.where('eid', '==', eid).where('members', 'array-contains', uid).orderBy('isProposition').startAt(true);
  }

  getPropositionsByUid = (uid: string) => this.af.collection('booking').ref.where('uid', '==', uid).orderBy('isProposition').startAt(true);
 
  getFavoritesByUid = (uid: string) => this.af.collection('booking').ref.where('uid', '==', uid).orderBy('isFavorite').startAt(true);

  getRequestsByEid = (eid: string) => this.af.collection('booking').ref.where('eid', '==', eid).orderBy('isRequest').startAt(true);

  getPendingsByEid = (eid: string) => this.af.collection('booking').ref.where('eid', '==', eid).orderBy('isPending').startAt(true);

  getPendingsByUid = (uid: string) => this.af.collection('booking').ref.where('uid', '==', uid).orderBy('isPending').startAt(true);

  getPendingsByOfid = (ofid: string) => this.af.collection('booking').ref.where('ofid', '==', ofid).orderBy('isPending').startAt(true);

  getBookingsByOfid = (ofid: string) => this.af.collection('booking').ref.where('ofid', '==', ofid).orderBy('isBooked').startAt(true);

  getBookedByUid = (uid: string) => this.af.collection('booking').ref.where('uid', '==', uid).orderBy('isBooked').startAt(true);

  getBookingsByEid = (eid: string) => this.af.collection('booking').ref.where('eid', '==', eid).orderBy('isBooked').startAt(true);

  getBookings = () => this.af.collection('booking');

  deleteBooking = (booking: Booking) => this.af.collection('booking').ref.doc(booking.id).delete();

  addBooking = (booking: Booking) => this.af.collection('booking').ref.add(booking);

  private resetBooking = (booking: Booking) => {
    booking = {
      ...booking,
      isFavorite: false,
      isProposition: false,
      isRequest: false,
      isPending: false,
      isBooked: false,
      isArchived: false
    }
    return booking;
  }

  fillBooking = (formation: Formation, user: User) => {
    let reservation = {
      ofid: formation.uid,
      fid: formation.id,
      intituleFormation: formation.intitule,
      uid: user.id,
      members: [ this.createMember(user) ],
      email: user.email
    } as Booking;
    formation.ville ? reservation.ville = formation.ville : null;
    formation.pid ? reservation.pid = formation.pid : null;
    formation.sid ? reservation.sid = formation.sid : null;
    user.eid ? reservation.eid = user.eid : null;
    this.slackService.sendMessageToSlackToProblemes("Création du favoris : " + reservation.id);
    return reservation;
  }

  createMember(user: User) {
    let member = {
      id: user.id,
      email: user.email,
      prenom: user.prenom,
      role: 'Salarié',
      formation: false,
      date: false,
      finance: false,
      theme: false
    }
    user.role ? member.role = user.role : null;
    return member;
  }

  toPending = (booking: Booking) => {
    this.slackService.sendMessageToSlackToProblemes("La réservation : " + booking.id + " passe de favoris/proposition/demande à en cours de validation OF");
    return { ...this.resetBooking(booking), isPending: true };
  }

  toProposition = (booking: Booking) => {
    this.slackService.sendMessageToSlackToProblemes("La réservation : " + booking.id + " passe de favoris à en cours de proposition");
    return { ...this.resetBooking(booking), isProposition: true };
  }

  toRequest = (booking: Booking) => {
    this.slackService.sendMessageToSlackToProblemes("La réservation : " + booking.id + " passe de favoris à en cours de demande");
    return { ...this.resetBooking(booking), isRequest: true };
  }

  toBooked = (booking: Booking) => {
    this.slackService.sendMessageToSlackToProblemes("La réservation : " + booking.id + " a été validée par l'OF");
    return { ...this.resetBooking(booking), isBooked: true };
  }

  toArchived = (booking: Booking) => {
    this.slackService.sendMessageToSlackToProblemes("La réservation : " + booking.id + " a été archivée par l'utilisateur");
    return { ...this.resetBooking(booking), isArchived: true };
  }

  isAlreadyBooked = (uid: string, fid: string) => {
    return this.af.collection('booking').ref.where('uid', '==', uid).where('fid', '==', fid)
  }

  updateBooking = (booking: Booking) => {
    return this.af.collection('booking').ref.doc(booking.id).update(booking)
  }
}
