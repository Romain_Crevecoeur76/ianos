import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Review } from '../../interfaces/review';

@Injectable({
  providedIn: 'root'
})
export class ReviewService {

  constructor(public af: AngularFirestore) { }

  getReviews = () => this.af.collection('review');

  getReviewByFid = (fid: string) => this.af.collection('review').ref.where('fid', '==', fid);

  getReviewByBid = (bid: string) => this.af.collection('review').ref.where('bid', '==', bid);

  deleteReview = (review: Review) => this.af.collection('review').ref.doc(review.id).delete();

  addReview = (review: Review) => this.af.collection('review').ref.add(review);

  updateReview = (review: Review) => this.af.collection('review').ref.doc(review.id).update(review);
}
