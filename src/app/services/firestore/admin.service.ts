import { SlackService } from '../fetch/slack.service';
import { Injectable } from '@angular/core';
import { Context } from '../../interfaces/context';
import { AngularFirestore } from '@angular/fire/firestore';
import { Formation } from 'src/app/interfaces/formations';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(
    private slackService: SlackService,
    public af: AngularFirestore
  ) { }

  getAllUsers = () => this.af.collection('user').ref.limit(5);

  getAllNewSessions = () => this.af.collection('session').ref.where('startDate', '>', new Date()).orderBy('startDate').limit(15);

  getAllOldSessions = () => this.af.collection('session').ref.where('startDate', '<', new Date()).orderBy('startDate').limit(15);

  getAll0rgannismes = () => this.af.collection('user').ref.where('isOrganisme', '==', true);

  getAllFormations = () => this.af.collection('formation').ref.where('formation', '==', true);

  getAllStagiaires = () => this.af.collection('user').ref.where('isStagiaire', '==', true);

  getAllMotCles = () => this.af.collection('dialogflow').doc('statsMotsCles');

  resetMotCleStatsCounts = () => {
    this.getAllMotCles().get().toPromise()
      .then(motCle => {
        let motCleToUpdate = motCle.data()['keywordToShow'];
        motCleToUpdate.forEach(motCle => motCle.count = 0);
        this.updateMotCle(motCleToUpdate);
      })
      .catch(error => console.log('TCL: AdminService -> resetMotCleStatsCounts -> error', error));
  }

  resetMotCleStatCount(keyword: string) {
    this.getAllMotCles().get().toPromise()
      .then(motCle => {
        let motCleToUpdate = motCle.data()['keywordToShow'];
        motCleToUpdate.forEach(motCle => motCle.keyword === keyword ? motCle.count = 0 : null);
        this.updateMotCle(motCleToUpdate);
      })
      .catch(error => console.log('TCL: AdminService -> resetMotCleStatCount -> error', error));
  }

  updateMotCle(motCle) {
    this.af.collection('dialogflow').doc('statsMotsCles').update({ keywordToShow: motCle });
  }

  updateContext(context: Context) {
    console.log("TCL: AdminService -> updateContext -> context", context)
    // this.af.collection('dialogflow').doc('statsContexts').collection('contexts').add(context);

    this.updateMotCleStats(context['mot_cle']);
  }

  updateMotCleStats(motCle: string) {
    this.af.collection('dialogflow').doc('statsMotsCles').get().toPromise()
      .then(keywords => {
        let keywordToShow = [];
        let isMotCle: boolean;
        if (keywords.exists) {
          let keywordToShow = keywords.data()['keywordToShow']
          keywordToShow.forEach((keyword, index) => {
            if (keyword.keyword == motCle) {
              keywordToShow[index].count++;
              this.countTooHigh(motCle, keywordToShow[index].count);
              isMotCle = true;
            }
          })
          isMotCle ? null : keywordToShow.push({ keyword: motCle, count: 1 });
          this.af.collection('dialogflow').doc('statsMotsCles').update({ keywordToShow })
        }
        else {
          keywordToShow.push({ keyword: motCle, count: 1 })
          this.af.collection('dialogflow').doc('statsMotsCles').set({ keywordToShow })
        }
      })
  }

  countTooHigh(motCle: string, count: number) {
    count > 400 ? this.slackService.sendMessageToSlackToProblemes("Beaucoup de requêtes sur le mot clé : " + motCle + " Nombre de fois : " + count) : null;
  }


  updateAllFormation = (formation: Formation) => this.af.collection('formations').doc(formation.id).update(formation)

}