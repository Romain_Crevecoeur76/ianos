  
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { todolist } from './../../interfaces/todolist';


@Injectable({
  providedIn: 'root'
})

export class TodoService{

  
  constructor(private af: AngularFirestore) {}

  createTodo = (Todolist: todolist) => this.af.collection('todolist').ref.add(Todolist);

  // createTask = (Task: todolist) => this.af.collection('todolist').ref.add(Task);
  
  updateTodo = (Todolist: todolist) => this.af.collection('todolist').doc(Todolist.id).update(Todolist); 

  getTodoList = () => this.af.collection('todolist');
   
  // getToDoListById = (id: string) => this.af.collection('todolist').doc(id);

  getToDoByid = (id: string) => this.af.collection('todolist').ref.where('id', '==', id);

  
}
