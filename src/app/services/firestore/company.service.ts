import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Company } from '../../interfaces/company';


@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  constructor(public af: AngularFirestore) { }

  getCompanies = () => this.af.collection('company');

  getCompanyById = (id: string) => this.af.collection('company').doc(id);

  deleteCompany = (company: Company) => this.af.collection('company').ref.doc(company.id).delete();

  createCompany = (company: Company) => this.af.collection('company').ref.add(company);

  createCompanyID = (company: Company) => this.af.collection('company').doc(company.id).set(company);

  updateCompany = (company: Company) => this.af.collection('company').ref.doc(company.id).update(company);

  updateMember = (member:Company) => this.af.collection('company').ref.doc(member.id).update(member);
  

}
