import { Injectable } from '@angular/core';
import { Article } from '../../../../../ianos/src/app/interfaces/article';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class BlogService {

  constructor(public af: AngularFirestore) { }

  getArticleById = (urlTitle: string) => this.af.collection('blog').doc(urlTitle);

  getBlog = () => this.af.collection('blog');

  deleteArticle = (article: Article) => this.af.collection('blog').doc(article.titleUrl).delete();

  addArticle = (article: Article) => this.af.collection('blog').doc(article.titleUrl).set(article);

  updateArticle = (blog: Article) => this.af.collection('blog').doc(blog.titleUrl).update(blog);

}
