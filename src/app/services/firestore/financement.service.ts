import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class FinancementService {

  constructor(private af: AngularFirestore) { }

  getAllFinancements() {
    return this.af.collection('financement');
  }

  getFinancementById(id: string) {
    return this.af.collection('financement').doc(id)
  }

}
