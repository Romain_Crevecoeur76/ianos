import { Place } from './../../interfaces/place';
import { Injectable } from '@angular/core';
import firebase from 'firebase/app'
import * as geofirex from 'geofirex';
// import { GeoPoint } from '@firebase/firestore-types';
import { AngularFirestore } from '@angular/fire/firestore';

const geo = geofirex.init(firebase);

@Injectable({
  providedIn: 'root'
})
export class PlaceService {

  constructor(public af: AngularFirestore) { }

  getPlacesById = (id: string) => this.af.collection('place').doc(id);

  getPlacesByFid = (fid: string) => this.af.collection('place').ref.where("fid", "==", fid);

  getPlacesAskingDate = (uid: string, toAsk: string) => this.af.collection('place').ref.where('uid', '==', uid).orderBy(toAsk).startAt('');

  getPlacesByTown = (town: string) => this.af.collection('place').ref.where('ville', '==', town);

  deletePlaceById = (placeId: string) => this.af.collection('place').doc(placeId).delete();

  updatePlace = (place: Place) => this.af.collection('place').doc(place.id).update(place);

  addPlace = (place: Place) => {
    place.location ? place.geoPoint = new firebase.firestore.GeoPoint(place.location.latitude, place.location.longitude) : null;
    return this.af.collection('place').add(place);
  }

  fillGeoPoint = (latitude: number, longitude: number) => new firebase.firestore.GeoPoint(latitude, longitude);

  distance = (start: firebase.firestore.GeoPoint, end: firebase.firestore.GeoPoint) => geo.point(start.latitude, start.longitude).distance(end.latitude, end.longitude);

}
