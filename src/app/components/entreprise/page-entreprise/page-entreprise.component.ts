import { Component, OnInit } from '@angular/core';
import { SignInComponent } from './../../guest/sign/sign-in/sign-in.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SlackService } from './../../../services/fetch/slack.service';


@Component({
  selector: 'page-entreprise',
  templateUrl: './page-entreprise.component.html',
  styleUrls: ['./page-entreprise.component.scss']
})
export class PageEntrepriseComponent implements OnInit {

  constructor(
    private modalService: NgbModal,
    private slackService: SlackService,
  ) { }

  ngOnInit() {
  }


  open(){
    this.modalService.dismissAll(); 
    this.modalService.open(SignInComponent, { size: 'lg', centered: true});
    this.slackService.sendMessageToSlackToInscription("tentative d'inscription page entreprise");
  }
}
