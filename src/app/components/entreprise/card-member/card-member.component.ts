import { Component, OnInit, Input, Inject } from '@angular/core';
import { CompanyService } from './../../../services/firestore/company.service';
import { Company } from '../../../interfaces/company';
import { User } from '../../../interfaces/user';
import { UsersService } from '../../../services/firestore/users.service';
import { ObservableService } from '../../../services/utils/observable.service';
import { ActivatedRoute } from '@angular/router';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SlackService } from './../../../services/fetch/slack.service';
// ajout de mots clés
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {MatChipInputEvent} from '@angular/material/chips';
// fin ajout de mots clés

// datepicker
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS, MatMomentDateModule } from '@angular/material-moment-adapter'; 
import * as _moment from 'moment';
const moment = _moment;



@Component({
  selector: 'app-card-member',
  templateUrl: './card-member.component.html',
  styleUrls: ['./card-member.component.scss'],
  providers: [{provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: {useUtc: true}}],
})


export class CardMemberComponent implements OnInit {

  // expension panel
  panelOpenState = false;
  
  // base du contenu
  prenom: Company;
  member: Company;
  members: User[] = [];
  user: User;
  company: Company;
  isRh: boolean = true;

   // ajout de mots clés
   visible = true;
   selectable = true;
   removable = true;
   addOnBlur = true;
   readonly separatorKeysCodes: number[] = [ENTER, COMMA];
   userKeyword: string;
   userKeywords = [];
   // fin ajout de mots clés

  // datePicker
  campaignOne: FormGroup;
  campaignTwo: FormGroup;
  startDisponibility: Date;
  EndDisponibility: Date;
  // fin

  constructor(
    @Inject(MAT_DIALOG_DATA) 
    private dataMember: Company,
    private snackBar: MatSnackBar,
    public observableService: ObservableService,
    public companyService: CompanyService,
    public usersService: UsersService,
    private dialog: MatDialog,
    private route: ActivatedRoute,
    private slackService: SlackService,
    
    
  ){
    const today = new Date();
    const month = today.getMonth();
    const year = today.getFullYear();
    
    this.campaignOne = new FormGroup({
      start: new FormControl(null, Validators.required),  
      end: new FormControl(null, Validators.required) 
    });

    this.campaignTwo = new FormGroup({
      start: new FormControl(new Date(year, month, 15)),
      end: new FormControl(new Date(year, month, 19))
    });

    
    
  }

  ngOnInit() {
    this.dataMember ? this.member = this.dataMember : null;
    this.user = this.member;
    
    this.member.campaignOne = this.campaignOne.value
    this.member.campaignTwo = this.campaignTwo.value

    console.log(this.campaignOne.value.start)
    console.log(this.member.campaignOne)

    // console.log(this.campaignTwo)
    // console.log(this.member.campaignTwo)
  }

  // enregistre les données du membre
  onSubmitMember(user: User){
    this.updateUser(user).then(() => this.openSnackBar(user.prenom + " " + user.nom + " modifié(e)", "Valider"));
  }
    
  updateUser = (user: User) => this.usersService.updateUser(user).catch();
  openSnackBar = (message: string, action: string) => this.snackBar.open(message, action, { duration: 10000 });

  // ferme la modal
  close = () => this.dialog.closeAll();

  
  
  // ajout de mots clés
  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    this.user.userKeywords = this.userKeywords;
    
    // Add motCle
    if ((value || '').trim()) {
      this.user.userKeywords.push(value.trim());
      this.slackService.sendMessageToSlackToFormation('Nouveau mot clé : ' + value + "" + this.user.email);

    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }
  remove(userKeyword: string): void {
    const index = this.user.userKeywords.indexOf(userKeyword);
    console.log(userKeyword)

    if (index >= 0) {
      this.user.userKeywords.splice(index, 1);
    }
  }
}
