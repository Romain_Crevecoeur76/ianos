import { BookingService } from './../../../services/firestore/booking.service';
import { UsersService } from './../../../services/firestore/users.service';
import { Booking } from './../../../interfaces/booking';
import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSelectionList } from '@angular/material/list';
import { MatSnackBar } from '@angular/material/snack-bar';
import { User } from '../../../interfaces/user';
import { ObservableService } from '../../../services/utils/observable.service';

@Component({
  selector: 'list-member',
  templateUrl: './list-member.component.html',
  styleUrls: ['./list-member.component.scss']
})
export class ListMemberComponent {

  membersCompany: User[] = [];
  user: User;
  propositionOwner: User;
  rhUser: User;
  email: string;
  selectedOptions: string[] = [];

  constructor(
    @Inject(MAT_DIALOG_DATA) public booking: Booking,
    private observableService: ObservableService,
    private bookingService: BookingService,
    private usersService: UsersService,
    private snackBar: MatSnackBar
  ) {
    this.observableService.currentUser.subscribe(user => user && !this.user ? this.user = user : null);
    this.booking.rhid ? this.getRh() : this.getPropositionOwner();
    this.booking.eid ? this.getCompanyMembers() : null;
  }

  getCompanyMembers() {
    this.usersService.getUsersByEid(this.booking.eid)
      .onSnapshot(query => {
        query.docChanges().forEach(user => {
          let userToShow = { id: user.doc.id, ...user.doc.data() as User};
          this.booking.members.forEach(member => member.id === userToShow.id ? userToShow.isMemberProposition = true : null);
          this.booking.rhid !== userToShow.id && !this.membersCompany.includes(userToShow) ? this.membersCompany.push(userToShow) : null;
        })
      })
  }
  getPropositionOwner = () => this.usersService.getUserById(this.booking.uid).snapshotChanges().subscribe(user => this.propositionOwner = user.payload.data() as User);

  getRh = () => this.usersService.getUserById(this.booking.rhid).snapshotChanges().subscribe(user => this.rhUser = user.payload.data() as User);

  onNgModelChange(list: MatSelectionList) {
    this.selectedOptions.forEach(uid => this.booking.members.some(member => uid === member.id) ? null : this.addMember(uid));
    list.selectedOptions.changed.subscribe(option => option.removed.forEach(remove => this.booking.members = this.booking.members.filter(member => member.id !== remove.value)));
    this.bookingService.updateBooking(this.booking);
  }

  addGuest(guest) {
    this.usersService.getUserByEmail(guest.value)
      .onSnapshot(query => {
        if (query.empty) {
          this.booking.members.push(this.bookingService.createMember({ email: guest.value }));
        }
        else {
          query.forEach(user => this.booking.members.push(this.bookingService.createMember({ id: user.id, ...user.data() as User })));
        }
      })
    this.bookingService.updateBooking(this.booking)
    .then(() => this.snackBar.open("Membre ajouté à la réservation  ", "Valider"))
    console.log("TCL: ListMemberComponent -> addGuest -> this.booking", this.booking)
  }

  addMember = (uid: string) => this.usersService.getUserById(uid).snapshotChanges().subscribe(user => this.booking.members.push(this.bookingService.createMember(user.payload.data() as User)));
}
