import { SpreadsheetService } from './../../../services/fetch/spreadsheet.service';
import { CompanyService } from './../../../services/firestore/company.service';
import { Company } from '../../../interfaces/company';
import { User } from '../../../interfaces/user';
import { UsersService } from '../../../services/firestore/users.service';
import { SendEmail } from '../../../services/fetch/send-email';
import { ObservableService } from '../../../services/utils/observable.service';
import { CardMemberComponent } from '../../entreprise/card-member/card-member.component';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { FormGroup, FormControl } from '@angular/forms';
import { RequestComponent } from '../../user/request/request.component';
import { TodoService } from './../../../services/firestore/todolist.service';
import { newArray } from '@angular/compiler/src/util';



@Component({
  selector: 'espace-entreprise',
  templateUrl: './espace-entreprise.component.html',
  styleUrls: ['./espace-entreprise.component.scss']
})
export class EspaceEntrepriseComponent implements OnInit {

  member: Company;
  members: User[] = [];
  user: User;
  company: Company;
  isRh: boolean = true;
  CardMemberComponent: CardMemberComponent;

  displayedColumns: string[] = ['change', 'trainingPriority', 'membre', 'function', 'need', 'delete'];
  dataSource = new MatTableDataSource(this.members);  



  constructor(
    
    private snackBar: MatSnackBar,
    public observableService: ObservableService,
    public spreadsheetService: SpreadsheetService,
    public companyService: CompanyService,
    public usersService: UsersService,
    public sendEmail: SendEmail,
    private dialog: MatDialog,    
    private todoService:TodoService,


  ) {
    // user connecté = true alors récupère les données user entreprise
    this.observableService.currentUser.subscribe(user => user && !this.user ? this.getCompany(user) : null); 
  }
  
  ngOnInit() {  
    
   }

  //  récupère l'entreprise et ses membres (uniquement ID)
  // si user id alors fonction getUsers()
  getCompany(user: User) {
    this.user = user;
    if (this.user.eid) {
      this.companyService.getCompanyById(this.user.eid).get().subscribe(company => {
          if (!this.company) {
            this.company = { id: company.id, ...company.data() as Company};
            this.company.members ? this.getUsers() : null;
            // this.getNumMember();
          }
        },
        )
    }
    
  }

  // récupère les membres de l'entreprise
  getUsers() {
    this.company.members.forEach(uid => {
      // company.members = Array of ID
      this.usersService.getUserById(uid).valueChanges().subscribe((user: User) => {
          let isUserMember = false;
          this.members.forEach(member => {
            if (member.id === user.id) {
              member = user;
              isUserMember = true;
            }
          })
          isUserMember ? null : this.members.push(user);
          user.role === "RH" ? this.isRh = true : this.isRh = false;  
          this.dataSource = new MatTableDataSource(this.members);
        });
    });
  }

  onSubmit() {
    this.companyService.updateCompany(this.company)
  }

  // envoi une invitation pour être membre / employé
  sendInviteEmail(emailToInvite) {
    this.openSnackBar("Envoie du mail en cours à : " + emailToInvite.value, "Valider")
    this.sendEmail.sendEmailInvitationEmployee(emailToInvite.value, this.company, this.user)
      .then(() => this.openSnackBar("Mail envoyé avec succès à : " + emailToInvite.value, "Valider"))
      .catch(error => {
        this.openSnackBar("Erreur dans l'envoie du mail : " + emailToInvite.value, "Valider");
        this.spreadsheetService.sendError(error);
      });
    return emailToInvite.value = '';

  }

  openSnackBar = (message: string, action: string) => this.snackBar.open(message, action, { duration: 10000 });

  // Fonction permettant de donner des droits aux users selon le rôle
  // isRh = accède au dashboard + peut réserver des formations
  // isManager = peut réserver des formations
  // isEmployee = peut réserver des formations et son profil est accessible au RH
  changeRole(user: User) {
    this.company.employees ? null : this.company.employees = [];
    this.company.rh ? null : this.company.rh = [];
    this.company.managers ? null : this.company.managers = [];

    this.company.employees.includes(user.id) ? this.company.employees = this.company.employees.filter(uid => uid !== user.id) : null;
    this.company.rh.includes(user.id) ? this.company.rh = this.company.rh.filter(uid => uid !== user.id) : null;
    this.company.managers.includes(user.id) ? this.company.managers = this.company.managers.filter(uid => uid !== user.id) : null;

    switch (user.role) {
      case 'Salarié':
        this.company.employees.push(user.id);
        break;
      case 'RH':
        this.company.rh.push(user.id);
        break;
      case 'Manager':
        this.company.managers.push(user.id);
        break;
    }

    // lorsue le rôle est changé, l'entreprise s'update et l'utilisateur également
    this.companyService.updateCompany(this.company).then(() => {
        this.updateUser(user)
          .then(() => this.openSnackBar("Rôle modifié pour l'utilisateur : " + user.email, "Valider"));
      })
      .catch(error => this.spreadsheetService.sendError(error));
  }

  updateUser = (user: User) => this.usersService.updateUser(user).catch(error => this.spreadsheetService.sendError(error));

  updateMember = (member:User) => this.companyService.updateMember(member);

  updateCompany() {
    this.companyService.updateCompany(this.company)
      .then(() => this.openSnackBar("Entreprise mise à jour avec succès", "Valider"))
      .catch(error => this.spreadsheetService.sendError(error));
  }

  // supprime le membre de l'entreprise
  removeFromCompany(user: User) {
    this.company.members = this.company.members.filter(uid => uid !== user.id);
    delete user.eid;
    delete user.company;
    this.updateUser(user);
    this.companyService.updateCompany(this.company);
    this.openSnackBar(this.user.prenom + "a été supprimé", "Valider")
  }


  // supprime l'entreprise et ses membres
  deleteCompany() {
    this.companyService.deleteCompany(this.company)
      .then(() => {
        this.openSnackBar("Entreprise supprimée avec succès", "Valider")
        this.members.forEach(member => {
          delete member.eid;
          this.usersService.updateUser(member);
          this.observableService.triggerLogout(true);
        })
      })
      .catch(error => this.spreadsheetService.sendError(error));

  }

  // ouvre le profil du membre
  open(member: Company): void {
    let dialogRef = this.dialog.open(CardMemberComponent, {
      minHeight: '500px',
      minWidth: '1000px',
      data: member,
      closeOnNavigation: true,
      disableClose: true,
    });

    dialogRef.backdropClick().subscribe(() => this.snackBar.open('Membre non sauvegardée voulez vous continuer ? ', 'Valider')
    .onAction().subscribe(() => dialogRef.close()));
  }




  

  
  
}
