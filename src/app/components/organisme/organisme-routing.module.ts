import { AddCatalogueComponent } from './add-catalogue/add-catalogue.component';
import { AddFormationComponent } from './add-formation/add-formation.component';
// import { ListeMembreComponent } from './liste-membre/liste-membre.component';
import { AuthGuardAdmin } from './../../services/auth/auth.guard-admin';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MonCatalogueComponent } from './mon-catalogue/mon-catalogue.component';
import { AuthGuardOrganisme } from './../../services/auth/auth.guard-organisme';
import { BillComponent } from './bill/bill.component';
import { FactureComponent } from './facture/facture.component';


const organismeRoutes: Routes = [
  {
    path: '',
    canActivate: [AuthGuardOrganisme],
    children: [
      {
        path: '',
        canActivate: [AuthGuardOrganisme],
        children: [
          // { path: 'liste-membre', component: ListeMembreComponent },
          { path: 'facture', component: FactureComponent },
          { path: 'bill', component: BillComponent },
          {
            path: 'mon-catalogue', component: MonCatalogueComponent,
            children: [{ path: ':uid', canActivate: [AuthGuardAdmin], component: MonCatalogueComponent }],
          },
          { path: 'ajouter-formation', component: AddFormationComponent },
          { path: 'ajouter-catalogue', component: AddCatalogueComponent },
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(organismeRoutes)],
  exports: [RouterModule]
})
export class OrganismeRoutingModule { }
