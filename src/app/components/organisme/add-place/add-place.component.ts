import { CalendarService } from '../../../services/utils/calendar.service';
import { FormationsService } from './../../../services/firestore/formations.service';
import { PlaceService } from './../../../services/firestore/place.service';
import { MapService } from './../../../services/fetch/map.service';
import { SessionService } from './../../../services/firestore/session.service';
import { FormControl } from '@angular/forms';
import { Formation } from './../../../interfaces/formations';
import { Place } from './../../../interfaces/place';
import { Component, Input, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'add-place',
  templateUrl: './add-place.component.html',
  styleUrls: ['./add-place.component.scss']
})
export class AddPlaceComponent implements OnInit {
  @Input() 
  formation: Formation;
  placeToAdd: Place = {};
  places: Place[];
  predictions: Array<any> = [];
  myControl = new FormControl();
  isResetCalendar = false;

  constructor(
    private mapService: MapService,
    private sessionService: SessionService,
    private placeService: PlaceService,
    private formationsService: FormationsService,
    private calendarService: CalendarService,
    public snackBar: MatSnackBar,
  ) { }

  ngOnInit() {
    this.getPlaces()
  };

  getPlaces() {
    this.places = [];
    this.placeService.getPlacesByFid(this.formation.id)
      .onSnapshot(query => {
        query.docChanges().forEach(place => {
          const placeToShow = { id: place.doc.id, ...place.doc.data() as Place};
          if (place.type === "added") {
            let isPlaceAlreadyThere = false;
            this.places.forEach(session => session.id === placeToShow.id ? isPlaceAlreadyThere = true : null);
            isPlaceAlreadyThere ? null : this.places.push(placeToShow)
          }
          if (place.type === "removed") {
            this.places.forEach((placeToDelete, index) => {
              placeToDelete.id == placeToShow.id ? this.places.splice(index, 1) : null;
            });
          }
        })
      });
  }

  deletePlace(placeToDelete: Place) {
    this.sessionService.getSessionsByPid(placeToDelete.id).get()
      .then(query => query.empty ? null : query.forEach(sessionToDelete => {
        this.sessionService.deleteSessionById(sessionToDelete.id);
        this.formation.sessionArray.filter(session => session !== sessionToDelete.id);
      }))
      .catch(error => console.log("​AddPlaceComponent -> deletePlace -> error", error));
    this.placeService.deletePlaceById(placeToDelete.id);
    this.formation.placeArray = this.formation.placeArray.filter(place => place.id !== placeToDelete.id);
    this.formationsService.updateFormation(this.formation);
    this.openSnackBar(placeToDelete.ville + ' supprimée', 'Valider');
  }

  onSubmitPlace(placeToAdd: Place) {
    if (placeToAdd.ville && this.formation.id) {
      placeToAdd = {
        fid: this.formation.id,
        uid: this.formation.uid,
        organisme: this.formation.organisme,
        intituleFormation: this.formation.intitule,
        ...placeToAdd
      }
      placeToAdd.id ? this.updatePlace(placeToAdd) : this.addPlace(placeToAdd);
    }
    else {
      this.openSnackBar("Merci d'enregistrer l'intitulé de la formation.", 'Valider');
    }
    this.getPlaces();
  }

  updatePlace(placeToUpdate: Place) {
    this.placeService.updatePlace(placeToUpdate)
      .then(() => {
        this.updateFormation(placeToUpdate);
        this.openSnackBar(placeToUpdate.ville + ' modifiée', 'Valider');
      })
      .catch(error => console.log('TCL: AddPlaceComponent -> onSubmitPlace -> error', error));
  }

  addPlace(placeToAdd: Place) {
    this.placeService.addPlace(placeToAdd)
      .then(query => query.get()
        .then(place => {
          this.openSnackBar(placeToAdd.ville + ' correctement créée', 'Valider')
          this.updateFormation({ id: place.id, ...placeToAdd as Place});
        }))
      .catch(error => console.log("​AddFormationComponent -> addPlace -> error", error));
  }

  updateFormation(placeToAdd: Place) {
    let placeToPushToFormation = {
      id: placeToAdd.id,
      ville: placeToAdd.ville,
      region: placeToAdd.region,
      departement: placeToAdd.departement,
      geoPoint: placeToAdd.geoPoint,
      isDate: placeToAdd.isDate
    } as Place;

    this.formation.placeArray ? null : this.formation.placeArray = [];
    this.formation.placeArray = this.formation.placeArray.filter(place => place.id !== placeToAdd.id);
    this.formation.placeArray.push(placeToPushToFormation);
    this.formationsService.updateFormation(this.formation);
  }

  openSnackBar = (message: string, action: string) => this.snackBar.open(message, action, { duration: 10000 });

  autocomplete(place: Place) {
    if (place.ville && place.ville.length > 1) {
      this.mapService.autocomplete(place.ville)
        .then(autocomplete => this.predictions = autocomplete['predictions']);
    }
  }

  selectedValue(option: { place_id: string; }) {
    this.predictions = [];
    let placeToAdd = {} as Place;
    if (option) {
      this.mapService.getPlaceId(option.place_id).then(query => {
          query.result.address_components.forEach(adressComponent => {
            switch (adressComponent.types[0]) {
              case "locality":
                placeToAdd.ville = adressComponent.long_name;
                break;
              case "administrative_area_level_2":
                placeToAdd.departement = adressComponent.long_name;
                break;
              case "administrative_area_level_1":
                placeToAdd.region = adressComponent.long_name;
                break;
              case "country":
                placeToAdd.pays = adressComponent.long_name;
                break;
              case "postal_code":
                placeToAdd.codePostal = adressComponent.long_name;
                break;
              default:
                break;
            }
          })
          placeToAdd.location = { latitude: query.result.geometry.location.lat, longitude: query.result.geometry.location.lng };
          placeToAdd.isDate == false ? null : placeToAdd.isDate = true;
          this.onSubmitPlace(placeToAdd);
        })
    }
  }

  resetCalendar = () => this.isResetCalendar ? this.isResetCalendar = false : this.isResetCalendar = true;

}
