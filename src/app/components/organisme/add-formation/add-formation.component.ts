import { FormationsService } from './../../../services/firestore/formations.service';
import { SpreadsheetService } from './../../../services/fetch/spreadsheet.service';
import { Component, OnInit, Input, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SlackService } from './../../../services/fetch/slack.service';
import { User } from './../../../interfaces/user';
import { Formation } from '../../../interfaces/formations';
import { EntitiesService } from '../../../services/firestore/entities.service';
import { AngularFireStorage } from '@angular/fire/storage';
import {COMMA, ENTER} from '@angular/cdk/keycodes'; // keywords
import {MatChipInputEvent} from '@angular/material/chips'; // keywords



@Component({
  selector: 'add-formation',
  templateUrl: './add-formation.component.html',
  styleUrls: ['./add-formation.component.scss']
})
export class AddFormationComponent implements OnInit {
  
  @Input() formation: Formation;
  

  // keywords
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  motCle: string;
  motCleArray = [];
  // fin keywords

  programme = '';
  user: User = {
    isOrganisme: false,
    isStagiaire: true,
  }
  

  constructor(
    @Inject(MAT_DIALOG_DATA) 
    private dataFormation: Formation,
    private formationService: FormationsService,
    private spreadsheetService: SpreadsheetService,
    private dialog: MatDialog,
    private slackService: SlackService,
    private snackBar: MatSnackBar,
    private entitiesService: EntitiesService,
    private storage: AngularFireStorage,

  ) { }

  ngOnInit() {

    // s'il y a des data formation alors this.formation = les datas présentes. sinon cellules vides
    this.dataFormation ? this.formation = this.dataFormation : null;
    // this.formation.contenu && !this.formation.programme ? this.formation.programme = this.formation.contenu : null;
    this.programme = this.formation.programme;
    
  }

  
// Enregistre la formation / update si formation existe
// S'il n'y a pas assez d'information alors pas possible d'enregistrer
// Enregistre et détermine 1) l'url 2) le nombre de point pour la recherche 3) calcul la durée de la formation
  onSubmitFormation() {
    if (this.formation.intitule && this.formation.modalite && this.formation.programme.length > 50) {
      this.calculateDuree();
      this.formation.urlFormation = "https://ianos.io/formation/" + this.formation.id + "/" + this.formation.intitule.replace(/ /g, "-").replace(/&/g, "");
      this.formation.baseSearchPoint = Object.keys(this.formation).length;
      if (this.formation.trainer.filePicture) {
        // this.uploadPicture();
      }
      else {
        this.formation.id ? this.updateFormation() : this.addFormation();
        
      }
    }
    else {
      this.formation.modalite = null;
      this.snackBar.open("Veuillez entrer une modalité, un prix, un titre et un programme de plus de 50 caractères", "Valider");
    }
    this.spreadsheetService.sendFormation(this.formation);
  }

  // quand on update une formation alors:
  // 1- On récupère les mots clés dans son array via la fonction getKeywordsArray
  // 2 - l'utilisateur est averti via le snackbar
  updateFormation() {
    this.getKeywordsArray().then(() => {
      this.formationService.updateFormation(this.formation)
        .then(() => this.snackBar.open("💾 " + this.formation.intitule + " modifiée"));
    });
    
  }

  // Ajoute une formation
  // Récupère les mots clés puis add formation
  // Envoi un message sur slack
  addFormation() {
    this.getKeywordsArray().then(() => {
      this.formationService.addFormation(this.formation).then(formation => (this.formation.id = formation.id, this.snackBar.open("💾 " + this.formation.intitule + " ajoutée")))
        .catch(error => console.log("TCL: AddFormationComponent -> addFormation -> error", error));
    });
    this.slackService.sendMessageToSlackToFormation('Nouvelle formation : ' + this.formation.organisme + this.formation.intitule);
    
  }

  // cette fonction permet de récupérer les mots clés et de les stocker dans firebase
  getKeywordsArray() {
    return this.entitiesService.getMotCleEntities().get().toPromise().then(success => {
        delete this.formation.keywords;
        let keywordsEntities = [];
        let keywordsString = (this.formation.objectifs + ' ' + this.formation.motCleArray + ' ' + this.formation.intitule).toLowerCase();
        success.data()['entities'].forEach(entitie => {
          entitie.synonyms.push(entitie.value)
          entitie.synonyms.forEach(synonym => {
            var regex = new RegExp("\\b(" + synonym.toLowerCase() + ")\\b", 'g');
            var regexResult = keywordsString.search(regex)
            regexResult !== -1 && !keywordsEntities.includes(synonym.toLowerCase()) ? keywordsEntities.push(synonym.toLowerCase()) : null;
          })
        });
        let keywordsAll = keywordsString.replace(/[()]/g, '').toLowerCase().replace(/\s|\'|\/|\’|\)|\(|\./g, ',').split(',').filter(keywords => keywords !== '');
        this.formation.keywords = this.unionOfArray(keywordsAll, keywordsEntities);
      })
      .catch(error => console.log("TCL: AddFormationComponent -> getKeywordsArray -> error", error));
  }

  unionOfArray(arrayA: string[], arrayB: string[]) {
    var hashTable = {};
    for (var i = 0; i < arrayA.length; i++) {
      hashTable[arrayA[i]] = true;
    }
    for (var i = 0; i < arrayB.length; i++) {
      if (!hashTable.hasOwnProperty(arrayB[i])) {
        arrayA.push(arrayB[i]);
      }
    }
    return arrayA;
  }

  // motCleArray debut
  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;
    
    this.formation.motCleArray = this.motCleArray;
    
    // Add our motCLe
    if ((value).trim()) {
      // liste de motCle dans le array motCLe Array
      
      this.formation.motCleArray.push(value.trim());
      
      console.log(this.formation.motCleArray);
      console.log(value);

      this.slackService.sendMessageToSlackToFormation('Nouveau motCle : ' + value);
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  // supprime mot clé
  remove(motCle): void {
    // cherche dans motCleArray le mot clé sélectionné
    const index = this.formation.motCleArray.indexOf(motCle);

    if (index >= 0) {
      this.formation.motCleArray.splice(index, 1);
    }
  }
  // motCleArray fin


  openSnackBar = (message: string, action: string) => this.snackBar.open(message, action, { duration: 5000 });

  calculateDuree() {
    if (this.formation.nombreHeures && this.formation.dureeSeance && !this.formation.duree) {
      this.formation.duree ? null : this.formation.duree = this.formation.nombreHeures * this.formation.dureeSeance;
    }
    return this.formation.duree;
  }

  close = () => this.dialog.closeAll();

  output = (event) => this.formation.programme = event;

  

 

}
