import { MaterialModule } from './../../../assets/material-module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrganismeRoutingModule } from './organisme-routing.module';
import { AddFormationComponent } from './add-formation/add-formation.component';
import { MonCatalogueComponent } from './mon-catalogue/mon-catalogue.component';
import { AddSessionComponent } from './add-session/add-session.component';
import { BillComponent } from './bill/bill.component';
import { FactureComponent } from './facture/facture.component';
import { AddPlaceComponent } from './add-place/add-place.component';
import { NgbModule, NgbDateNativeAdapter } from '@ng-bootstrap/ng-bootstrap';
import { EditorComponent } from '../guest/editor/editor.component';
import { GlobalPipesModule } from '../../pipes/global-pipes/global-pipes.module';
import { AddCatalogueComponent } from './add-catalogue/add-catalogue.component';

@NgModule({
  imports: [
    CommonModule,
    OrganismeRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    MaterialModule,
    GlobalPipesModule
  ],
  declarations: [
    EditorComponent,
    // ListeMembreComponent,
    AddFormationComponent,
    MonCatalogueComponent,
    AddSessionComponent,
    BillComponent,
    FactureComponent,
    AddPlaceComponent,
    AddCatalogueComponent
  ],
  providers: [
    NgbDateNativeAdapter
  ]
})
export class OrganismeModule { }
