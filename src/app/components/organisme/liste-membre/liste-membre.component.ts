import { User } from './../../../interfaces/user';
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UsersService } from 'src/app/services/firestore/users.service';
import { BookingService } from 'src/app/services/firestore/booking.service';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Booking } from 'src/app/interfaces/booking';


@Component({
  selector: 'liste-membre',
  templateUrl: './liste-membre.component.html',
  styleUrls: ['./liste-membre.component.scss']
})
export class ListeMembreComponent {
  user: User;
  bid: string;

  constructor(
    private snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private bookingService: BookingService,
    private usersService: UsersService
  ) {
    this.route.params.subscribe(param => { param.uid ? this.getUser(param.uid) : null;
    this.bid = param.bid;
    })
  }

  getUser(uid: string) {
    this.usersService.getUserById(uid).get().subscribe(user => this.user = user.data());
  }

  submitInscription() {
    this.bookingService.getBookingById(this.bid).get()
      .subscribe(booking => {
        let bookingToUpdate = this.bookingService.toBooked({ id: booking.id, ...booking.data() as Booking });
        this.bookingService.updateBooking(bookingToUpdate)
          .then(() => this.snackBar.open("L'inscription de : " + this.user.email + " est validée", "Valider", { duration: 10000 }))
          .catch(() => this.snackBar.open("Erreur dans l'inscription de : " + this.user.email, "Valider", { duration: 10000 }));
      })


  }

}
