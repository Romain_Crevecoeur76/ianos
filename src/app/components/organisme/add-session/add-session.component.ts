import { SessionService } from './../../../services/firestore/session.service';
import { PlaceService } from './../../../services/firestore/place.service';
import { FormationsService } from './../../../services/firestore/formations.service';
import { CalendarService } from '../../../services/utils/calendar.service';
import { Formation } from './../../../interfaces/formations';
import { Place } from './../../../interfaces/place';
import { Component, Input, OnChanges } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Session } from '../../../interfaces/session';
import { NgbDateAdapter, NgbDateNativeAdapter, NgbDate } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'add-session',
  templateUrl: './add-session.component.html',
  styleUrls: ['./add-session.component.scss'],
  providers: [
    { provide: NgbDateAdapter, useClass: NgbDateNativeAdapter },
    SessionService
  ]
})
export class AddSessionComponent implements OnChanges {
  @Input() formation: Formation;
  @Input() place: Place;
  @Input() fromDate: NgbDate;

  session: Session = {};
  sessions: Session[] = [];

  dateArray: any[];
  displayMonths: number;

  constructor(
    private formationsService: FormationsService,
    private placeService: PlaceService,
    private sessionService: SessionService,
    private snackBar: MatSnackBar,
    private adapter: NgbDateNativeAdapter,
    public calendarService: CalendarService
  ) {
    this.calendarService.dateArray = [];
    
  }

  // une session est une date et lieu de formation
  ngOnChanges() {
    console.log("TCL: AddSessionComponent -> ngOnChanges -> this.calendarService", this.calendarService)
    this.calendarService.dateArray = [];
    console.log("TCL: AddSessionComponent -> ngOnChanges -> this.calendarService", this.calendarService)
    this.getSession();
    this.fromDate ? this.session.startDate = this.adapter.toModel(this.fromDate) : null;
  }

  // bouton ajouter session avec ses données
  onSubmitSession() {
    this.session = {
      pid: this.place.id,
      fid: this.formation.id,
      uid: this.formation.uid,
      organisme: this.formation.organisme,
      intituleFormation: this.formation.intitule,
      ...this.session
    };

    if (this.session.startDate || this.session.dateString) {
      this.session.id ? this.updateSession(this.session) : this.addSession();
    }
    this.getSession();
  }

  // récupère les infos des sessions
  getSession() {
    this.place
    console.log("TCL: AddSessionComponent -> getSession -> this.place", this.place)
    this.sessions = [];

    this.sessionService.getSessionsByPid(this.place.id)
      .onSnapshot(query => query.docChanges().forEach(session => {
        let sessionToShow = { id: session.doc.id, ...session.doc.data() as Session};

        if (session.type === "added") {
          let isSessionAlreadyThere = false;
          this.sessions.forEach(session => session.id === sessionToShow.id ? isSessionAlreadyThere = true : null);
          if(!isSessionAlreadyThere) {
            this.updateCalendarDateArray(sessionToShow);
            this.sessions.push(sessionToShow);
          }
        }
        if (session.type === "removed") {
          this.sessions.forEach((sessionToDelete, index) => {
            sessionToDelete.id == sessionToShow.id ? this.sessions.splice(index, 1) : null;
          });
        }
      }))
  }

  // met à jour le calendrier
  updateCalendarDateArray(sessionToShow: Session) {
    if (sessionToShow.dateString) {
      this.calendarService.dateArray ? null : this.calendarService.dateArray = [];
      this.calendarService.parseDateStringAndPushToArray(sessionToShow.dateString);
    }
  }

  // met à jour la session
  updateSession(session: Session) {
    this.sessionService.updateSession(session);
    this.openSnackBar('La session de ' + session.intituleFormation + ' modifiée avec succès', 'Valider');
  }

  addSession() {
    this.sessionService.addSession(this.session)
      .then(query => query.get().then(session => {
        this.place.sessionArray ? null : this.place.sessionArray = [];
        this.place.sessionArray.push(session.id);
        this.placeService.updatePlace(this.place);
        this.formation.sessionArray ? null : this.formation.sessionArray = [];
        this.formation.sessionArray.push(session.id);
        this.formationsService.updateFormation(this.formation);
      }))
    this.openSnackBar('La session de ' + this.session.intituleFormation + ' correctement créée', 'Valider');
  }

  deleteSession(sessionToDelete: Session) {
    this.formation.sessionArray ? null : this.formation.sessionArray = [];
    this.place.sessionArray = this.place.sessionArray.filter(sessionId => sessionId !== sessionToDelete.id);
    this.placeService.updatePlace(this.place);
    this.formation.sessionArray = this.formation.sessionArray.filter(sessionId => sessionId !== sessionToDelete.id);
    this.formationsService.updateFormation(this.formation);
    this.sessions.forEach((sessionToCompare, index) => sessionToDelete == sessionToCompare ? this.sessions.splice(index, 1) : null);
    this.sessionService.deleteSession(sessionToDelete);
    this.calendarService.dateArray = [];
    this.getSession();
    this.openSnackBar(this.session.intituleFormation + ' supprimée', 'Valider');
  }

  openSnackBar = (message: string, action: string) => this.snackBar.open(message, action, { duration: 2500 });

  // ajoute une ou plusieurs dates
  addDate() {
    this.dateArray ? null : this.dateArray = [];
    this.dateArray.push({ toDate: this.calendarService.toDate, fromDate: this.calendarService.fromDate });
    this.session.dateString = JSON.stringify({ toDate: this.calendarService.toDate, fromDate: this.calendarService.fromDate });
    this.calendarService.deleteDate();
    this.onSubmitSession()
  }

  public addSessionFromListeMembre(place: Place, formation: Formation) {
    this.place = place;
    this.formation = formation;
    this.addDate();
    return this.session;
  }

  public getSessionFromListeMembre(pid: string) {
    this.place = { id: pid };
    this.getSession();
  }
}