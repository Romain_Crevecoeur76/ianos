import { FormationsService } from './../../../services/firestore/formations.service';
import { UsersService } from './../../../services/firestore/users.service';
import { User } from './../../../interfaces/user';
import { Component } from '@angular/core';
import { Formation } from '../../../interfaces/formations';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ObservableService } from '../../../services/utils/observable.service';
import { SpreadsheetService } from '../../../services/fetch/spreadsheet.service';


@Component({
  selector: 'add-catalogue',
  templateUrl: './add-catalogue.component.html',
  styleUrls: ['./add-catalogue.component.scss']
})
export class AddCatalogueComponent {

  allCells: any[][] = [];
  titles: any[] = [];
  organisme: User;
  user: User;
  organismeUid: string;

  myControl = new FormControl();
  filteredOptions: Observable<User[]>;
  organismes: User[] = []

  constructor(
    private observableService: ObservableService,
    private spreadsheetService: SpreadsheetService,
    private usersService: UsersService,
    private formationsService: FormationsService,
    private snackBar: MatSnackBar
  ) {
    this.observableService.currentUser.subscribe(user => user ? (this.user = user, this.organisme = user) : this.user = null);

    this.usersService.getOrganismes().onSnapshot(snapshot => {
        snapshot.forEach(user => this.organismes.push({ id: user.id, ...user.data() as User }))
        this.initAutocomplete();
      })
  }

  initAutocomplete() {
    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith<string | User>(''),
        map(value => typeof value === 'string' ? value : value.displayName),
        map(name => name ? this._filter(name) : this.organismes.slice())
      );
  }

  private _filter(name: string): User[] {
    const filterValue = name.toLowerCase();
    return this.organismes.filter(option => option.displayName.toLowerCase().indexOf(filterValue) === 0);
  }

  getDataFromSpreadSheet(spreadSheetId: string) {
    console.log("TCL: AddCatalogueComponent -> getDataFromSpreadSheet -> spreadSheetId", spreadSheetId)
    // exemple : '1-6P64rImCY_BOaA892DdxmVs2t5HXx-c5Lc-i-IxW8w'
    this.spreadsheetService.getData(spreadSheetId)
      .then((result) => {
        result.json()
          .then(query => {
            query[0].data[0].rowData.forEach((data, index) => {
              if (index == 0) {
                data.values.forEach(value => {
                  this.titles.push(value.formattedValue)
                })
              }
              else {
                this.allCells[index] = [];
                data.values.forEach(value => {
                  value.formattedValue == undefined ? value.formattedValue = "" : null;
                  this.allCells[index].push(value.formattedValue);
                })
              }
            })
          })
      })
      .catch(error => console.log("TCL: AddCatalogueComponent -> ngOnInit -> error", error))
  }

  addFormation(row: string[]) {
    let formation: Formation = {};
    if (this.organisme) {
      this.titles.forEach((title, index) => formation[title] = row[index]);
      formation.uid = this.organisme.id;
      this.formationsService.addFormation(formation)
        .then(() => {
          this.snackBar.open('Formation ajoutée avec succès', 'Valider', { duration: 10000 })
        })
        .catch(error => console.log("TCL: AddCatalogueComponent -> addFormation -> error", error));
    }
    else {
      this.snackBar.open('Indiquez de quel organisme de formation vous faites parti', 'Valider', { duration: 10000 });
    }
  }

}
