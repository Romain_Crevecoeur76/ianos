import { UsersService } from './../../../services/firestore/users.service';
import { SessionService } from './../../../services/firestore/session.service';
import { PlaceService } from './../../../services/firestore/place.service';
import { FormationsService } from './../../../services/firestore/formations.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, NgZone } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { AddFormationComponent } from '../add-formation/add-formation.component';
import { User } from '../../../interfaces/user';
import { Formation } from '../../../interfaces/formations';
import { ObservableService } from '../../../services/utils/observable.service';

@Component({
  selector: 'mon-catalogue',
  templateUrl: './mon-catalogue.component.html',
  styleUrls: ['./mon-catalogue.component.scss']
})
export class MonCatalogueComponent implements OnInit {
  user: User = null;
  formations: Formation[] = [];
  displayedColumns: string[] = ['titre', 'modalite', 'lieu', 'duree', 'certification', 'prix', 'modifier', 'supprimer'];
  dataSource = new MatTableDataSource(this.formations);

  constructor(
    private formationService: FormationsService,
    private placeService: PlaceService,
    private sessionService: SessionService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    private userService: UsersService,
    private route: ActivatedRoute,
    private observableService: ObservableService,
    private zone: NgZone
  ) {
    this.observableService.currentUser.subscribe(user => user && !this.user ? (this.user = user, this.getFormationsByUid(user.id)) : null);
  }

  ngOnInit() {
    // suite à sa connexion, on récupère les données users et ses formations selon son UID
    this.route.firstChild ? (this.formations = [], this.getUserAndFormations(this.route.firstChild.snapshot.paramMap.get('uid'))) : null;
  }

  applyFilter = (filterValue: string) => this.dataSource.filter = filterValue.trim().toLowerCase();

  // récupère user et ses formations
  getUserAndFormations(uid: string) {
    this.getFormationsByUid(uid);
    this.userService.getUserById(uid).get().toPromise().then(user => this.user = { id: user.id, ...user.data() as User})
    
  }

  // surpprime formation button
  deleteFormation(formation: Formation) {
    this.placeService.getPlacesByFid(formation.id).get()
      .then(query => query.forEach(place => {
        this.placeService.deletePlaceById(place.id);
        this.sessionService.getSessionsByPid(place.id).get()
          .then(query => query.empty ? null : query.forEach(session => this.sessionService.deleteSessionById(session.id)));
      }))
      .catch(error => console.log("​MonCatalogueComponent -> deleteFormation -> error", error));
    this.formationService.deleteFormation(formation);
    this.openSnackBar(formation.intitule + ' supprimée avec succès', 'Valider');
  }

  // récupère formations selon uid du user
  getFormationsByUid(uid: string) {
    this.formationService.getFormationsByUid(uid).onSnapshot(query => {
        query.docChanges().forEach((formation, index) => {
          const formationToShow = { id: formation.doc.id, ...formation.doc.data() as Formation };
          if (formation.type === "added") {
            this.formations.push(formationToShow);
          }
          if (formation.type === "modified") {
            this.formations[formation.newIndex].id == formationToShow.id ? this.formations[formation.newIndex] = formationToShow : null;
          }
          if (formation.type === "removed") {
            this.formations.length == 1 ? this.formations.pop() : null;
            this.formations.forEach((formation, index) => {
              formation.id == formationToShow.id ? this.formations.splice(index, 1) : null;
            })
          }
        })
        this.dataSource = new MatTableDataSource(this.formations);
        this.zone.run(() => { });
      });
  }

  // bouton ouvre formation et les données
  openDialog(formation: Formation): void {
    if (formation && this.user) {
      this.user.presentation ? formation.presentation = this.user.presentation : null;
      this.user.displayName ? formation.organisme = this.user.displayName : formation.organisme = this.user.email;
      formation.uid ? null : formation.uid = this.user.id;
      formation.trainer ? null: formation.trainer = {};
    }
    formation.id ? null : formation.dateCreated = new Date();

    let dialogRef = this.dialog.open(AddFormationComponent, {
      width: '90%',
      height: '90%',
      data: formation,
      closeOnNavigation: true,
      disableClose: true,
    });

    dialogRef.backdropClick().subscribe(() => this.snackBar.open('Formation non sauvegardée voulez vous continuer ? ', 'Valider')
      .onAction().subscribe(() => dialogRef.close()));
  }

  openSnackBar = (message: string, action: string) => this.snackBar.open(message, action, { duration: 10000 });

}
