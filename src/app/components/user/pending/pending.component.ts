import { Booking } from './../../../interfaces/booking';
import { FormationsService } from './../../../services/firestore/formations.service';
import { BookingService } from './../../../services/firestore/booking.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component } from '@angular/core';
import { Formation } from '../../../interfaces/formations';
import { User } from '../../../interfaces/user';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ObservableService } from '../../../services/utils/observable.service';
import { CompanyService } from '../../../services/firestore/company.service';
import { Company } from '../../../interfaces/company';
import { UsersService } from '../../../services/firestore/users.service';

@Component({
  selector: 'pending',
  templateUrl: './pending.component.html',
  styleUrls: ['./pending.component.scss']
})
export class PendingComponent {

  members: User[] = [];
  user: User;
  memberToPropose: User;
  company: Company;
  formation: Formation;
  booking: Booking;
  
  constructor(
    private observableService: ObservableService,
    private companyService: CompanyService,
    private usersService: UsersService,
    private route: ActivatedRoute,
    private router: Router,
    private bookingService: BookingService,
    private formationsService: FormationsService,
    private snackBar: MatSnackBar
  ) {
    this.observableService.currentUser.subscribe(user => user && !this.user ? this.isUser(user) : null);
    this.route.params.subscribe(param => param.bid ? this.getBooking(param.bid) : null)
  }

  isUser(user: User) {
    if (user) {
      this.user = user;
      this.user.eid ? this.getCompany() : null;
    }
  }

  getCompany() {
    this.companyService.getCompanyById(this.user.eid).get().toPromise()
      .then(company => {
        this.company = { id: company.id, ...company.data() as Company }
        this.company.members ? this.getUsers() : null;
      })
      .catch(error => console.log("TCL: EspaceEntrepriseComponent -> ngOnInit -> error", error));
  }

  getUsers() {
    this.company.members.forEach(uid => {
      this.usersService.getUserById(uid).get().toPromise()
        .then(user => this.members.push({ id: user.id, ...user.data() as User }));
    });
  }

  getBooking(bid: string) {
    this.bookingService.getBookingById(bid).get().toPromise()
      .then(booking => {
        this.getFormation(booking.data()['fid']);
        this.booking = { id: booking.id, ...booking.data() as Booking };
      })
      .catch(error => console.log("TCL: FavoriteComponent -> getBooking -> error", error));
  }

  getFormation(fid: string) {
    this.formationsService.getFormationById(fid).get().toPromise()
      .then(formation => this.formation = { id: formation.id, ...formation.data() as Formation });
  }

  // proposeToMember(member: User) {
  //   this.formation
  //   let reservation = {
  //     fid: this.formation.id,
  //     pid: this.formation.pid,
  //     intituleFormation: this.formation.intitule,
  //     ville: this.formation.ville,
  //     uid: member.id,
  //     eid: member.eid,
  //     email: member.email,
  //     isProposition: true
  //   } as Booking;
  //   reservation.sid = this.formation.sid ? this.formation.sid : null;
  //   this.bookingService.addBooking(reservation)
  //     .then(() => {
  //       this.snackBar.open("Réservation en cours de validation par l'organisme de formation", "Valider");
  //       this.router.navigate(['/dashboard', { outlets: { 'dashboard': ['pending', this.booking.id] } }]);
  //     })
  //     .catch(error => console.log("TCL: DashboardCompanyComponent -> proposeToMember -> error", error));
  // }

  validate() {
    this.booking = this.bookingService.toBooked(this.booking);
    this.bookingService.updateBooking(this.booking)
      .then(() => {
        this.snackBar.open("Réservation validée par l'organisme de formation", "Valider");
        this.router.navigate(['/dashboard', { outlets: { 'dashboard': ['booking', this.booking.id] } }]);
      })
      .catch(error => console.log("TCL: FavoriteComponent -> bookTraining -> error", error));
  }

  deleteBooking() {
    this.bookingService.deleteBooking(this.booking)
      .then(() => (this.snackBar.open("Réservation supprimée avec succès", "Valider"), this.router.navigate(['/dashboard'])))
      .catch(error => console.log("TCL: FavoriteComponent -> deleteFromFavorite -> error", error));
  }
}
