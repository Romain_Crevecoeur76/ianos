import { ListMemberComponent } from './../../entreprise/list-member/list-member.component';
import { User } from './../../../interfaces/user';
import { Booking } from './../../../interfaces/booking';
import { FormationsService } from './../../../services/firestore/formations.service';
import { BookingService } from './../../../services/firestore/booking.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component } from '@angular/core';
import { Formation } from '../../../interfaces/formations';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ReviewService } from '../../../services/firestore/review.service';
import { Review } from '../../../interfaces/review';
import { ObservableService } from '../../../services/utils/observable.service';
import { SpreadsheetService } from '../../../services/fetch/spreadsheet.service';

@Component({
  selector: 'proposition',
  templateUrl: './proposition.component.html',
  styleUrls: ['./proposition.component.scss']
})
export class PropositionComponent {

  formation: Formation;
  user: User;
  members: User[] = [];
  booking: Booking;
  chat: Review;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private bookingService: BookingService,
    private observableService: ObservableService,
    private formationsService: FormationsService,
    private spreadsheetService: SpreadsheetService,
    private reviewService: ReviewService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog
  ) {
    this.observableService.currentUser.subscribe(user => user && !this.user ? this.user = user : null);
    this.route.params.subscribe(param => param.bid ? this.getBooking(param.bid) : null)
  }

  getBooking(bid: string) {
    this.bookingService.getBookingById(bid).get().toPromise().then(booking => {
        this.booking = { id: booking.id, ...booking.data() } as Booking;
        this.getFormation(this.booking.fid);
        this.showProposition(this.booking);
      })
      .catch(error => this.spreadsheetService.sendError(error));
  }

  getFormation(fid: string) {
    this.formationsService.getFormationById(fid).get().toPromise()
      .then(formation => this.formation = { id: formation.id, ...formation.data() });
  }

  deleteBooking() {
    this.bookingService.deleteBooking(this.booking)
      .then(() => {
        this.snackBar.open("Réservation supprimée avec succès", "Valider");
        this.router.navigate(['/dashboard']);
      })
      .catch(error => this.spreadsheetService.sendError(error));
  }

  showProposition(proposition: Booking) {
    this.reviewService.getReviewByBid(proposition.id).get()
      .then(query => {
        if (query.empty) {
          this.chat = {
            dateCreated: new Date().getTime(),
            email: this.user.email,
            prenom: this.user.prenom,
            nom: this.user.nom,
            role: this.user.role,
            bid: proposition.id
          };
        }
        else {
          query.forEach(chat => this.chat = { id: chat.id, ...chat.data() });
        }
      })
      .catch(error => this.spreadsheetService.sendError(error));
  }

  addComment() {
    this.reviewService.addReview({ ...this.chat, isCompanyChat: true, })
      .then(chat => (this.chat.id = chat.id, this.addResponse()))
      .catch(error => this.spreadsheetService.sendError(error));
  }

  addResponse() {
    if (this.chat.id) {
      this.chat.role ? null : this.chat.role = "Salarié";
      this.chat.responses ? null : this.chat.responses = [];
      this.chat.responses.push({ nom: this.user.nom, prenom: this.user.prenom, message: this.chat.lastResponse, role: this.user.role });
      this.chat.lastResponse = "";
      this.reviewService.updateReview(this.chat);
    }
    else {
      this.addComment();
    }
  }

  deleteResponse(review: Review, responseToDelete: string) {
    review.responses = review.responses.filter(response => response !== responseToDelete);
    this.reviewService.updateReview(review)
      .then(() => this.snackBar.open("Commentaire supprimée avec succès", "Valider"))
      .catch(error => this.spreadsheetService.sendError(error));
  }

  openListMember = () => this.dialog.open(ListMemberComponent, { data: this.booking });

  book() {
    this.booking = this.bookingService.toPending(this.booking);
    this.bookingService.updateBooking(this.booking)
      .then(() => {
        this.snackBar.open("Réservation en cours de validation par l'organisme de formation", "Valider");
        this.router.navigate(['/dashboard', { outlets: { 'dashboard': ['pending', this.booking.id] } }]);
      })
      .catch(error => this.spreadsheetService.sendError(error));
  }

  onChange() {
    this.bookingService.updateBooking(this.booking)
      .catch(error => this.spreadsheetService.sendError(error));
  }

  toggle(drawer, changeClass: HTMLDivElement) {
    drawer.toggle()
    changeClass.classList.remove("content");
    changeClass.classList.add("contentToggled");
  console.log("TCL: PropositionComponent -> toggle -> changeClass", changeClass)
  console.log("TCL: PropositionComponent -> toggle -> drawer", drawer)
    
  }
}
