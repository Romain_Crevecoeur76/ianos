import { Booking } from './../../../interfaces/booking';
import { FormationsService } from './../../../services/firestore/formations.service';
import { BookingService } from './../../../services/firestore/booking.service';
import { ActivatedRoute } from '@angular/router';
import { Component } from '@angular/core';
import { Formation } from '../../../interfaces/formations';
import { User } from '../../../interfaces/user';

@Component({
  selector: 'request',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.scss']
})
export class RequestComponent {

  formation: Formation;
  booking: Booking;
  members: User[] = [];

  constructor(
    private route: ActivatedRoute,
    private bookingService: BookingService,
    private formationsService: FormationsService
  ) {
    this.route.params.subscribe(param => param.bid ? this.getBooking(param.bid) : null)
  }

  getBooking(bid: string) {
    this.bookingService.getBookingById(bid).get().toPromise()
      .then(booking => {
        this.getFormation(booking.data()['fid']);
        this.booking = { id: booking.id, ...booking.data() as Booking };
      })
      .catch(error => console.log("TCL: FavoriteComponent -> getBooking -> error", error));
  }

  getFormation(fid: string) {
    this.formationsService.getFormationById(fid).get().toPromise()
      .then(formation => this.formation = { id: formation.id, ...formation.data()as Formation });
  }

}
