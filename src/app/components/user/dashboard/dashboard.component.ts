import { Booking } from './../../../interfaces/booking';
import { User } from './../../../interfaces/user';
import { BookingService } from './../../../services/firestore/booking.service';
import { Component, OnInit } from '@angular/core';
import { ObservableService } from 'src/app/services/utils/observable.service';

@Component({
  selector: 'dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent {
  user: User;

  bookings: Booking[];
  pendings: Booking[];

  constructor(
    private bookingService: BookingService,
    private observableService: ObservableService,
  ) {
    this.observableService.currentUser.subscribe(user => user ? this.getBookings(user) : null);
  }

  getBookings(user: User) {
    if (!this.user) {
      this.bookings = [];
      this.pendings = [];
      this.bookingService.getBookingsByOfid(user.id).get()
        .then(bookings => bookings.forEach(booking => this.bookings.push({ id: booking.id, ...booking.data() as Booking })));
      this.bookingService.getPendingsByOfid(user.id).get()
        .then(pendings => pendings.forEach(pending => this.pendings.push({ id: pending.id, ...pending.data() as Booking })));
    }
    this.user = user;
  }

}