import { Booking } from './../../../interfaces/booking';
import { FormationsService } from './../../../services/firestore/formations.service';
import { BookingService } from './../../../services/firestore/booking.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component } from '@angular/core';
import { Formation } from '../../../interfaces/formations';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ObservableService } from '../../../services/utils/observable.service';
import { User } from '../../../interfaces/user';

@Component({
  selector: 'booked',
  templateUrl: './booked.component.html',
  styleUrls: ['./booked.component.scss']
})
export class BookedComponent {

  formation: Formation;
  booking: Booking;
  user: User;

  constructor(
    private snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private router: Router,
    private bookingService: BookingService,
    private observableService: ObservableService,
    private formationsService: FormationsService
  ) {
    this.observableService.currentUser.subscribe(user => user && !this.user ? this.user = user : null);
    this.route.params.subscribe(param => param.bid ? this.getBooking(param.bid) : null)
  }

  getBooking(bid: string) {
    this.bookingService.getBookingById(bid).get().toPromise().then(booking => {
        this.getFormation(this.newMethod(booking));
        this.booking = { id: booking.id, ...booking.data() as Booking };
      })
      .catch(error => console.log("TCL: FavoriteComponent -> getBooking -> error", error));
  }

  private newMethod(booking): string {
    return booking.data().fid;
  }

  getFormation(fid: string) {
    this.formationsService.getFormationById(fid).get().toPromise()
      .then(formation => this.formation = { id: formation.id, ...formation.data() as Formation });
  }

  archived() {
    this.booking = this.bookingService.toArchived(this.booking);
    this.bookingService.updateBooking(this.booking)
      .then(() => this.snackBar.open("Réservation archivée", "Valider"))
      .then(() => this.router.navigate(["dashboard"]))
  }
}
