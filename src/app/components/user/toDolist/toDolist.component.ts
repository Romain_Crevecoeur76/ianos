import { Component, OnInit} from '@angular/core';
import { todolist } from './../../../interfaces/todolist';
import { TodoService } from './../../../services/firestore/todolist.service';
import { User } from '../../../interfaces/user';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ObservableService } from './../../../services/utils/observable.service';
import { ActivatedRoute } from '@angular/router';

import { Company } from 'src/app/interfaces/company';
import { CompanyService } from './../../../services/firestore/company.service';
import { UsersService } from '../../../services/firestore/users.service';

@Component({
  selector: 'toDolist',
  templateUrl: './toDolist.component.html',
  styleUrls: ['./toDolist.component.scss']
})
export class toDolistComponent implements OnInit {

  todoList: todolist[]= [];
  user: User;
  newTodoText: string;
  Todolist: todolist = {};


  constructor(
    private snackBar: MatSnackBar,
    private todoService:TodoService,
    private route: ActivatedRoute,
    private observableService: ObservableService,
    public companyService: CompanyService,
    public usersService: UsersService,


    ) {
      // this.observableService.currentUser.subscribe(user => user && !this.user ? this.getCompany(user) : null);    
      this.observableService.currentUser.subscribe(user => user && !this.user ? this.getTodolist(user) : null );
      console.log(this.Todolist)
  }

  ngOnInit() {  }

  getTodolist(user: User) {
    // this.user = user;
    //   this.todoService.getToDoListById(id).get().subscribe(Todolist => {
    //         this.Todolist = { id: this.Todolist.id };
    //         console.log(this.Todolist)
    //         console.log(this.Todolist.task)
    //     },)  
}
  

  updateTodoList = (Todolist: todolist) => this.todoService.updateTodo(Todolist).catch();
  openSnackBar = (message: string, action: string) => this.snackBar.open(message, action, { duration: 10000 });


  getNotDeleted() {
    return this.todoList.filter((item:any) => {
      return !item.deleted
    })
  }

  addToDoItem($event) {

    if (($event.which === 1 || $event.which === 13) && this.Todolist.text.trim() != '') {

      this.todoList.unshift({text: this.Todolist.text});
      // this.Todolist.text = '';
      // this.todoService.addTodo(this.Todolist)

      console.log(this.todoList)
      console.log(this.Todolist.text)
    }
  
  }


  
}
