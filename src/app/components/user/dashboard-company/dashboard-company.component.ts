import { Component } from '@angular/core';
import { UsersService } from '../../../services/firestore/users.service';
import { ObservableService } from '../../../services/utils/observable.service';
import { CompanyService } from '../../../services/firestore/company.service';
import { User } from '../../../interfaces/user';
import { Company } from '../../../interfaces/company';
import { Booking } from '../../../interfaces/booking';
import { BookingService } from '../../../services/firestore/booking.service';

@Component({
  selector: 'dashboard-company',
  templateUrl: './dashboard-company.component.html',
  styleUrls: ['./dashboard-company.component.scss']
})
export class DashboardCompanyComponent {
  members: User[] = [];
  user: User;
  company: Company;

  bookingFavorites: Booking[] = [];
  bookingRequests: Booking[] = [];
  bookingPropositions: Booking[] = [];
  bookingPendings: Booking[] = [];
  bookings: Booking[] = [];

  constructor(
    private observableService: ObservableService,
    private bookingService: BookingService,
    private companyService: CompanyService,
    private usersService: UsersService
  ) {
    this.observableService.currentUser.subscribe(user => user && !this.user ? this.isUser(user) : null);
  }

  isUser(user: User) {
    if (user) {
      this.user = user;
      if (user.isOrganisme) {
        this.getPendingsOrganismes();
        this.getBookingsOrganismes();
      }
      else {
        if (this.user.eid) {
          this.getCompany();
          this.getRequests();
          this.getPropositions();
          this.getPendingsByEid();
          this.getBookingsByEid();
        }
        else {
          this.getBookingsByUid();
          this.getPendingsByUid();
        }
        this.getFavorites();
      }
    }
  }

  handleBooking(booking, bookingToFill: Booking[]) {
    booking.type === "removed" ? bookingToFill = bookingToFill.filter(item => item.id !== booking.doc.id) : null;
    booking.type === "added" ? bookingToFill.push({ id: booking.doc.id, ...booking.doc.data() as Booking }) : null;
    return bookingToFill;
  }

  getPendingsOrganismes() {
    this.bookingService.getPendingsByOfid(this.user.id).onSnapshot(query => 
      query.docChanges().forEach(booking => this.bookingPendings = this.handleBooking(booking, this.bookingPendings)))
  }

  getBookingsOrganismes() {
    this.bookingService.getBookingsByOfid(this.user.id).onSnapshot(query => 
      query.docChanges().forEach(booking => this.bookings = this.handleBooking(booking, this.bookings)));
  }

  getBookingsByEid() {
    this.bookingService.getBookingsByEid(this.user.eid).onSnapshot(query => 
      query.docChanges().forEach(booking => this.bookings = this.handleBooking(booking, this.bookings)));
  }

  getBookingsByUid() {
    this.bookingService.getBookedByUid(this.user.id).onSnapshot(query => 
      query.docChanges().forEach(booking => this.bookings = this.handleBooking(booking, this.bookings)));
  }

  getPendingsByEid() {
    this.bookingService.getPendingsByEid(this.user.eid).onSnapshot(query => 
      query.docChanges().forEach(booking => this.bookingPendings = this.handleBooking(booking, this.bookingPendings)));
  }

  getPendingsByUid() {
    this.bookingService.getPendingsByUid(this.user.id).onSnapshot(query =>
      query.docChanges().forEach(booking => this.bookingPendings = this.handleBooking(booking, this.bookingPendings)));
  }

  getRequests() {
    this.bookingService.getRequestsByEid(this.user.eid).onSnapshot(query => 
      query.docChanges().forEach(booking => this.bookingRequests = this.handleBooking(booking, this.bookingRequests)));
  }

  getPropositions() {
    this.bookingService.getPropositionsByEidAndMemberIncluded(this.user.eid, this.user.id).onSnapshot(query => 
      query.docChanges().forEach(booking => this.bookingPropositions = this.handleBooking(booking, this.bookingPropositions)));
  }

  getFavorites() {
    this.bookingService.getFavoritesByUid(this.user.id).onSnapshot(query => 
      query.docChanges().forEach(booking => this.bookingFavorites = this.handleBooking(booking, this.bookingFavorites)));
  }

  getCompany() {
    this.companyService.getCompanyById(this.user.eid).get().toPromise()
      .then(company => {
        this.company = { id: company.id, ...company.data() as Company }
        this.company.members ? this.getUsers() : null;
      })
      .catch(error => console.log("TCL: EspaceEntrepriseComponent -> ngOnInit -> error", error));
  }

  getUsers() {
    this.company.members.forEach(uid => {
      this.usersService.getUserById(uid).get().toPromise()
        .then(user => this.members.push({ id: user.id, ...user.data() as User }));
    });
  }
}
