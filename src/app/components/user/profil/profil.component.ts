import { SendEmail } from './../../../services/fetch/send-email';
import { Company } from './../../../interfaces/company';
import { CompanyService } from './../../../services/firestore/company.service';
import { ObservableService } from '../../../services/utils/observable.service';
import { AuthService } from './../../../services/auth/auth.service';
import { UsersService } from './../../../services/firestore/users.service';
import { User } from './../../../interfaces/user';
import { Component, ViewChild} from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { AngularFireStorage } from '@angular/fire/storage';
import { SlackService } from './../../../services/fetch/slack.service';


// ajout de mots clés
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {MatChipInputEvent} from '@angular/material/chips';
// fin ajout de mots clés

// datepicker
import {FormGroup, FormControl, FormBuilder, Validators} from '@angular/forms';
import { Moment } from 'moment';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { MAT_DATE_FORMATS, DateAdapter, MatDateFormats } from '@angular/material/core';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
const CUSTOM_DATE_FORMATS: MatDateFormats = {
  parse: {
      dateInput: 'D/MM/YYYY'
  },
  display: {
      dateInput: 'DD/MM/YYYY',
      monthYearLabel: 'MMMM Y',
      dateA11yLabel: 'LL',
      monthYearA11yLabel: 'MMMM Y'
  }
};

@Component({
  selector: 'profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.scss'],
  // datepicker
  providers: [
    {provide: MAT_DATE_FORMATS, useValue: CUSTOM_DATE_FORMATS},
    {provide: DateAdapter, useClass: MomentDateAdapter}
],
})


export class ProfilComponent {
  
  user: User = {};
  company: Company = {};

  isMailSending = false;

  companies: Company[] = [];
  imageURL: any;
  userImage: string;
  task: any;

  // public featuredPhotoStream: FirebaseObjectObservable<FeaturedPhotosURL>,

  // ajout de mots clés
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  userKeyword: string;
  userKeywords = [];
  // fin ajout de mots clés
  
  // datepicker
  // range = new FormGroup({
  //   start: new FormControl(),
  //   end: new FormControl()
  // });

  


  constructor(
    private userService: UsersService,
    private companyService: CompanyService,
    private authService: AuthService,
    private snackBar: MatSnackBar,
    private router: Router,
    private sendEmail: SendEmail,
    private observableService: ObservableService,
    private storage: AngularFireStorage,
    private slackService: SlackService,
    private fb: FormBuilder,

  ) {


    this.observableService.currentUser.subscribe(user => user ? this.getUser(user) : null);
    this.companyService.getCompanies().snapshotChanges().subscribe(query => query.forEach(company => this.companies.push({ id: company.payload.doc.id, ...company.payload.doc.data() as Company})))
    
  }

  ngOnInit() {
    
  }

  getUser = (user: User) => this.user = user;

  // @ViewChild('picker') 
  // picker: any;
  // dateRange: moment.Moment;
  // disabled = false;  
  // tempDate : Date;
  // range = new FormGroup({
  //     start: new FormControl(new Date(this.tempDate)),
  //     end: new FormControl(new Date()),
  // });

  // ngAfterViewInit() {
  //     this.range.valueChanges.pipe().subscribe(event => {
  //         if (event.start && event.end) {
  //             this.onDateChanged(event);
  //         }
  //     });
  // }


  onSubmit() {
    if (this.user.displayName !== "" || (this.user.prenom !== "" && this.user.nom !== "")) {
      this.updateUser();
    }
    else {
      if (this.user.isStagiaire) {
        this.openSnackBar('Entrez votre nom et prénom', 'Valider')
      }
      else {
        this.openSnackBar('Entrez le nom de votre organisme de formation', 'Valider')
      }
    }
  }

  updateUser() {
    this.userService.updateUser(this.user).then(() => {
        this.openSnackBar(this.user.email + ' correctement mis à jour', 'Valider')
        this.observableService.triggerGetUser(this.user);
      })
      .catch(() => this.openSnackBar(this.user.displayName + ' erreur dans la mise à jour', 'Valider'));
  }

  onChangePassword() {
    this.user.displayName == '' ? null : this.authService.changePassword(this.user.email);
    this.openSnackBar(this.user.displayName + ' mail de changement de mot de passe envoyé', 'Valider');
  }

  onChangeEmail() {
    this.user.displayName == '' ? null : this.authService.changeEmail(this.user.email);
    this.openSnackBar(this.user.displayName + ' mail de changement de mot de passe envoyé', 'Valider');
  }

  openSnackBar = (message: string, action: string) => this.snackBar.open(message, action, { duration: 10000 });

  deleteUser() {
    let email = this.user.email;
    this.authService.getCurrentUser();
    this.authService.deleteUser(this.user)
      .then(() => {
        this.router.navigate(['/']);
        this.openSnackBar(email + ' correctement supprimé', 'Valider');
        this.observableService.triggerLogout(true);
        this.authService.doLogout();
      })
      .catch(error => {
        console.log('TCL: ProfilComponent -> deleteUser -> error', error);
        this.router.navigate(['/']);
        this.openSnackBar(email + ' erreur dans la supression', 'Valider');
        this.observableService.triggerLogout(true);
        this.authService.doLogout();
      });
  }

  sendInviteEmail(emailToInvite, emailToInviteButton) {
    this.isMailSending = true;
    this.sendEmail.sendEmailInvitationRh(emailToInvite.value, this.user)
      .then(email => {
        console.log("TCL: ProfilComponent -> sendInviteEmail -> email", email)
        this.openSnackBar("Mail envoyé avec succès", "Valider")
      })
      .catch(error => {
        console.log("TCL: ProfilComponent -> sendInviteEmail -> error", error)
        this.openSnackBar("Erreur dans l'envoie du mail nous notifions nos équipes", "Valider");
      });
    return emailToInviteButton.hidden = true;

  }

  // uploadPicture(event:any) {
  //   const file: File = event.target.files[0];

  //   const metaData = { 'contentType': file.type};
  //   const storageRef: firebase.storage.Reference = firebase.storage().ref('/photos/organisme/'+ file.name)
  //   storageRef.put(file, metaData);
  //   console.log('uploading:', file.name)

  //   this.userImage = this.task.downloadURL();
  // }



// ajout de mots clés
    add(event: MatChipInputEvent): void {
      const input = event.input;
      const value = event.value;

      this.user.userKeywords = this.userKeywords;
      
      // Add motCle
      if ((value || '').trim()) {
        this.user.userKeywords.push(value.trim());
    
        console.log(this.user.userKeywords);
        console.log(value);

        this.slackService.sendMessageToSlackToFormation('Nouvelle Newsletter : ' + value + "" + this.user.email);

      }

      // Reset the input value
      if (input) {
        input.value = '';
      }
    }
    remove(userKeyword: string): void {
      const index = this.user.userKeywords.indexOf(userKeyword);
      console.log(userKeyword)

      if (index >= 0) {
        this.user.userKeywords.splice(index, 1);
      }
    }
  


// datepicker
  // handleStartChange(event) {   
  //   const m: Moment = event.value;
  //   if(m){
  //     this.user.startDisponibility = m.toDate();
  //     console.log("Date of Birth: " + m.toDate());
  //   }
  // }
  // handleEndChange(event) {
  //   const m: Moment = event.value;
  //     if(m){
  //       this.user.EndDisponibility = m.toDate();
  //       console.log("Date of Birth: " + m.toDate());
  //     }
  //   }



    // onDateChanged(event){ 
    //   const m: Moment = event.start;
    //   if(m){
    //     this.user.startDisponibility = m.toDate();
    //     console.log("Début des disponibilité " + m.toDate());
    //     this.tempDate = this.user.startDisponibility
    // }

      // const me: Moment = event.end;
      // if(me){
      //   this.user.EndDisponibility = me.toDate();
      //   console.log("Date of Birth: " + me.toDate());
      // }
    // }
    
}
