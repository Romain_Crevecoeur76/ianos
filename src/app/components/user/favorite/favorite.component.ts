// import { Booking } from './../../../interfaces/booking';
// import { FormationsService } from './../../../services/firestore/formations.service';
// import { BookingService } from './../../../services/firestore/booking.service';
// import { ActivatedRoute, Router } from '@angular/router';
// import { Component } from '@angular/core';
// import { Formation } from '../../../interfaces/formations';
// import { User } from '../../../interfaces/user';
// import { MatSnackBar } from '@angular/material';
// import { ObservableService } from '../../../services/utils/observable.service';
// import { CompanyService } from '../../../services/firestore/company.service';
// import { Company } from '../../../interfaces/company';
// import { UsersService } from '../../../services/firestore/users.service';

// @Component({
//   selector: 'favorite',
//   templateUrl: './favorite.component.html',
//   styleUrls: ['./favorite.component.scss']
// })
// export class FavoriteComponent {

//   members: User[] = [];
//   user: User;
//   memberToPropose: User;
//   company: Company;

//   formation: Formation;
//   booking: Booking;

//   constructor(
//     private observableService: ObservableService,
//     private companyService: CompanyService,
//     private usersService: UsersService,
//     private route: ActivatedRoute,
//     private router: Router,
//     private bookingService: BookingService,
//     private formationsService: FormationsService,
//     private snackBar: MatSnackBar
//   ) {
//     this.observableService.currentUser.subscribe(user => user && !this.user ? this.isUser(user) : null);
//     this.route.params.subscribe(param => param.bid ? this.getBooking(param.bid) : null)
//   }

//   isUser(user: User) {
//     if (user) {
//       this.user = user;
//       this.user.eid ? this.getCompany() : null;
//     }
//   }

//   getCompany() {
//     this.companyService.getCompanyById(this.user.eid).get().toPromise()
//       .then(company => {
//         this.company = { id: company.id, ...company.data() }
//         this.company.members ? this.getUsers() : null;
//       })
//       .catch(error => console.log("TCL: EspaceEntrepriseComponent -> ngOnInit -> error", error));
//   }

//   getUsers() {
//     this.company.members.forEach(uid => {
//       this.usersService.getUserById(uid).snapshotChanges()
//         .subscribe(user => user.payload.id !== this.user.id ? this.members.push({ id: user.payload.id, ...user.payload.data() }) : null);
//     });
//   }

//   getBooking(bid: string) {
//     this.bookingService.getBookingById(bid).get().toPromise()
//       .then(booking => {
//         this.getFormation(booking.data().fid);
//         this.booking = { id: booking.id, ...booking.data() };
//         this.booking.dateString ? JSON.parse(this.booking.dateString) : null;
//       })
//       .catch(error => console.log("TCL: FavoriteComponent -> getBooking -> error", error));
//   }

//   getFormation(fid: string) {
//     this.formationsService.getFormationById(fid).get().toPromise()
//       .then(formation => this.formation = { id: formation.id, ...formation.data() });
//   }

//   proposeToMember(member: User) {
//     console.log("TCL: FavoriteComponent -> demandToMember -> member", member)
//     this.booking = this.bookingService.toProposition(this.booking);
//     this.booking.rhid = this.user.id;
//     this.booking.members ? null : this.booking.members = [];
//     this.booking.members.push(this.bookingService.createMember(member));
//     this.bookingService.updateBooking(this.booking)
//       .then(() => {
//         this.snackBar.open("Proposition de formation envoyée", "Valider");
//         this.router.navigate(['/dashboard', { outlets: { 'dashboard': ['proposition', this.booking.id] } }]);
//       })
//       .catch(error => console.log("TCL: DashboardCompanyComponent -> proposeToMember -> error", error));
//   }

//   // demandToMember(member: User) {
//   //   console.log("TCL: FavoriteComponent -> demandToMember -> member", member)
//   //   this.booking = this.bookingService.toRequest(this.booking);
//   //   this.booking.rhid = this.user.id;
//   //   this.booking.members ? null : this.booking.members = [];
//   //   // this.booking.members.push(member.id);
//   //   this.bookingService.updateBooking(this.booking)
//   //     .then(() => {
//   //       this.snackBar.open("Proposition de formation envoyée", "Valider");
//   //       this.router.navigate(['/dashboard', { outlets: { 'dashboard': ['proposition', this.booking.id] } }]);
//   //     })
//   //     .catch(error => console.log("TCL: DashboardCompanyComponent -> proposeToMember -> error", error));
//   // }

//   bookTraining() {
//     this.booking = this.bookingService.toPending(this.booking);
//     this.bookingService.updateBooking(this.booking)
//       .then(() => {
//         this.snackBar.open("Réservation en cours de validation par l'organisme de formation", "Valider");
//         this.router.navigate(['/dashboard', { outlets: { 'dashboard': ['pending', this.booking.id] } }]);
//       })
//       .catch(error => console.log("TCL: FavoriteComponent -> bookTraining -> error", error));
//   }

//   deleteBooking() {
//     this.bookingService.deleteBooking(this.booking)
//       .then(() => {
//         this.snackBar.open("Réservation supprimée avec succès", "Valider")
//         // this.router.navigate(['/'])
//       })
//       .catch(error => console.log("TCL: FavoriteComponent -> deleteFromFavorite -> error", error));
//   }
// }
