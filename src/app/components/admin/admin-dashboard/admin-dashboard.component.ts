import { UsersService } from './../../../services/firestore/users.service';
import { EntitiesService } from './../../../services/firestore/entities.service';
import { FormationsService } from './../../../services/firestore/formations.service';
import { AdminService } from './../../../services/firestore/admin.service';
import { User } from './../../../interfaces/user';
import { Session } from './../../../interfaces/session';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Context } from '../../../interfaces/context';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { Formation } from '../../../interfaces/formations';


@Component({
    selector: 'admin-dashboard',
    templateUrl: './admin-dashboard.component.html',
    styleUrls: ['./admin-dashboard.component.scss']
})
export class AdminDashboard implements OnInit {
    users: User[] = [];
    formations: Formation[];
    organismes: User[] = [];
    stagiaires: User[] = [];
    sessionsNew: Session[] = [];
    sessionsOld: Session[] = [];
    keywords: any;

    contexts: Context = {};
    arrayRecherche: string[] = [];
    myControl = new FormControl();
    filteredOptions: Observable<string[]>;

    results: {
        numberFormation: number;
    };

    constructor(
        private adminService: AdminService,
        private formationsService: FormationsService,
        private entitiesService: EntitiesService,
        private usersService: UsersService,
        private route: Router,
    ) { }

    ngOnInit() {

        this.adminService.getAllUsers().get()
            .then(query => query.empty ? null : query.forEach(user => this.users.push({ id: user.id, ...user.data() as User })))
            .catch(error => console.log('TCL: AdminComponent -> ngOnInit -> error', error));

        this.adminService.getAllNewSessions().get()
            .then(query => query.empty ? null : query.forEach(session => this.sessionsNew.push({ id: session.id, ...session.data() as Session})))
            .catch(error => console.log('TCL: AdminComponent -> ngOnInit -> error', error));

        this.adminService.getAllOldSessions().get()
            .then(query => query.empty ? null : query.forEach(session => this.sessionsOld.push({ id: session.id, ...session.data() as Session })))
            .catch(error => console.log('TCL: AdminComponent -> ngOnInit -> error', error));

        this.adminService.getAll0rgannismes().get()
            .then(query => query.forEach(user => this.organismes.push({ id: user.id, ...user.data() as User })))
            .catch(error => console.log('TCL: AdminDashboard -> ngOnInit -> error', error));

        this.adminService.getAllStagiaires().get()
            .then(query => query.forEach(user => this.stagiaires.push({ id: user.id, ...user.data() as User })))
            .catch(error => console.log('TCL: AdminDashboard -> ngOnInit -> error', error));

        this.adminService.getAllFormations().get()
            .then(query => query.forEach(formation => this.formations.push({ id: formation.id, ...formation.data() as Formation }))) 
            .catch(error => console.log('TCL: AdminDashboard -> ngOnInit -> error', error));

        this.adminService.getAllMotCles().get().toPromise()
            .then(keywords => this.keywords = this.limitKeywordsNumber(this.sortKeywords(keywords.data()['keywordToShow'])))
            .catch(error => console.log('TCL: AdminDashboard -> ngOnInit -> error', error));

        

        this.filteredOptions = this.myControl.valueChanges.pipe(
            startWith(''),
            map(value => this._filter(value))
        );
        this.getContextsOptions();
    }

    private _filter(value: string): string[] {
        const filterValue = value.toLowerCase();
        return this.arrayRecherche.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
    }

    sortKeywords(toSort: Array<any>) {
        return toSort.sort((a, b) => {
            return b.count - a.count;
        });
    }

    limitKeywordsNumber(toSort: Array<any>) {
        return toSort.slice(0, 10);
    }

    showCatalogue(uid: string) {
        this.route.navigate(['/organisme/mon-catalogue/' + uid])
    }

    showDashboard(uid: string) {
        this.route.navigate(['/dashboard/' + uid])
    }

    showProfil(uid: string) {
        this.route.navigate(['/profil/' + uid])
    }

    resetRecherches = () => this.adminService.resetMotCleStatsCounts();

    resetRecherche(keyword: string) {
        this.adminService.resetMotCleStatCount(keyword);
    }

    getContextsOptions() {
        this.entitiesService.getMotCleEntities().get().toPromise()
            .then(doc => doc.exists ? doc.data()['entities'].forEach(entitie => this.arrayRecherche.push(entitie.value)) : null)
            .catch(error => console.log("​CarouselComponent -> getContextsOptions -> error", error));
        this.arrayRecherche = this.deleteDuplicate(this.arrayRecherche);
    }

    deleteDuplicate(array: string[]) {
        return array.filter(function (elem, index, self) {
            return index === self.indexOf(elem);
        })
    }

    addKeyword(motCle: string) {
        this.contexts.motCleArray ? null : this.contexts.motCleArray = [];
        this.contexts.motCleArray.push(motCle.toLowerCase());
    }

    onKeydown = (event) => event.key === "Enter" ? this.addKeyword(event.path[0].value) : null;

    research() {
        this.formations = [];
        this.results = { numberFormation: 0 }
        this.formationsService.getFormationsByKeyword(this.contexts.motCleArray[0].toLowerCase()).get()
            .then(query => query.forEach(formation => {
                this.formations.push(formation.data());
                this.results.numberFormation = this.formations.length;
            }))
            .catch(error => console.log("TCL: AdminDashboard -> research -> error", error));
    }


    resetResearch() {
        this.contexts.motCleArray = [];
        this.results = null;
    }

    updateLeadUser(lead: string, user: User) {
        user.leadOrNot = lead;
        this.usersService.updateUser(user);
    }

    updateAllTrainings(){
        // this.formationsService.updateAllFormation(formation)
        console.log(this.organismes)
    }

}