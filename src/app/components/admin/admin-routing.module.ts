import { AdminDashboard } from './admin-dashboard/admin-dashboard.component';
import { AuthGuardAdmin } from './../../services/auth/auth.guard-admin';
import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


const adminRoutes: Routes = [
  {
    path: '',
    canActivate: [AuthGuardAdmin],
    children: [
      {
        path: '',
        canActivate: [AuthGuardAdmin],
        children: [
          { path: 'admin-dashboard', component: AdminDashboard },
        ]
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(adminRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AdminRoutingModule {}
