import { SendEmail } from './../../../services/fetch/send-email';
import { ObservableService } from '../../../services/utils/observable.service';
import { SlackService } from './../../../services/fetch/slack.service';
import { User } from './../../../interfaces/user';
import { Context } from './../../../interfaces/context';
import { Component, OnInit, Input } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  @Input() context: Context;
  
  showButton: boolean = true;
  user: User = {};
  formationNonTrouvee: boolean = false;
  message: string = '';
  choice: string = "formation";

  reviewWebsite = {
    markWebsite: 0,
    markChatbot: 0,
    markStagiaire: 0,
    foundTraining: null,
    device: '',
    chatbotMessage: ''
  }

  constructor(
    private slackService: SlackService,
    private observableService: ObservableService,
    private snackBar: MatSnackBar,
    private router: Router,
    private route: ActivatedRoute,
    private modalService: NgbModal,
    private sendEmail: SendEmail
  ) {
    this.observableService.currentUser.subscribe(user => user ? this.user = user : null);
  }


  ngOnInit() {
    this.route.queryParams.subscribe(success => success.mode ? this.choice = success.mode : null)
    if (this.context) {
      this.showButton = true;
      this.formationNonTrouvee = true;
      this.slackService.sendMessageToSlackToProblemes(this.message + " je suis : " + this.user.email);
      this.message = "J'ai cherché une formation en '" + this.context.motCle + "' mais je n'ai pas trouvé pouvez vous m'aider ?";
      this.openSnackBar("Venez vous inscrire chez nous pour que nous restions en contact !", "S'inscrire");
    }
    else {
      this.context  = {};
        }
  }

  submit() {

    switch (this.choice) {
      case 'test':
        let isFormation = '';
        this.reviewWebsite.foundTraining ? isFormation = "J'ai trouvé ma formation" : isFormation = "Je n'ai pas trouvé ma formation je cherchais : " + this.context.motCle + " ";
        let message = `je suis : `+ this.user.email + ` ` + this.user.displayName +
        `, j'ai testé le site sur : ` + this.reviewWebsite.device +
        ` La page stagiaire est claire à : ` + this.reviewWebsite.markStagiaire + ` `
        + isFormation +
        `Je suggère pour le chatbot : ` + this.reviewWebsite.chatbotMessage +
        ` et je lui donne la note globale de ` + this.reviewWebsite.markWebsite +
        ` Je dis : ` + this.message;
        this.slackService.sendMessageToSlackToProblemes(message);
        break;
      case 'formation':
        this.slackService.sendMessageToSlackToProblemes(" je suis : " + this.user.email + " Je dis : " + this.message + " Je cherche une formation en   ");

        break;
      case 'autre':
        this.sendEmail.sendEmail(this.user.email, 'romain.crevecoeur@lilo.org', 'Formulaire de contact de la part de : ' + this.user.email, this.message);
        this.slackService.sendMessageToSlackToProblemes(" je suis : " + this.user.email + " Je dis : " + this.message);
        break;
    }

    this.formationNonTrouvee ? null : this.openSnackBar("Merci pour votre message notre équipe vous recontacte très bientôt !", "S'inscrire");
  }

  openSnackBar = (message: string, action: string) => {
    let snackBarRef = this.snackBar.open(message, action, {
      verticalPosition: 'top',
      duration: 10000,
    });

    snackBarRef.onAction().subscribe(() => {
      action == "S'inscrire" ? this.router.navigate(['/inscriptions']) : null;
    });
  }

  hasOpenModals () {
    this.showButton = true;
  }

    
  close () { 
    this.modalService.dismissAll();
  }
  
}