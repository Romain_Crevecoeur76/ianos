import { ObservableService } from '../../../services/utils/observable.service';
import { CategoryService } from './../../../services/firestore/category.service';
import { EntitiesService } from './../../../services/firestore/entities.service';
import { Category } from './../../../interfaces/category';
import { User } from './../../../interfaces/user';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
// import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {
  user: User;

  arrayRecherche: string[] = [];
  myControl = new FormControl();
  filteredOptions: Observable<string[]>;

  // public editor = ClassicEditor;
  public config = { language: 'fr' };
  category: Category = { content: ' ' };

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private entitiesService: EntitiesService,
    private categoryService: CategoryService,
    private observableService: ObservableService

  ) {
    this.observableService.currentUser.subscribe(user => user ? this.user = user : this.user = null);
  }

  ngOnInit() {
    this.route.params.subscribe(query => this.getCategory(query.titreCategory));
    this.user && this.user.isAdmin ? this.initAdmin() : null;
  }

  initAdmin() {
    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );
    this.getContextsOptions();
  }

  getCategory(id: string) {
    this.categoryService.getCategoryById(id).get().toPromise()
      .then(category => this.category = category.data())
      .catch(fail => console.log("TCL: CategoriesComponent -> getCategory -> fail", fail));
  }

  newCategory() {
    this.category.content = this.category.content.replace(`<figure class="media"><oembed url="`, "###");
    this.category.content = this.category.content.replace(`"></oembed></figure>`, "###");
    this.category.contentArray = this.category.content.split("###");
    this.category.titleUrl = this.category.titleUrl.replace(/ /g, "");
    this.categoryService.addCategory(this.category)
      .then(() => (this.router.navigate(['/training/' + this.category.titleUrl]), this.ngOnInit()))
      .catch(error => console.log("TCL: ArticleComponent -> newArticle -> error", error));
  }

  updateCategory() {
    this.categoryService.updateCategory(this.category)
      .catch(error => console.log("TCL: ArticleComponent -> updateArticle -> error", error));
  }

  getContextsOptions() {
    this.entitiesService.getMotCleEntities().get().toPromise()
      .then(doc => !doc.exists ? null : doc.data()['entities'].forEach(entitie => this.arrayRecherche.push(entitie.value)))
      .catch(error => console.log("​CarouselComponent -> getContextsOptions -> error", error));
  }

  private _filter = (value: string): string[] => this.arrayRecherche.filter(option => option.toLowerCase().indexOf(value.toLowerCase()) === 0);

  addKeyword(option: string) {
    this.category.motCleArray ? null : this.category.motCleArray = [];
    this.category.motCleArray.push(option);
  }

  deleteCategory = () => this.categoryService.deleteCategory(this.category);

}
