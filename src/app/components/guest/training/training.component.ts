import { SlackService } from './../../../services/fetch/slack.service';
import { SeoService } from './../../../services/fetch/seo.service';
import { ObservableService } from '../../../services/utils/observable.service';
import { Session } from './../../../interfaces/session';
import { Formation } from './../../../interfaces/formations';
import { User } from './../../../interfaces/user';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormationsService } from '../../../services/firestore/formations.service';
import { GetSessionsModalComponent } from '../bot/get-sessions-modal/get-sessions-modal.component';
import { GetSessionComponent } from '../bot/get-session/get-session.component';
import { Location } from '@angular/common'; 

@Component({
  selector: 'training',
  templateUrl: './training.component.html',
  styleUrls: ['./training.component.scss']
})
export class TrainingComponent implements OnInit {
  user: User;
  formation: Formation;
  formations: Formation[];
  sessions: Session[] = [];
  sessionChose: string;

  constructor(
    private location: Location,
    private router: Router,
    private route: ActivatedRoute,
    private formationService: FormationsService,
    private observableService: ObservableService,
    private seoService: SeoService,
    private modalService: NgbModal,
    private snackBar: MatSnackBar,
    private slackService: SlackService
  ) {
    this.observableService.currentUser.subscribe(user => user ? this.user = user : this.user = null);
  }

  ngOnInit() {
    this.route.paramMap.subscribe(query => {
      query.get('fid') ? this.getFormation(query.get('fid')) : null;
    });
  }

  getFormation(id: string) {
    this.formationService.getFormationById(id).get().toPromise()
      .then(formation => {
        formation.exists ? null : this.router.navigate(['/']);
        this.formation = { id: formation.id, ...formation.data() as Formation};
        this.seoChange();
      })
      .catch(error => {
        console.log('TCL: TrainingComponent -> ngOnInit -> error', error);
        this.router.navigate(['/']);
      });
  }

  seoChange() {
    this.formation.intitule ? this.location.go('/formation/' + this.formation.id + '/' + this.formation.intitule.replace(/ /g, "-")) : null;
    this.seoService.generateTags({
      title: this.formation.intitule,
      description: this.formation.description
    })
  }

  buttonToSubscribe() {
    if (this.user) {
      if (this.sessions[0] && this.formation.modalite !== 'Elearning') {
        return "Se pré-inscrire";
      }
      else if (!this.sessions[0] && this.formation.modalite !== 'Elearning' && this.formation.isDate === false) {
        return "Me prévenir quand une date se libère";
      }
      else if (this.formation.modalite == 'Elearning') {
        return "Lien vers la formation en ligne";
      }
      else {
        return "Indiquez la date qui vous arrange";
      }
    }
    else {
      return "Se connecter pour se pré - inscrire";
    }
  }

  submitSession(choice: string) {
    switch (choice) {
      case "Se pré-inscrire":
        this.submit();
        break;
      case "Me prévenir quand une date se libère":
        this.getNewDate();
        break;
      case "Indiquez la date qui vous arrange":
        this.submit();
        break;
      case "Lien vers la formation en ligne":
        this.formationEnLigne();
        break;
      case "Se connecter pour se pré - inscrire":
        this.redirect();
        break;
      default:
        break;
    }
  }

  formationEnLigne() {
    this.slackService.sendMessageToSlackToInscription("J'ai cliqué sur le lien de la formation en ligne : " + this.formation.intitule + " ID : " + this.formation.id + " Je suis : " + this.user.email);
    !this.formation.urlweb.includes('https') || !this.formation.urlweb.includes('http') ? this.formation.urlweb = 'http://' + this.formation.urlweb : null;
    window.open(this.formation.urlweb);
  }

  submit() {
    const modalRef = this.modalService.open(GetSessionsModalComponent, { size: 'lg' });
    modalRef.componentInstance.formation = this.formation;
    modalRef.componentInstance.user = this.user;
    modalRef.componentInstance.sessionChose = this.sessionChose;
  }

  getNewDate() {
    this.slackService.sendMessageToSlackToInscription("Me prévenir quand une date se libère pour la formation : : " + this.formation.intitule + " ID : " + this.formation.id + " Je suis : " + this.user.email);
    this.openSnackBar("Message envoyé", "Valider");
  }

  redirect() {
    this.router.navigate(['/inscriptions/' + this.formation.id]);
  }

  openSnackBar(message: string, action: string) {
    let snackBarRef = this.snackBar.open(message, action, { duration: 10000 });

    snackBarRef.onAction().subscribe(() => {
      action == "S'inscrire" ? this.router.navigate(['/inscriptions/' + this.formation.id]) : null;
      action == "Chercher un financement" && this.formation.certification ? this.router.navigate(['/préinscription']) : null;
      action == "Chercher un financement" && !this.formation.certification ? this.router.navigate(['/enregistrement']) : null;
    });
  }

  open(formation: Formation) {
    const modalRef = this.modalService.open(GetSessionComponent);
    modalRef.componentInstance.formation = formation;
    modalRef.componentInstance.user = this.user;
  }


}
