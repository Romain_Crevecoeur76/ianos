import { SeoService } from './../../../services/fetch/seo.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'batisseur',
  templateUrl: './batisseur.component.html',
  styleUrls: ['./batisseur.component.scss']
})
export class BatisseurComponent implements OnInit {

  constructor(
    private seoService: SeoService,
  ) { }

  ngOnInit() {
    let tags = {
      title: "IANOS, Salarié, demandeur d'emploi, chef d'entreprise, indépendant",
      description: "Vous cherchez une formation professionnelle et nous vous en félicitons ! Vous êtes à nos yeux le bâtisseur du monde de demain."
    }
    this.seoService.generateTags(tags);
  }

}
