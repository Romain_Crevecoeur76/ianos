import { ObservableService } from '../../../services/utils/observable.service';
import { AuthService } from './../../../services/auth/auth.service';
import { User } from './../../../interfaces/user';
import { Component, Output, EventEmitter, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'bs-navbar',
  templateUrl: './bs-navbar.component.html',
  styleUrls: ['./bs-navbar.component.scss']
})
export class BsNavbarComponent {
  @Output() triggerDrawer = new EventEmitter<Boolean>();
  @Input() user: User;
  @Input() isDesktop: boolean;

  constructor(
    private authService: AuthService,
    private router: Router,
    private observableService: ObservableService,
  ) { }

  toggleSideBar() {
    this.triggerDrawer.emit(true);
  }

  logout() {
    this.authService.doLogout()
      .then(() => {
        this.observableService.triggerLogout(true);
        this.router.navigate(['/']);
      })
      .catch(error => console.log("​BsNavbarComponent -> logout -> error", error));
  }
}
