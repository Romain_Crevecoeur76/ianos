import { Component, OnInit } from '@angular/core';
import { FormationsService } from '../../../services/firestore/formations.service';
import { Formation } from '../../../interfaces/formations';

@Component({
  selector: 'all-training',
  templateUrl: './all-training.component.html',
  styleUrls: ['./all-training.component.scss']
})
export class AllTrainingComponent implements OnInit {
  formations: Formation[] = [];

  sitemap = "";

  constructor(
    public formationsService: FormationsService
  ) { }

  ngOnInit() {
    if (window.location.hostname === "localhost") {
      this.formationsService.getAllFormations().snapshotChanges()
        .subscribe(query => {
          query.forEach(formation => {
            let formationToShow = { id: formation.payload.doc.id, ...formation.payload.doc.data() as Formation, intitule: formation.payload.doc.data()['intitule'].replace(/ /g, "-").replace(/&/g, '')};
            this.formations.push(formationToShow);
            this.sitemap = this.sitemap + `<url><loc>https://ianos.io/formation/` + formationToShow.id + `/` + formationToShow.intitule + `</loc></url>`;
          });
          console.log("TCL: AllTrainingComponent -> ngOnInit -> this.sitemap", this.sitemap)

        })
    }
  }

}
