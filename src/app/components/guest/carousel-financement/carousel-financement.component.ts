import { SpreadsheetService } from './../../../services/fetch/spreadsheet.service';
import { FinancementService } from './../../../services/firestore/financement.service';
import { Message } from './../../../interfaces/message';
import { Component, OnInit, Input } from '@angular/core';
import { User } from '../../../interfaces/user';
import { FinancementComponent } from '../financement/financement.component';

@Component({
  selector: 'carousel-financement',
  templateUrl: './carousel-financement.component.html',
  styleUrls: ['./carousel-financement.component.scss']
})
export class CarouselFinancementComponent implements OnInit {

  @Input() message: Message;
  @Input() user: User

  financements: any[];
  // message.contexts: any[] = [];
  rights: string[] = []

  constructor(
    private financementService: FinancementService,
    private spreadsheetService: SpreadsheetService
  ) { }

  ngOnInit() {
    console.log("TCL: CarouselFinancementComponent -> ngOnInit -> this.message.contexts", this.message.contexts)

    if (this.message.contexts['statut'] === "salarié") {
      this.rights.push("Entreprise")
      this.message.contexts['TailleEntreprise'] > 50 ? null : this.rights.push("OPCO");
    }
    if (this.message.contexts['statut'] === "sans emploi") {
      this.rights.push("Pôle Emploi")
      this.message.contexts['intention'] === "changer de métier" || 'reconversion' ? null : this.rights.push("CPF transition");
    }
    this.message.contexts['statut'] === "salarié" || this.message.contexts['statut'] === "sans emploi" ? this.rights.push("CPF") : null;
    switch (this.message.contexts['ActiviteIndependant']) {
      case "profession libérale":
        this.rights.push("FIF-PL")
        break;
      case "médecine":
        this.rights.push("FIF-PM")
        break;
      case "commerçant":
        this.rights.push("Agefice")
        break;
      case "artiste":
        this.rights.push("AFDAS")
        break;
      case "artisan":
        this.rights.push("FAFCEA")
        break;
      case "agricole":
        this.rights.push("Vivéa")
        break;
      case "Pêche":
        this.rights.push("Agefos")
        break;
    }
    console.log("TCL: CarouselFinancementComponent -> ngOnInit -> this.rights", this.rights)

    this.financements ? null : this.financements = [];
    this.rights.forEach(right => {
      this.financementService.getFinancementById(right).get().subscribe(
        financement => {
          financement.data()
          console.log("TCL: CarouselFinancementComponent -> ngOnInit -> financement.data()", financement.data())
          this.financements.push({ ...financement.data() as Message, id: financement.id })
          console.log("TCL: CarouselFinancementComponent -> ngOnInit -> this.financements", this.financements)
        },
        error => this.spreadsheetService.sendError(error));
    })
  }

}
