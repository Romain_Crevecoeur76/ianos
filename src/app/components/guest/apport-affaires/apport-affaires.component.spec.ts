import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ApportAffairesComponent } from './apport-affaires.component';

describe('ApportAffairesComponent', () => {
  let component: ApportAffairesComponent;
  let fixture: ComponentFixture<ApportAffairesComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ApportAffairesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApportAffairesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
