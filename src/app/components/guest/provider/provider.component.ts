import { SeoService } from './../../../services/fetch/seo.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'provider',
  templateUrl: './provider.component.html',
  styleUrls: ['./provider.component.scss']
})
export class ProviderComponent implements OnInit {
  
  constructor(
    private seoService: SeoService,
  ) { }

  ngOnInit() {
    let tags = {
      title: "IANOS, Fournisseur de connaissance",
      description: "Vous avez acquis de la connaissance et souhaitez la mettre à disposition de votre prochain, nous vous en félicitons ! Vous êtes à nos yeux le fournisseur de la matière première la plus importante de notre siècle. C'est pourquoi IANOS vous est entièrement dédié."
    }
    this.seoService.generateTags(tags);
  }

}
