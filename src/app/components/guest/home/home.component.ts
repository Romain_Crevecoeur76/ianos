import { Article } from './../../../interfaces/article';
import { SeoService } from '../../../services/fetch/seo.service';
import { User } from './../../../interfaces/user';
import { ObservableService } from '../../../services/utils/observable.service';
import { Component, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SignInComponent } from '../sign/sign-in/sign-in.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {
  @Output() user: User;

  chatbot: boolean;
  isSafari: boolean;

  articles: Article[];

  constructor(
    private observableService: ObservableService,
    private seoService: SeoService,
    private route: ActivatedRoute,
    public router: Router,
    private modalService: NgbModal,
  ) {
    this.observableService.currentUser.subscribe(user => user ? (this.chatbot = false, this.user = user) : this.user = null);
    this.route.queryParams.subscribe(value => value.chatbot ? this.chatbot = true : null);
    this.seoService.generateTags(null);
    let ua = navigator.userAgent.toLowerCase();
    ua.indexOf('safari') != -1 && ua.indexOf('chrome') < -1 ? this.isSafari = true : this.isSafari = false;
  }


  sendMessage = (inputMessage: string) => inputMessage.length > 2 ? this.router.navigate(['bot'], { queryParams: { message: inputMessage } }) : this.router.navigate(['/bot']);

  open(){
    this.modalService.dismissAll(); 
    this.modalService.open(SignInComponent, { size: 'lg', centered: true});
  }
}
