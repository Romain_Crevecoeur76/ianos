import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss']
})
export class EditorComponent {
  @Input() 
  programme: string;

  @Output() 
  programmeChange = new EventEmitter<string>();

  constructor() {}

  output = (event) => this.programmeChange.emit(event.target.innerHTML);

  change = (parameter, bool, textToChange) => document.execCommand(parameter, bool, textToChange);

}
