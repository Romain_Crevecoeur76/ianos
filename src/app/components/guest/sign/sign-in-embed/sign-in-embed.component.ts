import { SendEmail } from './../../../../services/fetch/send-email';
import { ObservableService } from '../../../../services/utils/observable.service';
import { SlackService } from './../../../../services/fetch/slack.service';
import { UsersService } from './../../../../services/firestore/users.service';
import { AuthService } from './../../../../services/auth/auth.service';
import { User } from './../../../../interfaces/user';
import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'sign-in-embed',
  templateUrl: './sign-in-embed.component.html',
  styleUrls: ['./sign-in-embed.component.scss']
})
export class SignInEmbedComponent {
  form: FormGroup;
  user: User = {
    isOrganisme: false,
    isStagiaire: true,
  }

  errorMessage: string = '';
  successMessage: string = '';

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private authService: AuthService,
    private userService: UsersService,
    private slackService: SlackService,
    private observableService: ObservableService,
    private sendEmail: SendEmail,
    private snackBar: MatSnackBar
  ) {
    this.createForm();
  }

  createForm() {
    this.form = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
      stagiaire: true,
    });
  }

  signInWithGoogle(value) {
    this.authService.doRegisterWithGoogle()
      .then(result => (value.email = result.user.email, this.addUserToBdd(result, value)))
      .catch(error => {
        console.log('TCL: SignInComponent -> signInWithGoogle -> error', error);
        this.errorMessage = this.authService.translateErrorMessage(error, value);
      });
  }

  signInWithFacebook(value) {
    this.authService.doRegisterWithFacebook()
      .then(result => (value.email = result.user.email, this.addUserToBdd(result, value)))
      .catch(error => {
        console.log('TCL: SignInComponent -> signInWithFacebook -> error', error);
        this.errorMessage = this.authService.translateErrorMessage(error, value);
      });
  }

  tryRegister(value) {
    if (value.email.includes("@")) {
      value.email = value.email.replace(' ', '');
      this.authService.doRegister(value)
        .then(result => this.addUserToBdd(result, value))
        .catch(error => {
          console.log('TCL: SignInComponent -> tryRegister -> err', error);
          this.errorMessage = this.authService.translateErrorMessage(error, value);
        });
    }
  }

  addUserToBdd(result, value) {
    if (result.user.displayName) {
      this.user.displayName = result.user.displayName;
      this.user.prenom = this.user.displayName.split(" ")[0];
      this.user.nom = this.user.displayName.split(" ")[1];
    }
    this.user.id = result.user.uid;
    this.errorMessage = "";
    this.successMessage = "Votre compte a été créé";
    this.fillUser(value);
    this.observableService.triggerDrawer(true);
    this.userService.createUser(this.user);
    this.observableService.triggerLogout(false);
    // this.user.isStagiaire ? this.sendEmail.sendEmailBienvenue(this.user.isOrganisme, this.user.email) : null;
    this.authService.emailConfirmationInscription();
    this.slackService.sendMessageToSlackToInscription('Nouvel utilisateur : ' + this.user.email);
  }

  tryConnectUser(value) {
    this.authService.doLogin(value)
      .then(() => {
        this.observableService.triggerLogout(false);
        this.openSnackBar(value.email + ' correctement connecté', "Page d'accueil");
      })
      .catch(error => {
        console.log('TCL: LoginComponent -> tryLogin -> error', error);
        this.errorMessage = this.authService.translateErrorMessage(error, value.email);
      });
  }

  openSnackBar(message: string, action: string) {
    let snackRef = this.snackBar.open(message, action, { duration: 10000 });
    snackRef.onAction().subscribe(() => action.includes('@') ? this.sendEmailResetPassword(action) : null);
  }

  sendEmailResetPassword(email: string) {
    this.authService.changePassword(email);
  }

  fillUser(value) {
    if (value.stagiaire == 'organisme') {
      this.user.isOrganisme = true;
      this.user.isStagiaire = false;
    }
    else {
      this.user.isOrganisme = false;
      this.user.isStagiaire = true;
    }
    this.user.email = value.email;
  }
}
