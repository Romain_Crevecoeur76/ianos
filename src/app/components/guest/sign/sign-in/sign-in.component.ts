import { SpreadsheetService } from './../../../../services/fetch/spreadsheet.service';
import { FormationsService } from './../../../../services/firestore/formations.service';
import { BookingService } from './../../../../services/firestore/booking.service';
import { SendEmail } from './../../../../services/fetch/send-email';
import { AuthService } from './../../../../services/auth/auth.service';
import { ObservableService } from '../../../../services/utils/observable.service';
import { SlackService } from './../../../../services/fetch/slack.service';
import { UsersService } from './../../../../services/firestore/users.service';
import { User } from './../../../../interfaces/user';
import { Component, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LoginComponent } from './../login/login.component';

// création d'une entreprise
import { CompanyService } from '../../../../services/firestore/company.service';
import { Company } from '../../../../interfaces/company';

// création d'une todo
import { TodoService } from './../../../../services/firestore/todolist.service';
import { todolist } from '../../../../interfaces/todolist';

// ajout de mots clés
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {MatChipInputEvent} from '@angular/material/chips';
import { Formation } from 'src/app/interfaces/formations';
// fin ajout de mots clés


@Component({
  selector: 'sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent {
  @Input() isModal: boolean;

  form: FormGroup;
  user: User = {
    isOrganisme: false,
    isStagiaire: true,
  }
  isOrganisme = false;

  // company
  userToAddToCompany: User;
  companyToAddToUser: Company;
  userToAddToTodolist: todolist;
  isRh = false;
  company: Company;
  // todolist
  Todolist: todolist;

  errorMessage = '';
  successMessage = '';

  // ajout de mots clés
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  userKeyword: string;
  userKeywords = [];
  // fin ajout de mots clés

  constructor(
    
    private todoService:TodoService,
    private companyService: CompanyService,
    private formationsService: FormationsService,
    private bookingService: BookingService,
    private authService: AuthService,
    private usersService: UsersService,
    private slackService: SlackService,
    private observableService: ObservableService,
    private spreadsheetService: SpreadsheetService,
    private sendEmail: SendEmail,
    private snackBar: MatSnackBar,
    private modalService: NgbModal,
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,

  ) {
    this.createForm('', '', '');
  }


  ngOnInit() {
    this.observableService.currentUser.subscribe(user => {user ? this.createForm(user.email, user.password, user.company) : null;})

    // souscription quand invitation
    this.route.queryParams.subscribe(query => {
      // si user to add to entreprise = true alors fonction "getUserByUid()"
      query.uidToAddToEid ? this.getUserByUid(query.uidToAddToEid) : null;
      // si entreprise to add to user = true alors fonction getCompanyByEid()
      query.eidToAddToUid ? this.getCompanyByEid(query.eidToAddToUid) : null;
      // si email to add = true alors création du questionnaire
      query.emailToAdd ? this.createForm(query.emailToAdd, '', '') : null;
    })
  }

  // récupères les données utilisateurs
  getUserByUid(uid: string) {
    this.isRh = true;
    this.createForm('', '', '');
    this.usersService.getUserById(uid).valueChanges().subscribe(user => this.userToAddToCompany = user);
    // this.usersService.getUserById(uid).valueChanges().subscribe(user => this.userToAddToTodolist = user);
  }

  // récupères les données des utilisateurs membres entreprise
  getCompanyByEid(eid: string) {
    
    this.companyService.getCompanyById(eid).valueChanges().subscribe(company => this.companyToAddToUser = company);
    
  }

  // formulaire
  createForm(email: string, password: string, organization: string) {
    let isStagiaire = this.user.role;

    email ? null : email = '';
    password ? null : password = '';
    organization ? null : organization = '';
    
    this.form = this.fb.group({
      email: [email, Validators.required],
      password: [password, Validators.required],
      organization: organization,
      isStagiaire: isStagiaire,
    });

  }

  // création du compte avec google
  signInWithGoogle(value) {
    this.authService.doRegisterWithGoogle()
      .then(result => {
        value.email = result.user.email;
        this.createUserToBdd(result, value);
      })
      .catch(error => {
        // this.spreadsheetService.sendError(error);
        this.errorMessage = this.authService.translateErrorMessage(error, value);
      });
  }

  // creation du compte avec facebook
  signInWithFacebook(value) {
    this.authService.doRegisterWithFacebook()
      .then(result => {
        value.email = result.user.email;
        this.createUserToBdd(result, value);
      }).catch(error => {
        this.spreadsheetService.sendError(error);
        this.errorMessage = this.authService.translateErrorMessage(error, value);
      });
  }

  // bouton submit
  tryRegister(value) {
    this.isModal ? this.modalService.dismissAll() : null;
    if (value.email.includes("@")) {
      value.email = value.email.replace(' ', '');
      this.authService.signUpWithEmailPasswoerd(value).then(result => this.createUserToBdd(result, value)).catch(error => {
          // this.spreadsheetService.sendError(error);
          this.errorMessage = this.authService.translateErrorMessage(error, value);
        });
    }
    this.isModal ? this.modalService.dismissAll() : null;
  }


  // création d'un utilisateur (depuis signInWithFacebook / signInWithGoogle / email) sur firebase selon les données indiquées
  async createUserToBdd(result, value) {
    this.user.id = result.user.uid;
    // création du compte entreprise quand invitation
    this.companyToAddToUser ? this.addCompanyToUser() : null
    // todolist
    this.userToAddToTodolist ? this.addTodolistToUser() : null

      if (result.user.displayName) {
        this.user.displayName = result.user.displayName;
        this.user.prenom = this.user.displayName.split(" ")[0];
        this.user.nom = this.user.displayName.split(" ")[1];
      }
    this.fillUser(value);

    // console.log("TCL: SignInComponent -> createUserToBdd -> this.user", this.user)
    // console.log("TCL: SignInComponent -> createUserToBdd -> value", value)

    if(value.organization) {
      this.createCompany(value.organization)
      this.createTodolist(this.user.id)
    }
    else {
      this.createUser();
    }
  }

  // création utilisateur
  createUser() {
    this.usersService.createUser(this.user).then(() => {
      // A la création:
      // 1- Message de création
      // 2- Envoi un email de bienvenue
      // 3- Envoi un email pour confirmer
        this.errorMessage = "";
        this.successMessage = "Votre compte a été créé";
        this.observableService.triggerLogout(false);
        this.sendEmail.sendEmailBienvenue(this.user);
        this.authService.emailConfirmationInscription();


        if (this.user.isOrganisme) {
          this.slackService.sendMessageToSlackToInscription('Nouvel OF : ' + this.user.email);
        }
        else {
          this.slackService.sendMessageToSlackToInscription('Nouveau stagiaire : ' + this.user.email);
        }
        this.router.navigateByUrl('/profil');
        this.route.queryParams.subscribe(query => {query.pid ? this.createBooking(query.pid) : null})
        this.modalService.dismissAll(); 
      })
  }

  

  // fonction permettant de créer des réservation de formation
  createBooking(pid: string) {
    this.formationsService.getFormationByPid(pid).onSnapshot(query => {
      query.forEach(formation => {
        let booking = this.bookingService.fillBooking({ id: formation.id, ...formation.data() as Formation }, this.user);
        this.bookingService.addBooking({ ...booking, isFavorite: true });
      })
    })
  }


  openSnackBar(message: string, action: string) {
    let snackRef = this.snackBar.open(message, action, { duration: 10000 });
    snackRef.onAction().subscribe(() => action == "Page d'accueil" ? this.router.navigate(['/']) : null);
    snackRef.onAction().subscribe(() => action.includes('@') ? this.sendEmailResetPassword(action) : null);
  }


  // fonction qui permet de changer de mot de passe en recevant un email
  sendEmailResetPassword = (email: string) => this.authService.changePassword(email);

  
  // fonction qui détermine si user= organisme ou stagiaire
  fillUser(value) {
    if (value.isStagiaire === 'organisme') {
      this.user.isOrganisme = true;
      this.user.isStagiaire = false;
    }
    else {
      this.user.isOrganisme = false;
      this.user.isStagiaire = true;
    }
    this.user.email = value.email.toLocaleLowerCase();
  }


  // fonction qui permet de changer le statut du membre dans l'entreprise
  // change = (event) => event.value === 'rh' ? this.isRh = true : this.isRh = false;
  change (event) {

    // définit le role dans l'entreprise
      event.value === 'stagiaire' ? this.user.role ="salarié" : null;
      event.value === 'rh' ? this.user.role = 'RH' : null;
      event.value === 'organisme' ? this.user.role = 'organisme' : null;
    // permet d'afficher contrat et nom d'entreprise
      event.value === 'rh' ? this.isRh = true : this.isRh = false;
      event.value === 'organisme' ? this.isOrganisme = true : this.isOrganisme = false; 
      // console.log(this.user.role) 
    }
   




  // Création d'un compte entreprise par invitation
  addCompanyToUser() {
    // ce user entreprise id = 
    this.user.eid = this.companyToAddToUser.id;
    this.user.company = this.companyToAddToUser.name;
    this.user.role ="salarié"
    // ajouter user.id à la liste de membres
    this.companyToAddToUser.members.push(this.user.id);
    this.companyToAddToUser.employees ? null : this.companyToAddToUser.employees = [];
    // push user.id à la liste des employés
    this.companyToAddToUser.employees.push(this.user.id);
    // met à jour l'entreprise
    this.companyService.updateCompany(this.companyToAddToUser)
    // console.log(this.user.id)
    // console.log(this.user.eid)
    // console.log(this.companyToAddToUser.id)
  }




  // création d'un compte todolist
  async addTodolistToUser() {
    this.user.id = this.userToAddToTodolist.id;
    this.todoService.updateTodo(this.userToAddToTodolist)
  }


  
  // création de l'entreprise
  // fonction lancée depuis createUserToBdd()
  async createCompany(companyName: string) {
    // le nom de l'entreprise correspond au nom de l'entreprise du 1er user
    this.user.company = companyName;
    
    // défini le contenu de l'objet Company
    let companyToAdd = { name: this.user.company, members: [this.user.id] } as Company;
    // S'il y a un nouvel utilisateur, le push dans l'entreprise
    this.userToAddToCompany ? companyToAdd.members.push(this.userToAddToCompany.id) : null;
    
    // creation de l'entreprise avec les informations "companytoadd"
    return this.companyService.createCompany(companyToAdd).then(company => {
      this.user.eid = company.id;
          this.createUser();  
          // l'utilisateur récupère l'identifiant de l'entreprise
          
          this.slackService.sendMessageToSlackToInscription('Nouvelle entreprise : ' + this.user.company);
         
          // mise à jour de l'entreprise avec de nouvelles informations dans l'objet => ID
          let companyToAdd = { name: this.user.company, members: [this.user.id], id: company.id }
          this.user.eid = company.id;
          this.companyService.updateCompany(companyToAdd)
        })
        // .catch(error => console.log("TCL: SignInComponent -> addUserToBdd -> error", error));
     
  }




  // création todolist
  // fonction lancée depuis createUserToBdd()
  async createTodolist(Todolist: string) {
    this.user.Todolist = Todolist;
    this.Todolist = { id: this.user.id } as todolist;
      return this.todoService.createTodo(this.Todolist).then(Todolist => {
        this.user.id = Todolist.id;
      })
      // .catch(error => console.log("TCL: SignInComponent -> addUserToBdd -> error", error));
  }

  open(){
    this.modalService.dismissAll(); 
    this.modalService.open(LoginComponent, { size: 'lg', centered: true});
  }

  close(){
    this.modalService.dismissAll(); 
  }



  // ajout de mots clés
  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    this.user.userKeywords = this.userKeywords;
    
    // Add motCle
    if ((value || '').trim()) {
      this.user.userKeywords.push(value.trim());
  
      // console.log(this.user.userKeywords);
      // console.log(value);

      this.slackService.sendMessageToSlackToFormation('Nouvelle Newsletter : ' + value + "" + this.user.email);

    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }
  remove(userKeyword: string): void {
    const index = this.user.userKeywords.indexOf(userKeyword);
    // console.log(userKeyword)

    if (index >= 0) {
      this.user.userKeywords.splice(index, 1);
    }
  }
}