import { CompanyService } from './../../../../services/firestore/company.service';
import { UsersService } from './../../../../services/firestore/users.service';
import { ObservableService } from '../../../../services/utils/observable.service';
import { AuthService } from './../../../../services/auth/auth.service';
import { SeoService } from './../../../../services/fetch/seo.service';
import { Component, Input } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { MatSnackBar } from '@angular/material/snack-bar';
import { User } from '../../../../interfaces/user';
import { Location } from '@angular/common';
import { Company } from '../../../../interfaces/company';
import { SlackService } from '../../../../services/fetch/slack.service';
import { SpreadsheetService } from '../../../../services/fetch/spreadsheet.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SignInComponent } from '../sign-in/sign-in.component';



@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  @Input() isModal: boolean;



  userToConnect: User;
  userToAddToCompany: User;
  companyToAddToUser: Company;

  passwordCode: string;
  form: FormGroup;
  errorMessage = '';

  constructor(
    private location: Location,
    private fb: FormBuilder,
    private seoService: SeoService,
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private slackService: SlackService,
    private authService: AuthService,
    private usersService: UsersService,
    private companyService: CompanyService,
    private spreadsheetService: SpreadsheetService,
    private observableService: ObservableService,
    private modalService: NgbModal,

  ) {
    this.createForm('', '');
  }

  ngOnInit() {
    this.route.queryParams.subscribe(query => {
      query.oobCode && query.mode === "resetPassword" ? this.handleResetPassword(query.oobCode) : null;
      query.oobCode && query.mode === "verifyEmail" ? this.handleVerifyEmail(query.oobCode) : null;
      query.uidToAddToEid ? this.getUserByUid(query.uidToAddToEid) : null;
      query.eidToAddToUid ? this.getCompanyByEid(query.eidToAddToUid) : null;
    });

    let tags = {
      title: "IANOS, Connectez vous ou inscrivez vous",
      description: "IANOS, Connectez vous ou inscrivez vous. Nous vous proposerons la formation de vos rêves"
    }
    this.seoService.generateTags(tags);
  }

  getUserByUid(uid: string) {
    this.usersService.getUserById(uid).snapshotChanges()
      .subscribe(user => this.userToAddToCompany = { id: user.payload.id, ...user.payload.data() as User });

  }

  getCompanyByEid(eid: string) {
    this.companyService.getCompanyById(eid).snapshotChanges()
      .subscribe(company => this.companyToAddToUser = { id: company.payload.id, ...company.payload.data() as Company});
  }

  handleResetPassword(oobCode: string) {
    this.passwordCode = oobCode;
    this.authService.handleResetPassword(oobCode).then(success => this.createForm(success, ""));
  }

  handleVerifyEmail(oobCode: string) {
    this.authService.handleVerifyEmail(oobCode)
      .then(() => {
        this.openSnackBar("Votre compte est verifié", "Valider");
        this.router.navigate(["/"]);
      })
      .catch(error => this.spreadsheetService.sendError(error));
  }

  createForm(email: string, password: string) {
    email ? null : email = '';
    password ? null : password = '';
    this.form = this.fb.group({
      email: [email, Validators.required],
      password: [password, Validators.required],
      stagiaire: true
    });
  }

  loginWithGoogle(value) {
    this.authService.doRegisterWithGoogle()
      .then(result => {
        value.email = result.user.email;
        value.id = result.user.uid;
        this.userLogin(value);
      })
      .catch(error => {
        this.spreadsheetService.sendError(error);
        this.errorMessage = this.authService.translateErrorMessage(error, value.email);
      });
  }

  loginWithFacebook(value) {
    this.authService.doRegisterWithFacebook()
      .then(result => {
        value.email = result.user.email;
        value.id = result.user.uid;
        this.userLogin(value);
      }).catch(error => {
        this.spreadsheetService.sendError(error);
        this.errorMessage = this.authService.translateErrorMessage(error, value.email);
      });
  }

  tryLogin(value) {
    if (value.email.includes("@")) {
      value.email = value.email.replace(' ', '');
      this.authService.doLogin(value)
        .then(result => {
          value.id = result.user.uid;
          this.userLogin(value)
        })
        .catch(error => {
          this.spreadsheetService.sendError(error);
          this.errorMessage = this.authService.translateErrorMessage(error, value.email);
        });
    }
  }

  userLogin(value) {
    this.isModal ? this.modalService.dismissAll() : null;
    this.observableService.triggerLogout(false);
    this.observableService.currentUser.subscribe(user => {
      user && user.eid && this.userToAddToCompany ? this.addUserToCompany(user) : null;
    });
    this.companyToAddToUser ? this.addCompanyToUser(value) : null;

    this.router.navigate(['/']);
    this.openSnackBar(value.email + ' correctement connecté', "Page d'accueil");
    this.modalService.dismissAll(); 
  }

  addUserToCompany(user: User) {
    this.companyService.getCompanyById(user.eid).valueChanges()
      .subscribe((company: Company) => {
        this.userToAddToCompany.eid = company.id;
        this.userToAddToCompany.company = company.name;
        company.members.includes(this.userToAddToCompany.id) ? null : company.members.push(this.userToAddToCompany.id);
        this.usersService.updateUser(this.userToAddToCompany);
        this.companyService.updateCompany(company)
          .then(() => this.slackService.sendMessageToSlackToInscription("L'utilisateur " + user.email + " est entré dans l'entreprise : " + this.companyToAddToUser.name))
          .catch(error => this.spreadsheetService.sendError(error));
      });
  }

  addCompanyToUser(user: User) {
    user.eid = this.companyToAddToUser.id;
    user.company = this.companyToAddToUser.name;
    this.companyToAddToUser.members.push(user.id);
    this.companyToAddToUser.employees ? null : this.companyToAddToUser.employees = [];
    this.companyToAddToUser.employees.push(user.id);
    this.usersService.updateUser(user);
    this.companyService.updateCompany(this.companyToAddToUser)
      .then(() => this.slackService.sendMessageToSlackToInscription("L'utilisateur " + user.email + " est entré dans l'entreprise : " + this.companyToAddToUser.name))
      .catch(error => this.spreadsheetService.sendError(error));
  }

  openSnackBar(message: string, action: string) {
    let snackRef = this.snackBar.open(message, action, { duration: 10000 });

    snackRef.onAction().subscribe(() => action == "Page d'accueil" ? this.router.navigate(['/']) : null);
    snackRef.onAction().subscribe(() => action.includes('@') ? this.sendEmailResetPassword(action) : null);
  }

  sendEmailResetPassword(email: string) {
    email.includes("@") ? this.authService.changePassword(email) : this.errorMessage = "Entrez votre adresse mail";
  }

  changePassword(value) {
    this.authService.confirmResetPassword(this.passwordCode, value.password)
      .then(() => {
        this.openSnackBar("Mot de passe modifié avec succès", "Valider")
        this.passwordCode = null;
        this.location
        this.location.go("/connexion");
      })
      .catch(error => {
        this.spreadsheetService.sendError(error);
        this.errorMessage = this.authService.translateErrorMessage(error, value.email);
      });
  }

  open(){
    this.modalService.dismissAll(); 
    this.modalService.open(SignInComponent, { size: 'lg', centered: true});
  }


  close(){
    this.modalService.dismissAll(); 
  }
}