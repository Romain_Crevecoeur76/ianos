import { SessionService } from './../../../../services/firestore/session.service';
import { BookingService } from './../../../../services/firestore/booking.service';
import { SlackService } from './../../../../services/fetch/slack.service';
import { Session } from './../../../../interfaces/session';
import { User } from './../../../../interfaces/user';
import { Formation } from './../../../../interfaces/formations';
import { Place } from './../../../../interfaces/place';
import { CalendarService } from '../../../../services/utils/calendar.service';
import { ObservableService } from '../../../../services/utils/observable.service';
import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'get-sessions-modal',
  templateUrl: './get-sessions-modal.component.html',
  styleUrls: ['./get-sessions-modal.component.scss'],
})
export class GetSessionsModalComponent implements OnInit {
  @Input() formation: Formation;
  @Input() user: User;
  @Input() sessions: Session[];

  dateArray: any[];
  sid: string;

  constructor(
    private slackService: SlackService,
    private bookingService: BookingService,
    private snackBar: MatSnackBar,
    private observableService: ObservableService,
    private sessionService: SessionService,
    public calendarService: CalendarService,
    private modalService: NgbModal,
  ) { }

  ngOnInit() {
    this.calendarService.dateArray = [];

    if (this.sessions[0]) {
      this.handleSessions();
    }
    else {
      this.sessionService.getSessionsByFid(this.formation.id).get()
        .then(query => query.forEach(session => {
          this.sessions.push({ id: session.id, ...session.data() as Session});
          this.handleSessions();
        }))
        .catch(error => console.log("TCL: GetSessionsModalComponent -> ngOnInit -> error", error))
    }
  }

  handleSessions = () => this.sessions.forEach(session => session.dateString ? this.parseDateStringAndPushToArray(session) : null);

  parseDateStringAndPushToArray(session: Session) {
    let sessionToPush = JSON.parse(session.dateString)
    this.calendarService.dateArray.push({ toDate: sessionToPush.toDate, fromDate: sessionToPush.fromDate });
    this.sid = session.id;
  }


  createBooking() {
    let booking = this.bookingService.fillBooking(this.formation, this.user);
    booking.dateString = JSON.stringify(this.dateArray);
    booking.sid ? booking.sid = this.sid : null;
    booking.isPending = true;
    this.bookingService.isAlreadyBooked(this.user.id, this.formation.id)
      .onSnapshot(query => {
        if (query.empty) {
          this.bookingService.addBooking(booking)
            .then(booking => {
              this.observableService.triggerContext({ bid: booking.id });
              this.modalService.dismissAll();
              this.openSnackBar("Le prestataire a reçu votre demande et vous contactera au plus vite", "Valider");
              //   this.sendEmail.sendMailPreinscription(this.user, this.formation);
              this.redirect();
              this.slackService.sendMessageToSlackToInscription("L'utilisateur : " + this.user.email + " a fait une demande de réservation pour : " + booking.id);
              this.calendarService.dateArray = null;
            })
            .catch(error => console.log("TCL: GetSessionsModalComponent -> addDateToPlaceOrSession -> error", error))
        }
        else {
          this.modalService.dismissAll();
          this.openSnackBar("Vous avez déjà une réservation pour cette formation", "Valider");
        }
      })

  }

  redirect = () => this.formation.certification ? this.observableService.triggerChat('préinscription') : this.observableService.triggerChat('enregistrement');

  stringify = (dateArrayToPush: any[], placeToUpdate: Place) => placeToUpdate.dateAskingPreInscrit.push(JSON.stringify(dateArrayToPush));

  addDate() {
    let sid = ''
    this.dateArray ? null : this.dateArray = [];
    this.calendarService.dateArray.forEach(item => this.isDateBetween(item) ? sid = item.sid : null);
    this.dateArray.push({ toDate: this.calendarService.toDate, fromDate: this.calendarService.fromDate, sid: sid });
    this.calendarService.dateArray = this.dateArray;
  }

  isDateBetween(item: any) {
    return (this.calendarService.toDate && this.calendarService.fromDate.after(item.fromDate) && this.calendarService.toDate.before(item.toDate) || this.calendarService.toDate.equals(item.toDate) || this.calendarService.fromDate.equals(item.fromDate))
      || (this.calendarService.fromDate.after(item.fromDate) && this.calendarService.fromDate.before(item.toDate) || this.calendarService.toDate.equals(item.toDate) || this.calendarService.fromDate.equals(item.toDate))
  }

  openSnackBar = (message: string, action: string) => this.snackBar.open(message, action, { duration: 10000 });

  close = () => this.modalService.dismissAll();
}
