import { UsersService } from './../../../../services/firestore/users.service';
import { Trainer } from './../../../../interfaces/trainer';
import { FormationsService } from './../../../../services/firestore/formations.service';
import { BookingService } from './../../../../services/firestore/booking.service';
import { Review } from './../../../../interfaces/review';
import { ReviewService } from './../../../../services/firestore/review.service';
import { SlackService } from './../../../../services/fetch/slack.service';
import { SessionService } from './../../../../services/firestore/session.service';
import { ObservableService } from '../../../../services/utils/observable.service';
import { Session } from './../../../../interfaces/session';
import { Place } from './../../../../interfaces/place';
import { User } from './../../../../interfaces/user';
import { Formation } from './../../../../interfaces/formations';
import { SignInComponent } from './../../sign/sign-in/sign-in.component';
import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { GetSessionsModalComponent } from '../get-sessions-modal/get-sessions-modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Location } from '@angular/common';
import { SeoService } from '../../../../services/fetch/seo.service';
import { ContactComponent } from '../../contact/contact.component';
import { CalendarService } from '../../../../services/utils/calendar.service';
import { count } from 'rxjs/operators';



@Component({
  selector: 'get-session',
  templateUrl: './get-session.component.html',
  styleUrls: ['./get-session.component.scss']
})
export class GetSessionComponent implements OnInit {
  @Input() formation: Formation;
  @Input() user: User;
  @Input() contact: ContactComponent;
  
  trainer: Trainer;

  urlToShare: string;

  places: Place[];
  sessions: Session[] = [];
  session: Session = {};
  ville: string;

  buttonSubscribe: string = '';
  textToShare: string;

  reviewString = '';
  reviews: Review[] = [];
  sid: string;
  sendEmail: any;
  

  viewNumber: number;
  

  constructor(
    private location: Location,
    private router: Router,
    private route: ActivatedRoute,
    private observableService: ObservableService,
    private bookingService: BookingService,
    private reviewService: ReviewService,
    private formationsService: FormationsService,
    private sessionService: SessionService,
    public calendarService: CalendarService,
    private modalService: NgbModal,
    private snackBar: MatSnackBar,
    private slackService: SlackService,
    private seoService: SeoService,
  ) {
    this.observableService.currentUser.subscribe(user => user && !this.user ? this.user = user : null);
  }



  ngOnInit() {
    this.calendarService.dateArray = [];
    // this.sessionService.getSessionsByPid;
    this.formation ? this.initModal() : null;
    
    this.route.queryParams.subscribe(query => {query.fid ? this.getFormation(query.fid) : null;})
    this.route.params.subscribe(query => {query.fid ? this.getFormation(query.fid) : null;});
    
    // this.viewNumber = 5;
    // console.log(this.viewNumber);
  }



  getFormation(fid: string) {
    this.formationsService.getFormationById(fid).snapshotChanges().subscribe(formation => {
        this.formation = { id: formation.payload.id, ...formation.payload.data() as Formation}
        this.getReview(fid);
      });
  }


  seoChange() {
    // this.formation.intitule && this.router.url.includes('formation') ? this.location.go('/formation/' + this.formation.id + '/' + this.formation.intitule.replace(/ /g, "-")) : null;

    this.seoService.generateTags({
      title: this.formation.intitule,
      description: this.formation.objectifs,
    })
  }

  getReview(fid: string) {
    this.reviewService.getReviewByFid(fid).get().then(query => {
        query.forEach(review => {
          this.reviews.push({ id: review.id, ...review.data() as Review})
        })
      });
  }

  initModal() {
    this.seoChange();

    this.getReview(this.formation.id);
    this.urlToShare = 'https://www.ianos.io/formation/' + this.formation.id + '/' + this.formation.intitule.replace(/ /g, "-");
    this.textToShare = 'Pensez vous que la formation : ' + this.formation.intitule + " puisse m'apporter un plus ? \n Vous pouvez trouver plus d'information sur celle ci ici : " + this.urlToShare;

    if (this.formation.pid) {
      this.sessionService.getSessionsByPid(this.formation.pid).get()
        .then(query => query.forEach(session => {
          let sessionToShow = { id: session.id, ...session.data() as Session };
          this.sessions.push(sessionToShow);
        }))
        .catch(error => console.log("​GetSessionComponent -> ngOnInit -> error", error));
    }
  }

  submit() {
    if (this.user && !this.user.isOrganisme) {
      const modalRef = this.modalService.open(GetSessionsModalComponent, { size: 'lg' });
      modalRef.componentInstance.formation = this.formation;
      modalRef.componentInstance.user = this.user;
      modalRef.componentInstance.sessions = this.sessions;
      this.slackService.sendMessageToSlackToInscription("tentative d'inscription" + this.formation.intitule + " ID : " + this.formation.id + " Je suis : " + this.user.email);
    }
    else {
      this.redirect();
    }
  }

  getNewDate() {
    this.slackService.sendMessageToSlackToInscription("Me prévenir quand une date se libère pour la formation : : " + this.formation.intitule + " ID : " + this.formation.id + " Je suis : " + this.user.email);
    this.modalService.dismissAll();
    if (!this.user) {
      this.modalService.open(SignInComponent, { size: 'lg' });
      this.openSnackBar("Vous pouvez vous inscrire pour reçevoir plus d'informations", "S'inscrire");
    } else {
      this.openSnackBar("Nous vous prévenons quand une date se libère pour la formation : " + this.formation.intitule, "Valider");
    }
  }

  openSnackBar(message: string, action: string) {
    let snackBarRef = this.snackBar.open(message, action, { duration: 10000 });

    snackBarRef.onAction().subscribe(() => {
      action == "S'inscrire" ? this.router.navigate(['/inscriptions/' + this.formation.id]) : null;
    });
  }

  formationEnLigne() {
    if (!this.user.isOrganisme) {
      this.slackService.sendMessageToSlackToInscription("J'ai cliqué sur le lien de la formation en ligne : " + this.formation.intitule + " ID : " + this.formation.id + " Je suis : " + this.user.email);
      !this.formation.urlweb.includes('https') || !this.formation.urlweb.includes('http') ? this.formation.urlweb = 'http://' + this.formation.urlweb : null;
      window.open(this.formation.urlweb);
    }
  }

  createBooking(bookingState: string) {
    if (this.user) {
      this.bookingService.isAlreadyBooked(this.user.id, this.formation.id).get()
        .then(query => {
          if (query.empty) {
            let reservation = this.bookingService.fillBooking(this.formation, this.user);
            reservation[bookingState] = true;
            this.bookingService.addBooking(reservation)
              .then(booking => {
                if (bookingState === "isFavorite") {
                  this.observableService.triggerContext({ bid: booking.id });
                  if (this.location.path().includes("bot")) {
                    this.formation.certification ? this.observableService.triggerChat('préinscription') : this.observableService.triggerChat('enregistrement');
                  }
                  this.openSnackBar("Formation ajoutée aux favoris avec succès", "Valider");
                }
                if (bookingState === "isProposition") {
                  this.router.navigate(['/dashboard', { outlets: { 'dashboard': ['proposition', booking.id] } }])
                }
                this.modalService.dismissAll();

              })
              .catch(error => console.log("TCL: GetSessionComponent -> toFavorite -> error", error));
          }
          else {
            this.modalService.dismissAll();
            this.snackBar.dismiss();
            this.openSnackBar("Vous avez déjà une réservation pour cette formation", "Valider");
          }

        })
    }
    else {
      this.redirect();
    }
  }
  
  elearningBooking() {
    if (this.user){
      let booking = this.bookingService.fillBooking(this.formation, this.user);
      booking.sid ? booking.sid = this.sid : null;
      booking.isPending = true;
      this.bookingService.isAlreadyBooked(this.user.id, this.formation.id)
        .onSnapshot(query => {
          if (query.empty) {
            this.bookingService.addBooking(booking)
              .then(booking => {
                this.observableService.triggerContext({ bid: booking.id });
                this.openSnackBar("Le prestataire a reçu votre demande et vous contactera au plus vite", "Valider");
                this.sendEmail.sendMailPreinscription(this.user, this.formation);
                this.slackService.sendMessageToSlackToInscription("L'utilisateur : " + this.user.email + " a fait une demande de réservation pour : " + booking.id);
              })
          }
        })
      }
    else {
      this.redirect();
    }
  }

  shareOnLinkedin = () => window.open('https://www.linkedin.com/sharing/share-offsite/?url=' + this.urlToShare, "myWindow", "width=400,height=500");

  shareOnFacebook = () => window.open('https://www.facebook.com/sharer/sharer.php?u=' + this.urlToShare + 'src=sdkpreparse' + this.urlToShare, "myWindow", "width=400,height=600");

  shareOnTwitter = () => window.open('https://twitter.com/intent/tweet?text=' + this.textToShare + '&url=' + this.urlToShare, "myWindow", "width=500,height=300");

  shareOnMailTo = () => window.location.href = "mailto:mail@example.org?subject=" + this.formation.intitule + "&body=" + this.textToShare + ' / ' + this.urlToShare;

  addComment() {
    if(!this.user) {
      this.snackBar.open("Veuillez vous connecter pour commenter cette formation");
      return;
    }
    let prenomUser = '';
    let email = '';
    let reviewToAdd: Review = {
      fid: this.formation.id,
      comment: this.reviewString,
      dateCreated: new Date().getTime(),
      isQuestion: true
    }
    if (this.user) {
      reviewToAdd.uid = this.user.id;
      email = this.user.email;
      this.user.prenom ? prenomUser = this.user.prenom : prenomUser = this.user.email.split('@')[0];
    }
    else {
      email = prenomUser = 'Anonyme';
    }
    reviewToAdd.prenom = prenomUser;
    this.reviewString = "";
    this.reviewService.addReview(reviewToAdd)
      .then(review => {
        reviewToAdd.id = review.id;
        this.reviews.push(reviewToAdd);
        this.slackService.sendMessageToSlackToProblemes("Question à la formation : https://ianos.io/formation/" + this.formation.id + " Par l'utilisateur " + email + " à propos de : " + reviewToAdd.comment);
      })
      .catch(fail => console.log("TCL: GetSessionComponent -> addComment -> fail", fail));
  }

  addResponse(review: Review) {
    review.responses ? null : review.responses = [];
    let response = { nom: this.user.nom, prenom: this.user.prenom, message: review.lastResponse, role: this.user.role }
    response.role ? null : response.role = "Salarié";
    review.responses.push(response);
    review.lastResponse = "";
    console.log("TCL: GetSessionComponent -> addResponse -> review", review)
    this.reviewService.updateReview(review);
  }

  deleteReview(reviewToDelete: Review) {
    this.reviewService.deleteReview(reviewToDelete)
      .then(() => {
        this.reviews = this.reviews.filter(review => review.id !== reviewToDelete.id);
      })
  }

  deleteResponse(review: Review, responseToDelete: string) {
    review.responses = review.responses.filter(response => response !== responseToDelete);
    this.reviewService.updateReview(review);
  }

  redirect() {
    this.modalService.dismissAll();

    if (this.user && this.user.isOrganisme) {
      this.openSnackBar("Vous ne pouvez vous inscrire à une formation en tant qu'organisme de formation", "Valider");
    }
    else {
      this.modalService.open(SignInComponent, { size: 'lg', centered: true});
      // this.router.navigate(['/inscriptions'], { queryParams: { 'pid': this.formation.pid } });
    }
  }

  // toContact() {
  //   this.modalService.dismissAll();
  //   this.router.navigate(['/contact']);
  // }
  open(contact: ContactComponent) {
    const modalRef = this.modalService.open(ContactComponent);
    modalRef.componentInstance.formation = contact;
  }

  


  toFaq = () => document.querySelector('#faq').scrollIntoView();

  close = () => this.router.url.split("/")[0] == "formation" ? this.router.navigate(["/"]) : this.modalService.dismissAll();

}