import { SpreadsheetService } from './../../../../services/fetch/spreadsheet.service';
import { SignInComponent } from './../../sign/sign-in/sign-in.component';
import { Component, ElementRef, AfterViewInit, ViewChildren, QueryList, Input } from '@angular/core';
import { DialogflowService } from '../../../../services/fetch/dialogflow.service';
import { ObservableService } from '../../../../services/utils/observable.service';
import { Message } from '../../../../interfaces/message';
import { User } from '../../../../interfaces/user';
import { ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
// import { VoiceRecognitionService } from '../../../../services/utils/voice-recognition.service';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { LoginComponent } from '../../sign/login/login.component';

@Component({
  selector: 'app-bot',
  templateUrl: './bot.component.html',
  styleUrls: ['./bot.component.scss']
})
export class BotComponent implements AfterViewInit {
  @ViewChildren('inputMessage') inputMessage: QueryList<ElementRef>;
  @ViewChildren('scrollMe') scrollMeEl: QueryList<ElementRef>;
  scrollTop: number = null;

  @Input() toggleBot: boolean;

  user: User;
  messages: Message[] = [];

  suggestions = ["Je cherche une formation", "Je cherche un Financement", "Je voudrais référencer mon catalogue"];

  isMessageSending = false;
  isListening = false;
  isOffers = false;
  showSearchButton = true;
  isSafari: boolean;

  sessionId: string;

  placeholderValue = 'Écrivez votre intention';

  constructor(
    private dialogflowService: DialogflowService,
    private spreadsheetService: SpreadsheetService,
    private observableService: ObservableService,
    // private voiceRecognitionService: VoiceRecognitionService,
    private router: ActivatedRoute,
    private modalService: NgbModal,
    private snackBar: MatSnackBar
  ) {
    this.observableService.currentUser.subscribe(user => user ? this.user = user : this.user = null);
    this.router.queryParams.subscribe(async param => {
      await this.deleteMessage();
      let message = param.message as string;
      this.isFitForSending(message) ? this.sendMessage(null, param.message) : null;
      window.history.replaceState({}, '', `/bot`);
    });
    this.router.paramMap.subscribe(param => {
      this.deleteMessage();
      // param.get("vocal") ? this.activateSpeech() : null;
    })

    let ua = navigator.userAgent.toLowerCase();
    ua.indexOf('safari') != -1 && ua.indexOf('chrome') < -1 ? this.isSafari = true : this.isSafari = false;
    if (localStorage.getItem('messages')) {
      JSON.parse(localStorage.getItem('messages')).forEach(message => this.messages.push(message as Message));
      this.sessionId = this.messages[0].sessionId;
    }
  }

  isFitForSending = (message: string) => message && !message.includes('/') && !message.includes('&') && !message.includes('=');

  ngAfterViewInit() {
    let i = 0;
    let speed = 50;
    this.placeholder(i, this.placeholderValue, speed)
  }

  sendMessage(inputMessage: HTMLInputElement, message?: string) {
    if (this.isMessageSending) {
      return;
    }
    this.suggestions = [];
    this.isMessageSending = true;
    let messageString = "";
    message ? messageString = message : (messageString = inputMessage.value, inputMessage.value = null);

    if (messageString.length !== 0 && this.isMessageSending) {
      let messageUser = { content: messageString, isBot: false } as Message;
      this.sessionId ? null : this.sessionId = this.dialogflowService.getNewSessionId();
      messageUser.sessionId = this.sessionId;
      this.messages.push(messageUser);
      this.dialogflowService.getIntent(messageUser)
        .then(result => {
          result.json().then(query => {
            this.isMessageSending = false;
            let messageBot = { isBot: true } as Message;
            query[0].queryResult.fulfillmentMessages[0] ? messageBot.content = query[0].queryResult.fulfillmentMessages[0].text.text[0] : null;
            query[0].queryResult.fulfillmentMessages[1] ? this.handleAction(query[0].queryResult.fulfillmentMessages[1].payload.fields.action.stringValue) : null;
            messageBot.content ? null : messageBot.content = "Je n'ai pas compris votre message";
            this.messages.length < 2 ? messageBot.content = "Bonjour, merci de votre confiance ! " + messageBot.content : null;
            messageBot.allRequiredParamsPresent = query[0].queryResult.allRequiredParamsPresent
            if (query[0].queryResult.intent) {
              messageBot.intent = query[0].queryResult.intent.displayName;
              this.handleIntent(messageBot.intent);
              messageBot.contexts = this.handleParameters(query[0].queryResult.parameters.fields)
            }
            messageBot.content.includes('~') ? (inputMessage.placeholder = "", messageBot.content = this.createPlaceholder(messageBot.content)) : null;
            messageBot.content.includes('&') ? messageBot.content = this.createButton(messageBot.content) : null;

            // this.isListening ? this.textToSpeech(messageBot.content) : null;

            this.messages.push(messageBot);
            // localStorage.setItem('messages', JSON.stringify(this.messages));
            inputMessage ? inputMessage.focus() : null;
          })
        })
        .catch(error => this.spreadsheetService.sendError(error));
    }
  }

  handleAction(action) {
    let modalRef: NgbModalRef;
    action === "openModalSignin" ? modalRef = this.modalService.open(SignInComponent) : null;
    action === "openModalLogin" ? modalRef = this.modalService.open(LoginComponent) : null;
    modalRef ? modalRef.componentInstance.isModal = true : null;
  }


  handleParameters(parameters: any[]) {
    let context = [];
    Object.entries(parameters).forEach(parameter => {
      parameter[1].stringValue && parameter[1].stringValue !== "" ? context[parameter[0]] = parameter[1].stringValue.toLowerCase() : null;
      if (parameter[1].listValue) {
        parameter[1].listValue.values.forEach(value => {
          value.stringValue && value.stringValue !== "" ? context[parameter[0]] = value.stringValue.toLowerCase() : null;
        })
      }
    })
    return context;
  }

  handleIntent(intent: string) {
    intent.includes("Demo - offres") || intent === "DemandeDevis" ? this.isOffers = true : this.isOffers = false;
    intent === "InfoEntreprise - no" ? this.suggestions = ["Partager sur Facebook", "Linkedin", "Twitter"] : null;
  }

  // setSpeech() {
  //   return new Promise(
  //     function (resolve, reject) {
  //       let synth = window.speechSynthesis;
  //       let id = setInterval(() => {
  //         if (synth.getVoices().length !== 0) {
  //           resolve(synth.getVoices());
  //           clearInterval(id);
  //         }
  //       }, 10);
  //     }
  //   )
  // }

  // textToSpeech(content: string) {
  //   let indexVoice: any;
  //   let s = this.setSpeech();
  //   s.then((voices: any) => {
  //     voices.forEach(element => {
  //       element.name === 'Google français' ? indexVoice = element : null
  //     });
  //     let message = new SpeechSynthesisUtterance(content);
  //     message.voice = indexVoice
  //     message.lang = navigator.language;
  //     window.speechSynthesis.speak(message);
  //   });

  // }

  // activateSpeech(inputMessage?: HTMLInputElement): void {
  //   if (this.isListening) {
  //     this.voiceRecognitionService.DestroySpeechObject();
  //     this.isListening = false;
  //   }
  //   else {
  //     this.isListening = true;
  //     this.showSearchButton = false;
  //     this.snackBar.open("Nous vous écoutons", "Arrêter")
    
  //       .onAction().subscribe(test => {
  //       console.log("TCL: BotComponent -> test", test)
  //         this.voiceRecognitionService.DestroySpeechObject()
  //       } );
  //     this.voiceRecognitionService.record()
  //       .subscribe(value => {
  //         if (inputMessage) {
  //           inputMessage.value = value;
  //           // inputMessage.placeholder = "";
  //           // this.placeholder(1, "~" + value, 50);
  //           this.sendMessage(inputMessage);
  //         }
  //         else {
  //           this.sendMessage(null, value);
  //         }
  //       },
  //         (err) => {
  //           console.log(err);
  //           if (err.error == "no-speech") {
  //             console.log("--restatring service--");
  //             this.activateSpeech(inputMessage);
  //           }
  //         },
  //         () => {
  //           this.snackBar.dismiss();
  //           this.showSearchButton = true;
  //           console.log("--complete--");
  //           this.activateSpeech(inputMessage);
  //         });
  //   }
  // }

  createButton(content: string) {
    let array = content.split('&');
    let arrayToReturn = array.shift();
    this.suggestions = array.filter(value => value !== " " && value);
    return arrayToReturn;
  }

  createPlaceholder(content: string) {
    let array = content.split('~');
    let contentSliced = array.splice(1, 1);
    console.log("TCL: BotComponent -> createPlaceholder -> contentSliced", contentSliced)
    this.placeholderValue = contentSliced[0];
    this.ngAfterViewInit();
    return array[0] + array[1];
  }

  placeholder(i: number, placeholder: string, speed: number) {
    console.log("TCL: BotComponent -> placeholder -> placeholder", placeholder)
    if (placeholder && i < placeholder.length && this.inputMessage && this.inputMessage.first) {
      this.inputMessage.first.nativeElement.placeholder = this.inputMessage.first.nativeElement.placeholder + placeholder.charAt(i);
      i++;
      setTimeout(() => this.placeholder(i, placeholder, speed), speed);
    }
  }

  async deleteMessage() {
    this.messages = [];
    localStorage.removeItem("messages")
    this.sessionId = null;
    this.suggestions = ["Je cherche une formation", "Je cherche un Financement"];
  }

  isCarouselFormation = (message: Message, isLast: boolean) => {
    this.scrollTop = this.scrollMeEl.first.nativeElement.scrollHeight
    return message.contexts && message.contexts['motCle'] && isLast
  }

}
