// import { AddFormationComponent } from './../../../organisme/add-formation/add-formation.component';
// import { SpreadsheetService } from './../../../../services/fetch/spreadsheet.service';
// import { LoginComponent } from './../../sign/login/login.component';
// import { BookingService } from './../../../../services/firestore/booking.service';
// import { ObservableService } from './../../../../services/observable.service';
// import { DialogflowService } from './../../../../services/fetch/dialogflow.service';
// import { SlackService } from './../../../../services/fetch/slack.service';
// import { Context } from './../../../../interfaces/context';
// import { User } from './../../../../interfaces/user';
// import { Message } from './../../../../interfaces/message';
// import { Component, OnInit, Input, OnDestroy, ViewChild, ElementRef } from '@angular/core';
// import { ActivatedRoute } from '@angular/router';
// import { Subscription } from 'rxjs/internal/Subscription';
// import { SignInComponent } from '../../sign/sign-in/sign-in.component';
// import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
// import { MatSnackBar } from '@angular/material';

// @Component({
//   selector: 'message-form',
//   templateUrl: './message-form.component.html',
//   styleUrls: ['./message-form.component.scss'],
// })
// export class MessageFormComponent implements OnInit, OnDestroy, User {
//   @Input('message') message: Message;
//   @Input('messages') messages: Message[];
//   @Input() user: User;

//   contexts: Context;
//   geoPoint: any;
//   subscribe: Subscription;

//   @ViewChild('inputToFocus') inputToFocus: ElementRef

//   constructor(
//     private slackService: SlackService,
//     private dialogflowService: DialogflowService,
//     private spreadsheetService: SpreadsheetService,
//     private observableService: ObservableService,
//     private bookingService: BookingService,
//     private router: ActivatedRoute,
//     private modalService: NgbModal,
//     private snackBar: MatSnackBar
//   ) {
//     this.observableService.currentContext.subscribe(result => result ? this.contexts = result : null)

//     this.subscribe = this.observableService.currentChat.subscribe(message => {
//       this.messages ? null : this.messages = [];
//       message && message.geoPoint ? (message = message.ville, this.geoPoint = message.geoPoint) : null;
//       if (message && this.isMessage(this.messages)) {
//         // this.message = new Message(message, '', null, null, false, null, null);
//         this.sendMessage()
//       }
//     })
//   }

//   isMessage = (messages: Message[]) => messages.length === 0 || messages[messages.length - 1].isBot;

//   isFitForSending = (message: string, messages: Message) => message && !message.includes('/') && !message.includes('&') && !message.includes('=') && ((messages && !(message == messages.content)) || !messages);

//   ngOnInit() {
//     this.router.queryParams.subscribe(param => {
//       let message = param.message as string;
//       if (this.isFitForSending(message, this.messages[this.messages.length - 2])) {
//         this.message.content = message;
//         this.sendMessage();
//       }
//     });
//   }

//   ngOnDestroy = () => this.subscribe.unsubscribe();

//   public sendMessage(): void {
//     this.message.timestamp = new Date();
//     this.message.isBot = false;
//     if (this.message.content) {
//       this.messages.push(this.message);
//       this.message.sessionId ? null : this.message.sessionId = '';

//       this.dialogflowService.getIntent(this.message)
//         .then(query =>
//           query.json().then(message => {
//             message[0].queryResult.fulfillmentMessages[1] ? this.handleAction(message[0].queryResult.fulfillmentMessages[1].payload.fields.action.stringValue) : null;
//             this.handleResponse(message[0].queryResult)
//           }))
//         .catch(error => this.spreadsheetService.sendError(error, this.user));
//     }
//     // this.message = new Message('', "../assets/images/user.png", new Date, this.message.contexts, false, this.message.sessionId, this.message.intent);
//   }

//   handleAction(action: string) {
//     action === "openModalSignin" ? this.modalService.open(SignInComponent, { size: 'lg' }) : null;
//     action === "openModalLogin" ? this.modalService.open(LoginComponent, { size: 'lg' }) : null;
//   }

//   handleResponse(response) {
//     let speech = response.fulfillmentText;
//     let intent = "";
//     response.intent ? intent = response.intent.displayName : null;
//     let contexts = response.outputContexts;
//     let allRequiredParamsPresent = response.allRequiredParamsPresent;

//     contexts[0] && contexts[0].parameters && contexts[0].parameters.fields.motCle && contexts[0].parameters.fields.motCle.stringValue !== "" && intent === "recherche" ? this.observableService.triggerCarousel(true) : null;
//     allRequiredParamsPresent && intent.includes("financement - ") ? this.observableService.triggerCarouselFinancement(true) : null;
//     allRequiredParamsPresent && intent === "actions_of" ? this.handleActionOf() : null
//     if (intent && intent !== 'ianos') {
//       contexts[0] ? this.handleContext(contexts) : null;
//       this.changeMessage(intent);
//     }
//     else {
//       speech = this.noIntent();
//     }
//     // this.messages.push(new Message(speech, "../assets/images/bot.png", new Date, this.message.contexts, true, this.message.sessionId, intent, allRequiredParamsPresent));
//     localStorage.setItem('messages', JSON.stringify(this.messages));
//   }

//   handleActionOf() {
//     if (this.user.isOrganisme) {
//       this.modalService.open(AddFormationComponent);
//     }
//     else {
//       this.snackBar.open("Vous n'êtes pas connecté en tant qu'organisme de formation", "Valider");
//     }
//   }

//   handleContext(context: any[]) {
//     this.message.contexts ? null : this.message.contexts = [];
//     this.geoPoint ? context.push(this.geoPoint) : null;
//     this.message.contexts.push(context[0]);
//   }

//   fromFavoriteToPending() {
//     this.observableService.currentContext.subscribe(context => {
//       if (context.bid) {
//         this.bookingService.getBookingById(context.bid).snapshotChanges()
//           .subscribe(booking => {
//             let bookingToUpdate = { id: booking.payload.id, ...this.bookingService.toPending(booking.payload.data()) };
//             this.bookingService.updateBooking(bookingToUpdate)
//           })
//       }
//     })
//   }

//   changeMessage(intentName: string) {
//     this.message.intent = intentName;
//     intentName === "reservation" ? this.fromFavoriteToPending() : null;
//   }

//   noIntent() {
//     if (this.messages[1]) {
//       this.slackService.sendMessageToSlackToProblemes("Pas d'intent pour : " + this.messages[1].content + "/ Utilisateur : " + this.user.email);
//       this.message.sessionId = null;
//       return "Je n'ai pas compis ce que vous me dites mais j'ai prévenu mon équipe ! #Je cherche une formation en " + this.messages[1].content;
//     }
//   }

//   deleteContexts() {
//     this.message = { content: "" };
//     this.messages = Array(this.messages.shift());
//     this.observableService.triggerChat(null);
//     this.observableService.triggerCarousel(false);
//     this.observableService.triggerCarouselFinancement(false);
//     localStorage.removeItem('messages');
//     this.observableService.triggerRefresh(true);
//   }

//   ngAfterViewInit() {
//     let i = 0;
//     let placeholder = 'Je cherche une formation certifiante en Anglais à Rouen';
//     let speed = 50;
//     this.placeholder(i, placeholder, speed)
//   }

//   placeholder(i: number, placeholder: string, speed: number) {
//     if (placeholder && i < placeholder.length && this.inputToFocus) {
//       this.inputToFocus.nativeElement.placeholder = this.inputToFocus.nativeElement.placeholder + placeholder.charAt(i);
//       i++;
//       setTimeout(() => this.placeholder(i, placeholder, speed), speed);
//     }
//   }
// }
