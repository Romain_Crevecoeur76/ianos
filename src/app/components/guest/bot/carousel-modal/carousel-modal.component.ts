import { EntitiesService } from './../../../../services/firestore/entities.service';
import { ObservableService } from '../../../../services/utils/observable.service';
import { Component, OnInit, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { Context } from '../../../../interfaces/context';

@Component({
  selector: 'carousel-modal',
  templateUrl: './carousel-modal.component.html',
  styleUrls: ['./carousel-modal.component.scss']
})
export class CarouselModalComponent implements OnInit {
  @Input() context: Context;

  arrayRecherche: string[] = [];
  myControl = new FormControl();
  filteredOptions: Observable<string[]>;

  constructor(
    private observableService: ObservableService,
    private entitiesService: EntitiesService
  ) { }

  ngOnInit() {
    this.context.motCle === 'Surprends Moi' ? this.context.motCle = "" : null;
    this.context.ville ? this.context.modalite = "présentiel" : null;
    this.getContextsOptions();
    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );
  }

  deleteDuplicate(array: string[]) {
    return array.filter(function (elem, index, self) {
      return index === self.indexOf(elem);
    })
  }

  private _filter = (value: string): string[] => this.arrayRecherche.filter(option => option.toLowerCase().indexOf(value.toLowerCase()) === 0);

  change = () => this.observableService.triggerContext(this.context);

  getContextsOptions() {
    this.entitiesService.getMotCleEntities().get().toPromise()
      .then(doc => !doc.exists ? null : doc.data()['entities'].forEach(entitie => this.arrayRecherche.push(entitie.value)))
      .catch(error => console.log("​CarouselComponent -> getContextsOptions -> error", error));
    this.arrayRecherche = this.deleteDuplicate(this.arrayRecherche);
  }

  onKeydown = (event) => event.key === "Enter" ? this.changeKeyword(event.path[0].value) : null;

  deleteKeyword(motCleToDelete: string) {
    this.context.motCleArray = this.context.motCleArray.filter((motCle: string) => motCle !== motCleToDelete);
    this.change();
  }

  changeKeyword(motCle: string) {
    this.context.motCle = motCle.toLowerCase();
    this.change();
  }
}
