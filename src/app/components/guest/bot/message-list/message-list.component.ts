// import { ObservableService } from './../../../../services/observable.service';
// import { User } from './../../../../interfaces/user';
// import { Message } from './../../../../interfaces/message';
// import { Component, OnInit, Input, AfterViewInit, ViewChild, ViewChildren, QueryList, ElementRef, Output } from '@angular/core';
// import { MessageItemComponent } from '../message-item/message-item.component';
// import { Subscription } from 'rxjs/internal/Subscription';

// @Component({
//   selector: 'message-list',
//   templateUrl: './message-list.component.html',
//   styleUrls: ['./message-list.component.scss']
// })
// export class MessageListComponent implements OnInit, AfterViewInit {
//   @Input() messages: Message[];

//   @Output() @Input() user: User;

//   // @ViewChild('chatlist', { read: ElementRef }) chatList: ElementRef;
//   @ViewChildren(MessageItemComponent, { read: ElementRef }) chatItems: QueryList<MessageItemComponent>;

//   link: string;
//   display: string;
//   subscription: Subscription;

//   isRecherche = false;
//   isFinancement = false;
//   scrollToBottomBoolean: boolean;

//   constructor(
//     private observableService: ObservableService,
//   ) {
//     this.subscription = this.observableService.currentToBottom.subscribe(message => {
//       message ? setTimeout(() => this.scrollToBottom(), 300) : null;
//     });

//     this.observableService.currentIsRecherche.subscribe(isRecherche => {
//       isRecherche ? this.isRecherche = true : this.isRecherche = false
//     });

//     this.observableService.currentIsFinancement.subscribe(isFinancement => {
//       isFinancement ? this.isFinancement = true : this.isFinancement = false
//     });
//   }

//   ngOnDestroy() {
//     this.subscription.unsubscribe()
//   }

//   ngAfterViewInit() {
//     this.chatItems.changes.subscribe(() => this.scrollToBottom());
//   }

//   ngOnInit() {
//     this.scrollToBottom();
//     // this.messages[0] ? null : this.messages.push(new wMessage('', '../../../assets/images/bot.png', new Date, null, true));

//     if (this.messages[this.messages.length - 1]) {
//       let lastMessage = this.messages[this.messages.length - 1] as Message;
//       lastMessage.allRequiredParamsPresent && lastMessage.intent.includes("financement - ") ? this.isFinancement = true : null;

//       if (lastMessage.contexts) {
//         this.isRecherche = false;
//         lastMessage.intent.includes("recherche") && lastMessage.contexts[0].parameters.fields.motCle.stringValue !== "" ? this.isRecherche = true : null;
//       }
//     }

//   }

//   private scrollToBottom(): void {
//     try {
//       // this.chatList.nativeElement.scrollTop = this.chatList.nativeElement.scrollHeight;
//     }
//     catch (err) {
//       console.log('Could not find the "chatList" element.');
//     }
//   }

//   onSubmit(button: string): void {
//     this.observableService.triggerChat(button);
//   }

//   focusInput() {
//     document.getElementById("inputToFocus").focus();
//   }
// }