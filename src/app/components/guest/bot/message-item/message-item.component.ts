// import { MapService } from '../../../../services/fetch/map.service';
// import { EntitiesService } from './../../../../services/firestore/entities.service';
// import { ObservableService } from '../../../../services/utils/observable.service';
// import { Place } from './../../../../interfaces/place';
// import { PlaceService } from './../../../../services/firestore/place.service';
// import { Message } from './../../../../interfaces/message';
// import { Component, OnInit, Input } from '@angular/core';
// import { FormControl } from '@angular/forms';
// import { Observable } from 'rxjs';
// import { map, startWith } from 'rxjs/operators';

// @Component({
//   selector: 'message-item',
//   templateUrl: './message-item.component.html',
//   styleUrls: ['./message-item.component.scss'],
// })
// export class MessageItemComponent implements OnInit {
//   @Input() isLast: boolean;
//   @Input() message: Message;
//   @Input() messages: Message[];

//   buttonsPlace: Place[];
//   buttons: string[];
//   list: string[] = [];
//   link: string;

//   myControl = new FormControl();
//   filteredOptions: Observable<string[]>;

//   constructor(
//     private observableService: ObservableService,
//     private entitiesService: EntitiesService,
//     private mapService: MapService,
//     private placeService: PlaceService
//   ) {
//     this.filteredOptions = this.myControl.valueChanges
//       .pipe(
//         startWith(''),
//         map(value => this._filter(value)));
//   }

//   private _filter(value: string): string[] {
//     const filterValue = value.toLowerCase();
//     return this.list.filter(option => option.toLowerCase().includes(filterValue));
//   }

//   ngOnInit() {
//     this.getButtonAndLinkFromDialogflow();
//   }
//   onSubmit = (button: string) => this.observableService.triggerChat(button);

//   getButtonAndLinkFromDialogflow = () => {
//     if (this.message.isBot && this.message.content) {
//       this.buttons = this.message.content.split('#').splice(1);
//       this.message.content = this.message.content.split('#').shift();

//       if (this.buttons[0] === "Secteur d'activité") {
//         this.entitiesService.getSecteurEntities().get().toPromise()
//           .then(doc => doc.data().entities.forEach(entitie => this.list.push(entitie.value)));
//       }
//       else if (this.buttons[0] === "activité principale") {
//         this.entitiesService.getSecteurActivitePrincipale().get().toPromise()
//           .then(doc => doc.data().entities.forEach(entitie => this.list.push(entitie.value)));
//       }
//       else if (this.buttons[0] === "Région") {
//         this.entitiesService.getRegions().get().toPromise()
//           .then(doc => doc.data().entities.forEach(entitie => this.list.push(entitie.value)));
//       }
//       else if (this.message.content.includes('http')) {
//         this.link = "http" + this.message.content.split("http").splice(1, 2).toString()
//         this.message.content = '<p>' + this.message.content
//           .replace(/[A-Za-z]+:\/\/[A-Za-z0-9\-_]+\.[A-Za-z0-9\-_:%&;\?\#\/.=]+/g, `<a href=` + this.link + ` target="_blank" style="color:white;text-decoration: underline">Suivre le lien : ` + this.link + `</a>`) + '</p>';
//       }
//       else if (this.message.content.includes("Où")) {
//         navigator.geolocation.getCurrentPosition(position => {
//           this.mapService.getPlaceWithLatitudeAndLongitude(position.coords)
//             .then(query => {
//               this.buttonsPlace = [];
//               query.results.forEach(result => {
//                 result.address_components.forEach(item => {
//                   if (item.types[0] == "locality") {
//                     this.buttonsPlace.push({ ville: item.long_name, geoPoint: this.placeService.fillGeoPoint(position.coords.latitude, position.coords.longitude) });
//                   }
//                 })
//               })
//               this.buttons = [];
//               this.buttonsPlace = this.deleteDuplicate(this.buttonsPlace);
//             })
//         });
//       }
//     }
//   }

//   deleteDuplicate(array) {
//     const unique = array
//       .map(e => e['ville'])
//       .map((e, i, final) => final.indexOf(e) === i && i)
//       .filter(e => array[e]).map(e => array[e]);
//     return unique;
//   }
// }