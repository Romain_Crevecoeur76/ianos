import { ObservableService } from './../../../../services/utils/observable.service';
import { BookingService } from './../../../../services/firestore/booking.service';
import { MapService } from './../../../../services/fetch/map.service';
import { PlaceService } from './../../../../services/firestore/place.service';
import { FormationsService } from './../../../../services/firestore/formations.service';
import { Place } from './../../../../interfaces/place';
import { Formation } from './../../../../interfaces/formations';
import { User } from './../../../../interfaces/user';
import { Context } from './../../../../interfaces/context';
import { Component, OnInit, Input, NgZone } from '@angular/core';
import { Booking } from '../../../../interfaces/booking';
import { SpreadsheetService } from '../../../../services/fetch/spreadsheet.service';
import { Message } from '../../../../interfaces/message';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ContactComponent } from '../../contact/contact.component';


@Component({
  selector: 'carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss'],
})
export class CarouselComponent implements OnInit {
  @Input() contexts: Context;
  @Input() message: Message;
  @Input() user: User;

  formation: Formation;
  formations: Formation[] = [];
  places: Place[] = [];

  showContact = false;
  scrollToBottom = false;

  motCle: string;
  desktopSize: boolean;

  constructor(
    private spreedsheetService: SpreadsheetService,
    private formationService: FormationsService,
    private observableService: ObservableService,
    private bookingService: BookingService,
    private placeService: PlaceService,
    private mapService: MapService,
    private modalService: NgbModal,
    private zone: NgZone,
  ) { }

  ngOnInit() {

    // s'il y a contexts alors getFormations, sinon rien
    this.contexts ? this.getFormations() : null;
    this.spreedsheetService.sendTrainingAsking(this.user, this.message)
  }

  getBooking(bid: string) {
    this.bookingService.getBookingById(bid).valueChanges().subscribe(booking => this.getFormation(booking));
  }

  // montre les formation "réservées"
  getFormation(booking: Booking) {
    this.formations = [];
    this.formationService.getFormationById(booking.fid).valueChanges()
      .subscribe(formation => this.formations[0] = { ...formation as Formation, ville: booking.ville })
  }

// show formations
  getFormations() {
    // s'il n'y a pas de motCle détecté alors affiche composant contact
    if (!this.contexts.motCle) {
      this.showContact = true;
      return;
    }
    // console.log("TCL: CarouselComponent -> getFormations -> this.contexts", this.contexts)

    // s'il y a motCle alors affiche formations selon le contexts défini par le service formation
    this.formationService.getFormationsByContext(this.contexts).onSnapshot(query => {
        // si pas de entity alors ouvre page contact
        if (query.empty) {
          if (this.contexts.motCle.includes(" ")) {
            this.contexts.motCle = this.contexts.motCle.split(" ")[0];
            this.getFormations();
          }
          else {
            // this.showContact = true;
            this.modalService.open(ContactComponent);
          }
        }

        // si entities alors montre formation dans un ordre
        else {
          query.forEach(formation => {
            let formationToShow = { id: formation.id, ...formation.data() as Formation } as Formation;
            console.log(formationToShow)

            // si pas elearning dans le context alors montre formations 
            if (this.contexts.modalite && this.contexts.modalite !== "Elearning") {

              if (formationToShow.placeArray) {
                formationToShow.placeArray.forEach(place => {
                  // si l'utilisateur indique une ville alors montre les formations selon la ville
                  place.ville.toLocaleLowerCase() === this.contexts.modalite ? this.addFormationPlace(place, formationToShow) : null;
                });
              }
              else {
                // sinon push les formations et leurs datas dans le carousel
                this.formations.push(formationToShow);
              }
            }
            else {
              // si indication d'une ville alors montre les formations uniquement avec ville (addFormationPlace)
              if (formationToShow.placeArray) {
                formationToShow.placeArray.forEach(place => this.addFormationPlace(place, formationToShow));
              }
              else {
                this.formations.push(formationToShow);
              }
            }

            // fin pas elearning
            this.orderFormations();
            this.observableService.triggerCarousel(true);
          })
        }
        
        this.zone.run(() => { });
      },
        error => this.spreedsheetService.sendError(error)
      )
  }


 
  addFormationPlace(place: Place, formationToShow: Formation) {
    let formationToShowWithPlace = this.fillFormation(place, formationToShow);
    if (!this.formations.includes(formationToShowWithPlace)) {
      this.formations.push(formationToShowWithPlace)
    }
  }

  fillFormation(place: Place, formationToShow: Formation) {
    return {
      id: formationToShow.id,
      ville: place.ville,
      region: place.region,
      geoPoint: place.geoPoint,
      pid: place.id,
      isDate: place.isDate,
      ...formationToShow
    } as Formation;
  }


  orderFormations() {

    this.formations.forEach((formation, index, formations) => {
      formation.baseSearchPoint ? formation.searchPoint = formation.baseSearchPoint : formation.searchPoint = 0;
      this.sort(formations);

      if (this.contexts.modalite !== "Elearning") {
        if (formation.geoPoint && this.contexts.geoPoint) {
          formation.searchPoint = formation.searchPoint + 200 - this.placeService.distance(formation.geoPoint, this.contexts.geoPoint);
          formation.searchPoint = this.getPointAndSort(formation);
          this.sort(formations);
        }
        if (formation.ville && this.contexts.ville) {
          this.distanceBetweenTown(formation.ville, this.contexts.ville)
            .then(distance => {
              formation.searchPoint = formation.searchPoint + 200 - distance.rows[0].elements[0].distance.value / 1000;
              formation.searchPoint = this.getPointAndSort(formation);
              this.sort(formations);
            })
            .catch(error => this.spreedsheetService.sendError(error))
        }
      }
      else {
        formation.searchPoint = this.getPointAndSort(formation);
        this.sort(formations);
      }
    })
  }

  getPointAndSort(formation) {
    if (this.contexts.motCleArray[1]) {
      this.contexts.motCleArray.forEach(keyword => formation.keywords.includes(keyword) ? formation.searchPoint = formation.searchPoint + 100 : null)
    }
    formation.certification == this.contexts.certification ? (formation.searchPoint = formation.searchPoint + 100) : null;
    formation.modalite && formation.modalite.toLowerCase() == this.contexts.modalite ? (formation.searchPoint = formation.searchPoint + 350) : null;
    formation.sessionArray ? formation.searchPoint = formation.searchPoint + formation.sessionArray.length * 4 : null;
    return formation.searchPoint
  }

  distanceBetweenTown = (start, end) => this.mapService.distanceBetweenTwoTown(start, end);

  sort(formations: Formation[]) {
    formations.sort((a , b) => {
      if (a.searchPoint > b.searchPoint) {
        return -1;
      }
      if (a.searchPoint < b.searchPoint) {
        return 1;
      }
      return 0;
    });
  }




  fillContexts() {
    // this.context['Ville'] ? this.context.ville = this.context['Ville'] : null;
    // if (isNumber(this.context['_lat'])) {
    //   this.context.geoPoint = this.placeService.fillGeoPoint(this.context['_lat'], this.context['_long']);
    // }
    // this.context.ville || this.context.geoPoint ? this.getGeoPoint() : this.getFormations();
    // this.context && this.context.motCleArray[0] && window.location.hostname !== "localhost" ? this.adminService.updateContext(this.context) : null;
  }

  getGeoPoint() {
    if (this.contexts.geoPoint) {
      this.getFormations();
    }
    else {
      this.mapService.getPlaceWithString(this.contexts.ville)
        .then(query => {
          if (query.candidates[0]) {
            this.contexts.geoPoint = this.placeService.fillGeoPoint(query.candidates[0].geometry.location.lat, query.candidates[0].geometry.location.lng);
          }
          this.getFormations();
        })
    }
  }
}