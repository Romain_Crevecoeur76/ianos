// import { ObservableService } from '../../../../services/utils/observable.service';
// import { Message } from './../../../../interfaces/message';
// import { User } from './../../../../interfaces/user';
// import { Component, OnInit, OnDestroy, Output } from '@angular/core';
// import { Subscription } from 'rxjs/internal/Subscription';
// import Unsplash from 'unsplash-js';

// @Component({
//   selector: 'chatform',
//   templateUrl: './chatform.component.html',
//   styleUrls: ['./chatform.component.scss']
// })
// export class ChatformComponent implements OnInit, OnDestroy {
//   @Output() user: User;

//   public message: Message;
//   public messages: Message[];
//   public messageTest: Message;
//   subscription: Subscription;
//   userCheck: boolean = false;

//   backgroundUrl = "https://source.unsplash.com/wD1LRb9OeEo";

//   constructor(
//     private observableService: ObservableService
//   ) {
//     this.observableService.currentUser.subscribe(user => user ? this.user = user : this.user = null);
//     this.observableService.currentChat.subscribe(message => {
//       message ? this.getBackgroundImage(message) : null;
//     })
//     // this.message = new Message('', '../../../assets/images/user.png', new Date)
//     this.messages = []
//     this.subscription = this.observableService.currentRefresh.subscribe(trigger => {
//       // trigger ? (this.messages = [], this.messages.push(new Message('', '../../../assets/images/bot.png', new Date, null, true))) : null;
//     })
//   }

//   ngOnInit() {
//     this.messages = [];
//     if (localStorage.getItem('messages')) {
//       JSON.parse(localStorage.getItem('messages')).forEach(message => this.messages.push(message as Message));
//     }
//   }

//   getBackgroundImage(message) {

//     const unsplash = new Unsplash({
//       applicationId: "580e048fbd65e8e048c508e68cc6990c9ec25e7e80ca003fff6bfdd88fe9d6ec",
//       secret: "02a2ded89481c435ec13725d9d6b499d830728a5bae179b41b2492575abc8f46"
//     });

//     unsplash.search.photos("anglais")
//       .then(query => query.json())
//       .then(json => this.backgroundUrl = json.results[Math.floor((Math.random() * json.total) + 1)].urls.full);
//   }

//   ngOnDestroy = () => this.subscription.unsubscribe();
// }
