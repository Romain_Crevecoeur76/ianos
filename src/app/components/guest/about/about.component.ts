import { SeoService } from './../../../services/fetch/seo.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  constructor(
    private seoService: SeoService,
  ) { }

  ngOnInit() {
    let tags = {
      title: "👩‍🏫👩‍🎓 IANOS, Cherchez, comparez, réservez votre formation professionnelle et trouvez les moyens de la financer 👨‍🏫👨‍🎓",
      description: "👩‍🏫👩‍🎓 👨‍🏫👨‍🎓 En France, 80 000 organismes de formation sont inscrits dans le Répertoire National des Certifications Professionnelles. Ce chiffre est huit fois plus important que notre voisin allemand. Cette abondance d’offre ainsi que la complexité administrative liée au financement de celle-ci ralentissent l’accès à la formation professionnelle. IANOS est le premier site de référencement et de réservation de formation professionnelle pour les personnes prenant l’initiative de se former. En quelques cliques, les particuliers peuvent rechercher, sélectionner et réserver la formation qui leur correspond le mieux selon plusieurs critères: Le domaine de compétence, le lieu, le prix, la modalité pédagogique ou encore les notes des stagiaires précédents."
    }
    this.seoService.generateTags(tags);
  }

}
