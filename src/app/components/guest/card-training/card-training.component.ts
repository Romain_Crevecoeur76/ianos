import { Component, Input } from '@angular/core';
import { Formation } from '../../../interfaces/formations';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { GetSessionComponent } from '../bot/get-session/get-session.component';
import { MatDialog } from '@angular/material/dialog';
import { SeoService } from '../../../services/fetch/seo.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ObservableService } from '../../../services/utils/observable.service';


@Component({
  selector: 'card-training',
  templateUrl: './card-training.component.html',
  styleUrls: ['./card-training.component.scss']
})
export class CardTrainingComponent {
  @Input() formation: Formation

  constructor(
    private modalService: NgbModal,
    public dialog: MatDialog,
    private router: Router,
    private route: ActivatedRoute,
    private observableService: ObservableService,
    private seoService: SeoService,
  ) { }
  
  getDuree(formation: Formation) {
    if (!formation.duree && formation.nombreHeures && formation.dureeSeance) {
      return formation.nombreHeures * formation.dureeSeance;
    }
    else {
      return formation.duree;
    }
  }

  // redirige vers la formation quand on clique via le chatbot
  open(formation: Formation) {
    this.router.navigate(['/formation/' + this.formation.id + '/' + this.formation.intitule.replace(/ /g, "-")])
  }

  autoOpenVideo(formation: Formation) {
    formation.urlVideoYoutube = formation.urlVideoYoutube + '?autoplay=1';
    this.open(formation);
  }
}


// open(formation: Formation) {
  // const modalRef = this.modalService.open(GetSessionComponent, { size: 'lg', windowClass : "myCustomModalClass" });
  // const modalRef = this.dialog.open(GetSessionComponent, { width: "1500px" });
  // modalRef.componentInstance.formation = formation;
// }