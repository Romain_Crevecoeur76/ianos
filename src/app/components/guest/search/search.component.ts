import { PlaceService } from './../../../services/firestore/place.service';
import { SeoService } from '../../../services/fetch/seo.service';
import { FormationsService } from '../../../services/firestore/formations.service';
import { Formation } from '../../../interfaces/formations';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CarouselModalComponent } from '../bot/carousel-modal/carousel-modal.component';
import { Context } from '../../../interfaces/context';
import { MatDialog } from '@angular/material/dialog';
import { ObservableService } from '../../../services/utils/observable.service';
import { Location } from '@angular/common';
import { Place } from '../../../interfaces/place';

import { EntitiesService } from './../../../services/firestore/entities.service';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';


@Component({
  selector: 'search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  context: Context = {};
  formation: Formation;
  formations: Formation[] = [];

  // filtre
  arrayRecherche: string[] = [];
  myControl = new FormControl();
  filteredOptions: Observable<string[]>;

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private formationsService: FormationsService,
    private observableService: ObservableService,
    private placeService: PlaceService,
    private seoService: SeoService,
    public dialog: MatDialog,
    private entitiesService: EntitiesService

  ) {
    this.observableService.currentContext.subscribe(context => {
      if (context) {
        this.context = context;
        this.getKeywordOrTown();
      }
    })
  }

  ngOnInit() {
    // change l'url de la page
    this.route.paramMap.subscribe(query => {
      console.log("TCL: SearchComponent -> ngOnInit -> query", query)
      
      this.context.motCle = query.get('keyword').replace('%20', ' ');

      query.get('town') ? this.context.ville = query.get('town').replace(/-/g, ' ') : null;

      this.getKeywordOrTown();

    })

  // filtre au chargement de la page
    this.context.motCle === 'Surprends Moi' ? this.context.motCle = "" : null;

    // si condition context.ville
    this.context.ville ? this.context.modalite = "présentiel" : null;

    this.getContextsOptions();

    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );
  // fin filtre au chargement

  }

  // au chargement de la page, récupère motCle et ville
  getKeywordOrTown() {
    this.formations = [];
    // si motCle alors affiche formations par motCle, sinon affiche formation par ville
    this.context.motCle ? this.getFormationByKeyword() : this.getFormationByTown();
    
    // si ville alors création URL motCle + ville
    if (this.context.ville) {
      this.location.go('/formations/' + this.context.motCle + '/' + this.context.ville);
    }
    // sinon (indication motCle uniquement) création URL motCle
    else {
      this.location.go('/formations/' + this.context.motCle);
    }
  }

  // affiche les formation selon le motCle et la Ville
  getFormationByTown() {
    
    this.placeService.getPlacesByTown(this.context.ville).onSnapshot(query => {
      
      query.forEach(place => {
        this.formationsService.getFormationById(place.data()['fid']).snapshotChanges().subscribe(formation => {
            let formationToShow = { id: formation.payload.id, ...formation.payload.data() as Formation};
            formationToShow.ville = place.data()['ville'];
            formationToShow.pid = place.data()['id'];
            this.formations.push(formationToShow);
            console.log(this.context.ville);
          })
      })
    })
  }

 


  // Affiche les formations selon le motCle 
  getFormationByKeyword() {
    
    this.formationsService.getFormationsByKeyword(this.context.motCle.toLocaleLowerCase())
    
      .onSnapshot(query => {
        // affiche les données des formations
        query.docChanges().forEach(formation => {
          let formationToShow = { id: formation.doc.id, ...formation.doc.data() as Formation };

          // permet d'afficher les villes sur les cards
          // Si le context n'est pas elearning
          if (this.context.modalite && this.context.modalite == "présentiel") {
            
            
            // original
            if (this.context.ville && formationToShow.placeArray) {
              formationToShow.placeArray.forEach(place => 
                 place.ville === this.context.ville ? this.addFormationPlace(place, formationToShow) : null);  
            }

            // if (this.context.ville && formationToShow.placeArray) {
            //   formationToShow.placeArray.forEach(place => {
            //     place.ville === this.context.modalite ? this.addFormationPlace(place, formationToShow) : null;
            //   });  
            // }

           
            

            else {
              // sinon push les formations et leurs datas dans le carousel
              this.formations.push(formationToShow);
            }
          }
          else {
            // montre les formations avec ville (addFormationPlace)
            if (formationToShow.placeArray) {
              formationToShow.placeArray.forEach(place => this.addFormationPlace(place, formationToShow));
            }
            else {
              this.formations.push(formationToShow);
            }
          }
          
          this.seoService.generateTags({
            title: formationToShow.intitule,
            description: formationToShow.objectifs,
          })
        });
      })
  }


addFormationPlace(place: Place, formationToShow: Formation) {
  let formationToShowWithPlace = this.fillFormation(place, formationToShow);
  if (!this.formations.includes(formationToShowWithPlace)) {
    this.formations.push(formationToShowWithPlace)
  }
}

fillFormation(place: Place, formationToShow: Formation) {
  return {
    id: formationToShow.id,
    ville: place.ville,
    geoPoint: place.geoPoint,
    pid: place.id,
    isDate: place.isDate,
    ...formationToShow
  } as Formation;
}




  // filtre
  deleteDuplicate(array: string[]) {
    return array.filter(function (elem, index, self) {
      return index === self.indexOf(elem);
    })
  }

  private _filter = (value: string): string[] => this.arrayRecherche.filter(option => option.toLowerCase().indexOf(value.toLowerCase()) === 0);

  change = () => this.observableService.triggerContext(this.context);
  

  // option de autocomplete pendant la recherche de mot clé
  getContextsOptions() {
    this.entitiesService.getMotCleEntities().get().toPromise()
      .then(doc => !doc.exists ? null : doc.data()['entities'].forEach(entitie => this.arrayRecherche.push(entitie.value)))
      .catch(error => console.log("​CarouselComponent -> getContextsOptions -> error", error));
    this.arrayRecherche = this.deleteDuplicate(this.arrayRecherche);
  }

  onKeydown = (event) => event.key === "Enter" ? this.changeKeyword(event.path[0].value) : null;

  deleteKeyword(motCleToDelete: string) {
    this.context.motCleArray = this.context.motCleArray.filter((motCle: string) => motCle !== motCleToDelete);
    this.change();
    console.log(this.context.motCleArray)
  }

  changeKeyword(motCle: string) {
    this.context.motCle = motCle.toLowerCase();
    this.change();
  }
// fin filtre
  

}
