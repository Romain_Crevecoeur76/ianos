import { Pipe, PipeTransform } from '@angular/core';
import { Formation } from '../interfaces/formations';

@Pipe({
  name: 'filterCarousel'
})
export class FilterCarouselPipe implements PipeTransform {

  transform(formationsToSort: Formation[]) {
    if (formationsToSort.length > 7 && formationsToSort.filter(formation => formation.searchPoint > 5).length > 7) {
      formationsToSort = formationsToSort.filter(formation => formation.searchPoint > 5);
    }
    return formationsToSort;
  }
}
