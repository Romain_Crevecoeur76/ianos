import { Pipe, PipeTransform } from '@angular/core';
import { CalendarService } from '../services/utils/calendar.service';

@Pipe({
  name: 'parseDateString'
})
export class ParseDateStringPipe implements PipeTransform {

  constructor(public calendarService: CalendarService) { }

  transform(dateString: string): string {
    dateString = dateString.replace("[", "").replace("]", "")
    let dateToReturn = JSON.parse(dateString);
    if (dateToReturn.fromDate && dateToReturn.toDate) {
      return 'Du ' + this.calendarService.stringifyDate(dateToReturn.fromDate) + ' Au ' + this.calendarService.stringifyDate(dateToReturn.toDate);
    }
    else {
      return 'Le ' + this.calendarService.stringifyDate(dateToReturn.fromDate);
    }
  }

}
