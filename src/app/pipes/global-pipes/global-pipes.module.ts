import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ParseDateStringPipe } from '../parse-date-string.pipe';

@NgModule({
  declarations: [
    ParseDateStringPipe
  ],
  imports: [
    CommonModule    
  ],
  exports: [
    ParseDateStringPipe
  ]
})
export class GlobalPipesModule { }
