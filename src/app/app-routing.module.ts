import { ListeMembreComponent } from './components/organisme/liste-membre/liste-membre.component';
import { GetSessionComponent } from './components/guest/bot/get-session/get-session.component';
import { DashboardCompanyComponent } from './components/user/dashboard-company/dashboard-company.component';
import { AllTrainingComponent } from './components/guest/all-training/all-training.component';
import { BookedComponent } from './components/user/booked/booked.component';
// import { FavoriteComponent } from './components/user/favorite/favorite.component';
// import { PendingComponent } from './components/user/pending/pending.component';
// import { PropositionComponent } from './components/user/proposition/proposition.component';
import { RequestComponent } from './components/user/request/request.component';
import { SearchComponent } from './components/guest/search/search.component';
import { CategoriesComponent } from './components/guest/categories/categories.component';
import { BatisseurComponent } from './components/guest/batisseur/batisseur.component';
import { HomeComponent } from './components/guest/home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router'; 

import { AuthGuardStagiaire as AuthGuard } from './services/auth/auth.guard';
import { AuthGuardOrganisme } from './services/auth/auth.guard-organisme';
import { LoginComponent } from './components/guest/sign/login/login.component';
import { SignInComponent } from './components/guest/sign/sign-in/sign-in.component';
import { ProviderComponent } from './components/guest/provider/provider.component';
import { ContactComponent } from './components/guest/contact/contact.component';
import { CguComponent } from './components/guest/cgu/cgu.component';
import { CharteComponent } from './components/guest/charte/charte.component';
import { CGVComponent } from './components/guest/cgv/cgv.component';
import { ApportAffairesComponent } from './components/guest/apport-affaires/apport-affaires.component';
import { AboutComponent } from './components/guest/about/about.component';
import { FinancementComponent } from './components/guest/financement/financement.component';

import { ProfilComponent } from './components/user/profil/profil.component';
import { AuthGuardAdmin } from './services/auth/auth.guard-admin';
import { BotComponent } from './components/guest/bot/bot/bot.component';
import { DashboardComponent } from './components/user/dashboard/dashboard.component';

// entreprise
import { CardMemberComponent } from './components/entreprise/card-member/card-member.component';
import { PageEntrepriseComponent } from './components/entreprise/page-entreprise/page-entreprise.component';
import { EspaceEntrepriseComponent } from './components/entreprise/espace-entreprise/espace-entreprise.component';


const routes: Routes = [

  { path: 'connexion', component: LoginComponent },
  {
    path: 'inscriptions', component: SignInComponent,
    children: [{ path: ':pid', component: SignInComponent }],
  },
  { path: 'batisseur', component: BatisseurComponent },
  { path: 'fournisseur', component: ProviderComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'cgu', component: CguComponent },
  { path: 'charte', component: CharteComponent },
  { path: 'cgv', component: CGVComponent },
  { path: 'apport-affaires', component: ApportAffairesComponent },
  { path: 'about', component: AboutComponent },
  { path: 'financement', component: FinancementComponent },
  { path: 'toutes-les-formations', component: AllTrainingComponent },

  // entreprise
  { path: 'espace-entreprise', component: EspaceEntrepriseComponent },
  { path: 'card-member', component: CardMemberComponent },
  { path: 'page-entreprise', component: PageEntrepriseComponent },
  { path: 'request', component: RequestComponent },
  { path: 'dashboard-company', component: DashboardCompanyComponent },

  { path: 'formation', component: GetSessionComponent },
  {
    path: 'formation/:fid', component: GetSessionComponent,
    children: [{ path: ':title', component: GetSessionComponent }],
  },
  { path: 'categorie/:titreCategory', component: CategoriesComponent },
  { path: 'formations/:keyword', component: SearchComponent },
  { path: 'formations/:keyword/:town', component: SearchComponent },
  
  
  {
    path: 'profil', canActivate: [AuthGuard], component: ProfilComponent,
    children: [{ path: ':uid', canActivate: [AuthGuardAdmin], component: ProfilComponent }],
  },

  // { path: 'proposition', component: PropositionComponent },

  {
    path: 'organisme',
    loadChildren: () => import('./components/organisme/organisme.module').then(m => m.OrganismeModule),
    canActivate: [AuthGuardOrganisme],
    data: { preload: false }
  },

  {
    path: 'dashboard-company', canActivate: [AuthGuard], component: DashboardCompanyComponent,
    children: [
      // { path: '', component: PendingComponent, outlet: 'dashboard' },
      // { path: 'liste-membre/:uid/:bid', component: ListeMembreComponent, outlet: 'dashboard' },
      // { path: 'proposition/:bid', component: PropositionComponent, outlet: 'dashboard' },
      // { path: 'pending/:bid', component: PendingComponent, outlet: 'dashboard' },
      // { path: 'booked/:bid', component: BookedComponent, outlet: 'dashboard' }
    ]
  },
  {
    path: 'admin',
    loadChildren: () => import('./components/admin/admin.module').then(m => m.AdminModule),
    canActivate: [AuthGuardAdmin],
    data: { preload: false } 
  },

  { path: 'bot', component: BotComponent },

  {
    path: '', component: HomeComponent,
    pathMatch: 'full',
    runGuardsAndResolvers: 'always',
  },
  { path: '**', component: HomeComponent }
];

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forRoot(routes, {
    scrollPositionRestoration: 'top',
    relativeLinkResolution: 'legacy',
    initialNavigation: 'enabled'
})],
})
export class AppRoutingModule { }